package robotspawn;

import battlecode.common.RobotController;


public class RobotPlayer {
	
	public static void run(RobotController rc) {
		
		BaseRobot robot = null;

		int rseed = rc.getRobot().getID();
		
		try {
			switch(rc.getType()) {
				case HQ:
					robot = new HQRobot(rc);
					robot.loop();
					break;
				case SOLDIER:
					robot = new SoldierRobot(rc);
					robot.loop();
					break;
				default:
					break;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
