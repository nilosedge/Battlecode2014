package robotspawn;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.TerrainTile;

public class HQRobot extends BaseRobot {


	public HQRobot(RobotController rc) throws GameActionException {
		super(rc);
		
//		int array[] = getMapLocations();
//		rc.broadcast(60000, array.length);
//		for(int i = 0; i < array.length; i++) {
//			rc.broadcast(i + 60001, array[i]);
//		}
	}

	public void run() {
		try {
			
			int[] closestEnemyInfo = getClosestEnemy(rc.senseNearbyGameObjects(Robot.class, 100000, rc.getTeam().opponent()));
			MapLocation closestEnemyLocation = new MapLocation(closestEnemyInfo[1], closestEnemyInfo[2]);
			
			if(rc.isActive() && rc.canAttackSquare(closestEnemyLocation)) {
				rc.attackSquare(closestEnemyLocation);
			}

			spawn();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void spawn() throws GameActionException {
		Direction desiredDir = rc.getLocation().directionTo(rc.senseEnemyHQLocation());
		Direction dir = getSpawnDirection(desiredDir);
		if (dir != null) {
			rc.spawn(dir);
		}
	}

	private Direction getSpawnDirection(Direction dir) {
		Direction canMoveDirection = null;
		int desiredDirOffset = dir.ordinal();
		int[] dirOffsets = new int[]{4, -3, 3, -2, 2, -1, 1, 0};
		for (int i = dirOffsets.length; --i >= 0; ) {
			int dirOffset = dirOffsets[i];
			Direction currentDirection = Direction.values()[(desiredDirOffset + dirOffset + 8) % 8];
			if (rc.isActive() && rc.canMove(currentDirection) && rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
				if (canMoveDirection == null) {
					canMoveDirection = currentDirection;
					break;
				}
			}
		}
		// Otherwise, let's just spawn in the desired direction, and make sure to clear out a path later
		return canMoveDirection;
	}


	private int[] getMapLocations() {
		int array[] = new int[(rc.getMapHeight() * rc.getMapWidth() / 16) + 1];
		int count = 0;
		int idx = 0;
		int mod = 0;
		for(int i = 0; i < rc.getMapHeight(); i++) {
			for(int j = 0; j < rc.getMapWidth(); j++) {
				TerrainTile tile = rc.senseTerrainTile(new MapLocation(j, i));
				idx = count / 16;
				mod = count % 16;
				array[idx] |= tile.ordinal() << (mod * 2);
				count++;
			}
		}
		return array;
	}

	public int[] getClosestEnemy(Robot[] enemyRobots) throws GameActionException {
		
		MapLocation closestEnemy=rc.senseEnemyHQLocation(); // default to HQ
		//MapLocation closestEnemy = rc.senseHQLocation();
		int closestDist = 10000;
		
		int dist = 0;
		for (int i = enemyRobots.length; --i >= 0; ) {
			RobotInfo arobotInfo = rc.senseRobotInfo(enemyRobots[i]);
			if(arobotInfo.type != RobotType.HQ) {
				dist = arobotInfo.location.distanceSquaredTo(rc.getLocation());
				if(dist < closestDist) {
					closestDist = dist;
					closestEnemy = arobotInfo.location;
				}
			}
		}
		int[] output = new int[4];
		output[0] = closestDist;
		output[1] = closestEnemy.x;
		output[2] = closestEnemy.y;
		rc.setIndicatorString(2, "Closest: " + output[0] + ", " + output[1] + ", " + output[2] + ", " + output[3]);
		return output;
	}
	
}
