package robotspawn;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class SoldierRobot extends BaseRobot {

	RobotController rc;
	
	public SoldierRobot(RobotController rc) throws GameActionException {
		super(rc);
		this.rc = rc;
		NavSystem.init(this);
	}
	
	public void run() {
		try {
			NavSystem.goToLocation(rc.getLocation(), rc.senseEnemyHQLocation(), false);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}

}
