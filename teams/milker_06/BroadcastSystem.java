package milker_06;

import java.util.ArrayList;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import battlecode.common.TerrainTile;


public class BroadcastSystem {
	
	public static BaseRobot robot;
	public static RobotController rc;
	public static int WIDTH = 0;
	public static int HEIGHT = 0;
	
	public static final int jobMessageLowOffset = 10000;
	public static final int jobMessageHighOffset = 20000;

	//public static final int jobMessageRange = (jobMessageHighOffset - jobMessageLowOffset) / jobMessageSize;
	

	public static final int jobMessageHQLowOffset = 20000;
	public static final int jobMessageHQHighOffset = 30000;

	//public static final int jobMessageHQRange = (jobMessageHQHighOffset - jobMessageHQLowOffset) / jobMessageHQSize;
	//public static       int jobMessageHQLastIndex = 0;
	
	public static final int robotData = 1100;
	public static final int latestJobMessage = 1101;
	public static final int latestJobMessageHQ = 1105;
	
	public static       int lastReadJobMessageIndex = 0;
	public static       int lastSendJobMessageIndex = 0;
	
	public static final int countOffset01 = 1102;

	public static final int mapComplete = 1107;
	
	public static final int bandChanelBase = 1110; // Used for bands 0 - 4 they will add their band offset to this
		
	public static final int ourPastrLocationsOffset = 1200;
	
	public static final int jobMessagePayloads = 10000; // top end will be 19999
	
	public static final int pastrPointsOffset = 59800;
	public static final int mapOffset = 60000;
	
	public static final boolean debug = false;

	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
		WIDTH = DataCache.mapWidth;
		HEIGHT = DataCache.mapHeight;
		if(rc.getType() == RobotType.HQ) {
			lastReadJobMessageIndex = jobMessageHQLowOffset;
			lastSendJobMessageIndex = jobMessageLowOffset;
		} else {
			lastReadJobMessageIndex = jobMessageLowOffset;
		}
	}

	public static void sendMap(TerrainTile[][] map) throws GameActionException {
		
		int array[] = new int[(HEIGHT * WIDTH / 16) + 1];
		int count = 0;
		int idx = 0;
		int mod = 0;
		for(int i = 0; i < HEIGHT; i++) {
			for(int j = 0; j < WIDTH; j++) {
				idx = count / 16;
				mod = count % 16;
				array[idx] |= map[j][i].ordinal() << (mod * 2);
				count++;
			}
		}

		rc.broadcast(mapOffset, array.length);
		int i = 0;
		while(i < array.length) {
			rc.broadcast(i + mapOffset + 1, array[i++]);
		}
	}
	
	public static TerrainTile[][] readMap() throws GameActionException {
		TerrainTile[][] map = new TerrainTile[WIDTH][HEIGHT];
		int mapSize = rc.readBroadcast(mapOffset);
		int count = 0;
		for(int i = 0; i < mapSize; i++) {
			int data = rc.readBroadcast(i + mapOffset + 1);
			for(int j = 0; j < 16; j++) {
				map[count % WIDTH][(count / HEIGHT)] = TerrainTile.values()[(3 & (data >> (j * 2)))];
				count++;
				if(count >= (HEIGHT * WIDTH)) {
					break;
				}
			}
			if(count >= (HEIGHT * WIDTH)) {
				break;
			}
		}
		return map;
	}

	public static void sendGoodMapSpots(ArrayList<MapLocation> goodTowerLocations) throws GameActionException {
		rc.broadcast(pastrPointsOffset, goodTowerLocations.size());
		int send = 0;
		for(int i = 1; i <= goodTowerLocations.size(); i++) {
			send = Util.convertMapLocationToInt(goodTowerLocations.get(i));
			rc.broadcast(i + pastrPointsOffset, send);
		}
	}
	
	public static ArrayList<MapLocation> readGoodMapSpots() throws GameActionException {
		ArrayList<MapLocation> locs = new ArrayList<MapLocation>();
		int size = rc.readBroadcast(pastrPointsOffset);
		for(int i = 1; i <= size; i++) {
			int data = rc.readBroadcast(i + pastrPointsOffset);
			locs.add(Util.convertIntToMapLocation(data));
		}
		return locs;
	}
	




	
	
	
	public static void sendJobMessage(JobMessage jm) throws GameActionException {
		// TODO handle the multiple send from the HQ to robots
		
		postJobMessage(jm, lastSendJobMessageIndex);
		
		//Util.printJobMessage(jm, false);
		//System.out.println("latestMessage: " + latestJobMessage);
		//System.out.println("lastSendJobMessageIndex: " + lastSendJobMessageIndex);
		
		lastSendJobMessageIndex += JobMessage.jobMessageSize;
		if(lastSendJobMessageIndex >= jobMessageHighOffset) {
			lastSendJobMessageIndex = jobMessageLowOffset;
		}
		
		
		//Util.printJobMessage(jm, true);

		//System.out.println("lastSendJobMessageIndex: " + lastSendJobMessageIndex);

	}
	
	public static JobMessage getJobMessage() throws GameActionException {
		JobMessage jm = null;

		int readLocation = rc.readBroadcast(latestJobMessage);
		//System.out.println("Read Location: " + readLocation);
		for(int k = readLocation; k >= jobMessageLowOffset && k > readLocation - (5 * JobMessage.jobMessageSize); k -= JobMessage.jobMessageSize) {
			
			//System.out.print(i + ", ");
			jm = new JobMessage(rc.readBroadcast(k));
			//System.out.println("Read Location: " + k);
			if(jm.header != 0 && (!jm.isAccepted() || jm.getBroadcastType() == JobMessage.BroadCastType.GLOBAL)) {
				jm.setAccepted(true);
				rc.broadcast(k, jm.header);
				int size = jm.bodySize;
				for(int j = 0; j < size; j++) {
					jm.body[j] = rc.readBroadcast(k + j + 1);
				}
				//System.out.println();
				//Util.printJobMessage(jm, true);
				//rc.setIndicatorString(1, "Counter Cost: " + Util.stopCounter());
				return jm;
			} else {
				jm = null;
			}
		}
		//System.out.println();
		return jm;
			
	}
	
	// Robots Call
	public static void sendJobMessageToHQ(JobMessage jm) throws GameActionException {
		// TODO handle the multiple send from the robots
		int offset = rc.readBroadcast(latestJobMessageHQ);
		//First Round
		if(offset == 0) {
			offset = jobMessageHQLowOffset;
			rc.broadcast(latestJobMessageHQ, offset);	
		}
		
		postJobMessage(jm, offset);
		//Util.printJobMessage(jm, false);
		if(offset + JobMessage.jobMessageSize >= jobMessageHQHighOffset) {
			rc.broadcast(latestJobMessageHQ, jobMessageHQLowOffset);
		} else {
			rc.broadcast(latestJobMessageHQ, offset + JobMessage.jobMessageSize);
		}
	}

	// HQ Calls needs to return the whole list for this round
	public static ArrayList<JobMessage> getJobMessagesHQ() throws GameActionException {
		ArrayList<JobMessage> jms = new ArrayList<JobMessage>();

		int readLocation = rc.readBroadcast(latestJobMessageHQ);
		
		//System.out.println("ReadLocation: " + readLocation);
		
		if(lastReadJobMessageIndex > readLocation) {
			//TODO handle wrapping
		} else {
			while(lastReadJobMessageIndex < readLocation) {
				JobMessage jm = new JobMessage(rc.readBroadcast(lastReadJobMessageIndex));
				if(jm.header != 0) {
					for(int k = 0; k < jm.body.length; k++) {
						jm.body[k] = rc.readBroadcast(lastReadJobMessageIndex + k + 1);
					}
					jms.add(jm);
				}
				lastReadJobMessageIndex += JobMessage.jobMessageSize;
			}
		}
		return jms;
	}
	
	public static void postJobMessage(JobMessage jm, int offset) throws GameActionException {
		rc.broadcast(offset, jm.header);
		for(int k = 0; k < jm.body.length; k++) {
			rc.broadcast(offset + k + 1, jm.body[k]);
		}
	}

	public static JobMessage getAllInJob() {
		
		return null;
	}
	
//	public static void sendRemoteData(int[] in) throws GameActionException {
//		rc.broadcast(robotData, Util.convertMapLocationToInt(new MapLocation(in[0], in[1])));
//		rc.broadcast(robotData + 1, Util.convertMapLocationToInt(new MapLocation(in[2], in[3])));
//	}

//	public static int[] readRemoteData() throws GameActionException {
//		int data[] = new int[4];
//		MapLocation d1 = Util.convertIntToMapLocation(rc.readBroadcast(robotData));
//		MapLocation d2 = Util.convertIntToMapLocation(rc.readBroadcast(robotData + 1));
//		data[0] = d1.x;
//		data[1] = d1.y;
//		data[2] = d2.x;
//		data[3] = d2.y;
//		return data;
//	}
	
//	public static MapLocation[] readOurPastrLocations() throws GameActionException {
//		int len = rc.readBroadcast(ourPastrLocationsOffset);
//		MapLocation[] locs = new MapLocation[len];
//		for(int i = 0; i < len; i++) {
//			locs[i] = Util.convertIntToMapLocation(rc.readBroadcast(i + 1));
//		}
//		return locs;
//	}
//
//	public static void sendOurPastrLocations(MapLocation[] ourPastrLocations) throws GameActionException {
//		rc.broadcast(ourPastrLocationsOffset, ourPastrLocations.length);
//		for(int i = 0; i < ourPastrLocations.length; i++) {
//			rc.broadcast(ourPastrLocationsOffset + (i + 1), Util.convertMapLocationToInt(ourPastrLocations[i]));
//		}
//	}

	public static void maintainJobs() throws GameActionException {
		int startIndex = rc.readBroadcast(latestJobMessage);
		int endIndex = lastSendJobMessageIndex;
		
		// First Round
		if(startIndex == 0) {
			startIndex = jobMessageLowOffset;
		}
		
//		boolean main = false;
//		if(startIndex != endIndex) {
//			main = true;
//			System.out.println("Start Index: " + startIndex + " End Index: " + endIndex);
//		}

		if(endIndex < startIndex) {
			// Handle the wrap around
		} else {
		
			while(startIndex <= endIndex) {
				
				JobMessage jm = new JobMessage(rc.readBroadcast(startIndex));
				//Util.printJobMessage(jm, true);
				if(jm.header != 0 && jm.isAccepted()) {
					startIndex += JobMessage.jobMessageSize;
				} else {
					break;
				}
			}
		}
		rc.broadcast(latestJobMessage, startIndex);
		//if(main) System.out.println("Setting Index to: " + startIndex);
	}

	public static void updateSolderierCount() throws GameActionException {
		int count[] = Util.unpackFourInts(rc.readBroadcast(countOffset01));
		count[1]++;
		rc.broadcast(countOffset01, Util.packFourInts(count));
	}
	
	public static void updateDefendingCount() throws GameActionException {
		int count[] = Util.unpackFourInts(rc.readBroadcast(countOffset01));
		count[0]++;
		rc.broadcast(countOffset01, Util.packFourInts(count));
	}
	
	public static void updatePastrCount() throws GameActionException {
		int count[] = Util.unpackFourInts(rc.readBroadcast(countOffset01));
		count[2]++;
		rc.broadcast(countOffset01, Util.packFourInts(count));
	}
	
	public static void updateNoiseCount() throws GameActionException {
		int count[] = Util.unpackFourInts(rc.readBroadcast(countOffset01));
		count[3]++;
		rc.broadcast(countOffset01, Util.packFourInts(count));
	}

	public static void getStatusCounts() throws GameActionException {
		
		int readBroadcast = rc.readBroadcast(countOffset01);

		DataCacheHQ.defendingCount = readBroadcast >>> 24;
		DataCacheHQ.defendingCountChanged = (DataCacheHQ.lastDefendingCount == DataCacheHQ.defendingCount);
		DataCacheHQ.lastDefendingCount =DataCacheHQ. defendingCount;
		DataCacheHQ.soldierCount = (readBroadcast >>> 16) & 255;
		DataCacheHQ.soldierCountChanged = (DataCacheHQ.lastSoldierCount == DataCacheHQ.soldierCount);
		DataCacheHQ.lastSoldierCount = DataCacheHQ.soldierCount;
		DataCacheHQ.pastrCount = (readBroadcast >>> 8) & 255;
		DataCacheHQ.pastrCountChanged = (DataCacheHQ.lastPastrCount == DataCacheHQ.pastrCount);
		DataCacheHQ.lastPastrCount = DataCacheHQ.pastrCount;
		DataCacheHQ.noiseCount = readBroadcast & 255;
		DataCacheHQ.noiseCountChanged = (DataCacheHQ.lastNoiseCount == DataCacheHQ.noiseCount);
		DataCacheHQ.lastNoiseCount = DataCacheHQ.noiseCount;

//		array = Util.unpackFourInts(rc.readBroadcast(bandChanelBase));
//		
//		counts[4] = array[0];
//		counts[5] = array[1];
//		counts[6] = array[2];
//		counts[7] = array[3];
		
		// BC 30
		clearCounters();

	}
	
	public static void clearCounters() throws GameActionException {
		rc.broadcast(countOffset01, 0);
		//rc.broadcast(bandChanelBase, 0);
	}

	public static void setMapComplete() throws GameActionException {
		rc.broadcast(mapComplete, 1);
	}
	
	public static boolean isMapComplete() throws GameActionException {
		return (rc.readBroadcast(mapComplete) > 0);
	}
}
