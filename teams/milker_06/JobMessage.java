package milker_06;

import java.util.ArrayList;

import battlecode.common.MapLocation;

public class JobMessage extends Message {
	
	// header =
	// 32 -> 1
	// 
	// Broadcast 31 - 3
	// BandNumber 29 - 27
	// Job type 26 - 24
	// Build Type 23
	// Accepted 22
	// MessageType
	// body = 
	// 9 ints of way points
	// 0 - 5 are 12 waypoints to get to the job
	// 6 - 8 are 6 waypoints used in the job
	// each int = 0000 1111111 1111111 1111111 1111111
	
	public static final int jobMessageSize = 10;
	
	int broadcastWidth = 3; // Two Bits
	int broadcastOffset = 30;
	int broadcastNumberWidth = 15; // Three Bits
	int broadcastNumberOffset = 27;
	int jobTypeWidth = 15; // Three Bits
	int jobTypeOffset = 24;
	int buildTypeWidth = 1; // 1 Bit
	int buildTypeOffset = 23;
	int acceptedWidth = 1; // 1 Bit
	int acceptedOffset = 22;
	int messageTypeWidth = 31; // 4 Bits
	int messageTypeOffset = 18;
	
	
	public JobMessage() {
		
	}
	
	public JobMessage(int header) {
		this.header = header;
	}
	
	public BroadCastType getBroadcastType() {
		return BroadCastType.valueOf((header & (broadcastWidth << broadcastOffset)) >>> broadcastOffset);
	}
	
	public void setBroadcastType(BroadCastType type) {
		header |= (type.value << broadcastOffset);
	}
	
	public int getBandNumber() {
		return (header & (broadcastNumberWidth << broadcastNumberOffset)) >>> broadcastNumberOffset;
	}

	public void setBandNumber(int band) {
		header |= (band << broadcastNumberOffset);
	}
	
	public JobType getJobType() {
		return JobType.valueOf((header & (jobTypeWidth << jobTypeOffset)) >>> jobTypeOffset);
	}
	
	public void setJobType(JobType type) {
		header |= (type.value << jobTypeOffset);
	}
	
	public BuildType getBuildType() {
		return BuildType.valueOf((header & (buildTypeWidth << buildTypeOffset)) >>> buildTypeOffset);
	}
	
	public void setBuildType(BuildType type) {
		header |= (type.value << buildTypeOffset);
	}

	public boolean isAccepted() {
		return (header & (acceptedWidth << acceptedOffset)) > 0;
	}
	
	public void setAccepted(boolean accepted) {
		if(accepted) {
			header |= (acceptedWidth << acceptedOffset);
		} else {
			header &= ~(acceptedWidth << acceptedOffset);
		}
	}
	
	public MessageType getMessageType() {
		return MessageType.valueOf((header & (messageTypeWidth << messageTypeOffset)) >>> messageTypeOffset);
	}
	
	public void setMessageType(MessageType type) {
		header |= (type.value << messageTypeOffset);
	}

	// BC 192
	public ArrayList<MapLocation> getWayPointsToJob() {
		// points are 7 bits each * 9 ints
		// int 0000 0000000 0000000 1111111 1111111
		// If job type is DEFENDING the last two are our "job Points and the first 7 our the way point points
		ArrayList<MapLocation> points = new ArrayList<MapLocation>();
		int size = body.length;
		for(int i = 0; body[i] != 0 && i < 5 && i < size; i++) {
		//for(int i: body) {
			points.add(Util.convertIntToMapLocation(body[i]));
		}
		return points;
	}
	
	public void setWayPointsToJob(ArrayList<MapLocation> points) {
		for(int i = 0; i < points.size() && i < 5; i++) {
			body[i] = Util.convertMapLocationToInt(points.get(i));
		}
	}

	public ArrayList<MapLocation> getJobPoints() {
		ArrayList<MapLocation> points = new ArrayList<MapLocation>();
		int size = body.length;
		for(int i = 5; body[i] != 0 && i < size; i++) {
			//if(body[i] != 0) {
				points.add(Util.convertIntToMapLocation(body[i]));
			//}
		}
		return points;
	}
	
	public void setJobPoints(ArrayList<MapLocation> points) {
		for(int i = 0; i < points.size(); i++) {
			body[i + 5] = Util.convertMapLocationToInt(points.get(i));
		}
	}
	
	// BC 220
//	public ArrayList<MapLocation> getWayPointsToJob() {
//		ArrayList<MapLocation> points = new ArrayList<MapLocation>();
//		for(int i = 0; i < 6 && body[i] != 0; i++) {
//			MapLocation upper = Util.convertIntToMapLocation(body[i] >>> 14);
//			MapLocation lower = Util.convertIntToMapLocation(body[i]);
//			if(upper.x != 0 && upper.y != 0) {
//				points.add(upper);
//			}
//			if(lower.x != 0 && lower.y != 0) {
//				points.add(lower);
//			}
//		}
//		return points;
//	}
//	
//	public void setWayPointsToJob(ArrayList<MapLocation> points) {
//		for(int i = 0; i < points.size() && i < 12; i++) {
//			if(i % 2 == 0) {
//				body[(i / 2)] = Util.convertMapLocationToInt(points.get(i)) << 14;
//			} else {
//				body[(i / 2)] |= Util.convertMapLocationToInt(points.get(i));
//			}
//		}
//	}

//	public ArrayList<MapLocation> getJobPoints() {
//		ArrayList<MapLocation> points = new ArrayList<MapLocation>();
//		for(int i = 6; i < body.length && body[i] != 0; i++) {
//			MapLocation upper = Util.convertIntToMapLocation(body[i] >>> 14);
//			MapLocation lower = Util.convertIntToMapLocation(body[i]);
//			if(upper.x != 0 && upper.y != 0) {
//				points.add(upper);
//			}
//			if(lower.x != 0 && lower.y != 0) {
//				points.add(lower);
//			}
//		}
//		return points;
//	}
//	
//	public void setJobPoints(ArrayList<MapLocation> points) {
//		for(int i = 0; i < points.size() && i < 6; i++) {
//			if(i % 2 == 0) {
//				body[(i + 12) / 2] = Util.convertMapLocationToInt(points.get(i)) << 14;
//			} else {
//				body[(i + 12) / 2] |= Util.convertMapLocationToInt(points.get(i));
//			}
//		}
//	}
		
	public String toString() {
		return "Job: " + getJobType() + " Build: " + getBuildType() + " Broadcast: " + getBroadcastType();
	}
	
	public enum BroadCastType {
		PERSONAL(0),  // 00
		BAND(1),      // 01
		GLOBAL(2),    // 10
		UNKNOWN(3)    // 11
		;
		
		private int value;
		private BroadCastType(int v) {
			value = v;
		}
		
		public static BroadCastType valueOf(int i) {
			return BroadCastType.values()[i];
		}
	}
	
	public enum JobType {
		UNKNOWN(0),            // 000
		HAVOCING(1),           // 001
		BUILDING(2),           // 010
		DEFENDING(3),          // 011
		MAPBUILD(4),           // 100
		TRAVEL(5),             // 101
		MESSAGE(6),            // 110
		GENGOODSPOTS(7),      // 111
		;
		
		private int value;
		
		JobType(int v) {
			value = v;
		}

		public static JobType valueOf(int i) {
			return JobType.values()[i];
		}
	}
	
	public enum BuildType {
		PASTR(0), // 0
		NOISE(1), // 1
		;

		private int value;
		BuildType(int v) {
			value = v;
		}
		
		public static BuildType valueOf(int i) {
			return BuildType.values()[i];
		}
	}
	
	public enum MessageType {
		MAPCOMPLETE(0),        // 000
		PASTRCOMPLETE(1),      // 001
		NOISECOMPLETE(2),      // 010
		GOODSPOTSCOMPLETE(3),  // 011
		TEST2(4),              // 100
		TEST3(5),              // 101
		TEST4(6),              // 110
		TEST5(7),              // 111
		;
		
		private int value;
		MessageType(int v) {
			value = v;
		}

		public static MessageType valueOf(int i) {
			return MessageType.values()[i];
		}
	}
}