package milker_06;

import battlecode.common.GameActionException;
import battlecode.common.Robot;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class DataCacheSoldier extends DataCache {

	public static Robot[] nearbyAlliedRobots;
	public static int numNearbyAlliedSoldiers;
	public static int bandNumber = 0;
	
	public static boolean haveMap = false;

	public static void init() {
		
		
	}
	
	public static void updateRoundVariables() throws GameActionException {

		broadcastOffset = 0;
		

		BroadcastSystem.updateSolderierCount();

		nearbyAlliedRobots = rc.senseNearbyGameObjects(Robot.class, 14, myTeam);

		numNearbyAlliedSoldiers = 0;
		for (int i = nearbyAlliedRobots.length; --i >= 0; ) {
			RobotInfo robotInfo = rc.senseRobotInfo(nearbyAlliedRobots[i]);
			if (robotInfo.type == RobotType.SOLDIER) {
				numNearbyAlliedSoldiers++;
			}
		}
		
		nearbyEnemyRobots = rc.senseNearbyGameObjects(Robot.class, 25, enemy);
		
		numNearbyEnemySoldiers = 0;
		for (int i = nearbyEnemyRobots.length; --i >= 0; ) {
			RobotInfo robotInfo = rc.senseRobotInfo(nearbyEnemyRobots[i]);
			if (robotInfo.type == RobotType.SOLDIER) {
				numNearbyEnemySoldiers++;
			}
		}
		
		numNearbyEnemyRobots = nearbyEnemyRobots.length;

	}



}
