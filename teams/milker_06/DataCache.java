package milker_06;

import java.util.ArrayList;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.Team;
import battlecode.common.TerrainTile;

public class DataCache {

	public static BaseRobot robot;
	public static RobotController rc;
	
	public static MapLocation ourHQLocation;
	public static MapLocation enemyHQLocation;
	public static int rushDistSquared;
	public static int rushDist;
	
	public static Direction[] directionArray = Direction.values();
	
	// Map width
	public static int mapWidth;
	public static int mapHeight;
	
	// Round variables - army sizes
	// Allied robots
	
	//public static int numAlliedRobots;
	//public static int numAlliedSoldiers;
	//public static int numAlliedPastrs;
	
	//public static int numNearbyAlliedRobots;	
	//public static int numNearbyAlliedSoldiers;
	//public static int numNearbyAlliedPastrs;

	public static Team myTeam;
	public static Team enemy;
	
	// Enemy robots
	//public static int numEnemyRobots;
	
	public static Robot[] enemiesList;
	public static Robot[] nearbyEnemyRobots;
	
	public static int numNearbyEnemyRobots;
	public static int numNearbyEnemySoldiers;
	
	public static ArrayList<MapLocation> goodMapSpots;
	public static double[][] cowGrowth;


	public static TerrainTile[][] map;
	

	//public static int roundNumber = 0;
	public static int broadcastOffset = 0;

	
	
	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
		
		myTeam = rc.getTeam();
		enemy = myTeam.opponent();
		
		ourHQLocation = rc.senseHQLocation();
		enemyHQLocation = rc.senseEnemyHQLocation();
		rushDistSquared = ourHQLocation.distanceSquaredTo(enemyHQLocation);
		rushDist = (int) Math.sqrt(rushDistSquared);
		cowGrowth = rc.senseCowGrowth();
		mapWidth = rc.getMapWidth();
		mapHeight = rc.getMapHeight();

	}

	public static boolean isLargeMap() {
		return (mapHeight * mapWidth > 2500);
	}

	public static boolean isMediumMap() {
		return (mapHeight * mapWidth > 1200);
	}
	
	public static void generateMap() throws GameActionException {
		map = new TerrainTile[mapWidth][mapHeight];
		for(int i = 0; i < mapHeight; i++) {
			for(int j = 0; j < mapWidth; j++) {
				map[j][i] = rc.senseTerrainTile(new MapLocation(j, i));
			}
		}
	}

	public void printCowMap(double[][] cowGrowth) {
		for(int k = 0; k < mapHeight; k++) {
			for(int i = 0; i < mapWidth; i++) {
				System.out.print(cowGrowth[i][k]);
			}
			System.out.println();
		}
	}
	
	public void printMap() {
		printMap(map);
	}
	
	public void printMap(TerrainTile[][] map) {
		for(int k = 0; k < mapHeight; k++) {
			for(int i = 0; i < mapWidth; i++) {
				System.out.print(map[i][k].ordinal());
			}
			System.out.println();
		}
	}
	
	public static void generateGoodTowerLocations() {
		

		goodMapSpots = new ArrayList<MapLocation>();
		goodMapSpots.add(DataCache.ourHQLocation.add(DataCache.enemyHQLocation.directionTo(DataCache.ourHQLocation)));
		
		int cowG = 0;
		int opens = 0;
		int sizeFactor = 6;
		int squ = sizeFactor * sizeFactor;
		for(int h = 0; h < mapHeight; h+=sizeFactor) {
			for(int w = 0; w < mapWidth; w+=sizeFactor) {
				for(int y = 0; y < sizeFactor && (y + h) < mapHeight; y++) {
					for(int x = 0; x < sizeFactor && (x + w) < mapWidth; x++) {
						if(DataCacheSoldier.cowGrowth[x + w][y + h] > 0.9) cowG++;
						if(map[x + w][y + h] == TerrainTile.NORMAL || map[x + w][y + h] == TerrainTile.ROAD) opens++;
					}
				}
				//System.out.println("W: " + w + " H: " + h + " Cows: " + cowG + " opens: " + opens);
				if(cowG >= (squ - sizeFactor) && opens > (squ - sizeFactor)) {
					goodMapSpots.add(new MapLocation(w + 2, h + 2));
					//goodTowerSpots.add(new MapLocation(w + 2, h + 2));
					//System.out.println("Added: ");
				} else {
					//System.out.println("No Added: ");
				}
				cowG = 0;
				opens = 0;
			}
		}
		// TODO sort by distance to HQ
		// TODO make this really smart
		// Sort order needs to be fixed
		//System.out.println("Good Map Locations: " + "Count: " + counter + " BC: " + Util.stopCounter());
	}


}
