package milker_06;

import java.util.ArrayList;
import java.util.HashMap;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class NoiseTowerRobot extends BaseRobot {
	
	public int rangeCounter = 0;
	public MapLocation myLoc;
	public double rotate;
	public int maxDist = 17;
	public int minDist = 5;
	public int dist = maxDist;
	public int localDist = 0;
	public double eightPI = Math.PI / 8;
	public double lookRate = eightPI;
	public int rotCount = 0;
	public Direction lookDirection;
	public int lookIndex = 0;
	
	public HashMap<Direction, ArrayList<MapLocation>> attacks = new HashMap<Direction, ArrayList<MapLocation>>();

	public NoiseTowerRobot(RobotController rc) throws GameActionException {
		super(rc);
		myLoc = rc.getLocation();
		//map = BroadcastSystem.readMap();
		
		// This is horrid however works.
		ArrayList<MapLocation> northMap = new ArrayList<MapLocation>();
		ArrayList<MapLocation> southMap = new ArrayList<MapLocation>();
		ArrayList<MapLocation> eastMap = new ArrayList<MapLocation>();
		ArrayList<MapLocation> westMap = new ArrayList<MapLocation>();
		ArrayList<MapLocation> southEastMap = new ArrayList<MapLocation>();
		ArrayList<MapLocation> southWestMap = new ArrayList<MapLocation>();
		ArrayList<MapLocation> northWestMap = new ArrayList<MapLocation>();
		ArrayList<MapLocation> northEastMap = new ArrayList<MapLocation>();
		double sqrt2 = Math.sqrt(2);
		int ip = 0;
		for(int i = 11; i >= 4; i--) {
			ip = (int)(i * sqrt2);
			northMap.add(myLoc.add(0, -ip));
			southMap.add(myLoc.add(0, ip));
			eastMap.add(myLoc.add(ip, 0));
			westMap.add(myLoc.add(-ip, 0));
			southEastMap.add(myLoc.add(i, i));
			southWestMap.add(myLoc.add(-i, i));
			northWestMap.add(myLoc.add(-i, -i));
			northEastMap.add(myLoc.add(i, -i));
		}
		attacks.put(Direction.NORTH, northMap);
		attacks.put(Direction.SOUTH, southMap);
		attacks.put(Direction.EAST, eastMap);
		attacks.put(Direction.WEST, westMap);
		attacks.put(Direction.SOUTH_EAST, southEastMap);
		attacks.put(Direction.SOUTH_WEST, southWestMap);	
		attacks.put(Direction.NORTH_WEST, northWestMap);
		attacks.put(Direction.NORTH_EAST, northEastMap);
		
		//System.out.println(attacks);
		
		lookDirection = DataCache.ourHQLocation.directionTo(DataCache.enemyHQLocation);
	}

	@Override
	public void run() {
		if(rc.getActionDelay() > 0) return;
		try {
			
			//DataCacheTower.updateRoundVariables(false);
			

				
				
//				rotate += lookRate;
//				
//	//			System.out.println("X: " + -Math.sin(rotate + lookRate));
//	//			System.out.println("Y: " + Math.cos(rotate + lookRate));
//	//			System.out.println("XD: " + -Math.sin(rotate + lookRate) * dist);
//	//			System.out.println("YD: " + Math.cos(rotate + lookRate) * dist);
//	//
//	//			System.out.println("MyLoc: X: " + myLoc.x + " Y: " + myLoc.y);
//	//			System.out.println("Checking: X: " + x + " Y: " + y);
//				if(dist < minDist) {
//					dist = maxDist;
//				}
//				if(rotCount % 12 == 0) {
//					dist-=1;
//				}
//				 
//				MapLocation attack = null;
//				localDist = dist;
//				while(attack == null && localDist > 0) {
//					attack = getAttackTile(rotate, localDist);
//					localDist--;
//				}
//				rotCount++;
				
				
				
				
				rc.setIndicatorString(0, "AD: " + rc.getActionDelay() + " ACT: " + rc.isActive());
				
		
				
				if(lookIndex == attacks.get(lookDirection).size()) {
					lookDirection = lookDirection.rotateLeft();
					lookIndex = 0;
					System.out.println(attacks.get(lookDirection));
				}
				
				rc.setIndicatorString(1, "Trying: " + lookIndex + " " + lookDirection + " " + attacks.get(lookDirection).get(lookIndex));
				
				MapLocation loc = attacks.get(lookDirection).get(lookIndex);
				
				
				
				if(rc.isActive() && rc.canAttackSquare(loc)) {
					rc.setIndicatorString(2, "Attacking: " + lookIndex + " " + lookDirection + " " + attacks.get(lookDirection).get(lookIndex));
					rc.attackSquare(loc);
				} else {
					rc.setIndicatorString(2, "No Attack: " + lookIndex + " " + lookDirection + " " + attacks.get(lookDirection).get(lookIndex));
				}
				lookIndex++;

			
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}

	public MapLocation getAttackTile(Direction dir, int dist) {
		MapLocation attack = myLoc;
		attack = attack.add(dir, dist);
		rc.setIndicatorString(0, "Trying to attack: " + attack);
		if(attack.x < 0 || attack.y < 0 || attack.x > DataCacheSoldier.mapWidth || attack.y > DataCacheSoldier.mapHeight) {
			return null;
		}
		if(rc.canAttackSquare(attack)) {
			return attack;
		}
		return null;
	}
	
	public MapLocation getAttackTile(double rotate, int dist) {
		int x = (int)(-Math.sin(rotate + lookRate) * dist) + myLoc.x;
		int y = (int)(Math.cos(rotate + lookRate) * dist) + myLoc.y;
		if(x < 0 || y < 0 || x > DataCacheSoldier.mapWidth || y > DataCacheSoldier.mapHeight) {
			return null;
		}
		MapLocation loc = new MapLocation(x, y);
		if(rc.canAttackSquare(loc)) {
			return loc;
		}
		return null;
	}

}
