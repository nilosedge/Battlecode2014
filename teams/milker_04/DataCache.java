package milker_04;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.TerrainTile;

public class DataCache {
	
	public static BaseRobot robot;
	public static RobotController rc;
	
	public static MapLocation ourHQLocation;
	public static MapLocation enemyHQLocation;
	public static int rushDistSquared;
	public static int rushDist;
	
	public static Direction[] directionArray = Direction.values();
	
	// Map width
	public static int mapWidth;
	public static int mapHeight;
	
	// Round variables - army sizes
	// Allied robots
	public static int numAlliedRobots;
	public static int numAlliedSoldiers;
	public static int numAlliedPastrs;
	
	public static int numNearbyAlliedRobots;	
	public static int numNearbyAlliedSoldiers;
	public static int numNearbyAlliedPastrs;

	// Enemy robots
	public static int numEnemyRobots;
	
	public static Robot[] nearbyEnemyRobots;
	
	public static int numNearbyEnemyRobots;
	public static int numNearbyEnemySoldiers;
	
	//public static MapLocation[] buildPastrLocations;
	public static double[][] cowGrowth;
	public static MapLocation[] ourPastrLocations;
	public static int idealPastrCount;
	public static TerrainTile[][] map;
	public static MapLocation[] topTowerLocations;
	
	
	
	public static int roundNumber = 0;
	public static int broadcastOffset = 0;

	
	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
		
		ourHQLocation = rc.senseHQLocation();
		enemyHQLocation = rc.senseEnemyHQLocation();
		rushDistSquared = ourHQLocation.distanceSquaredTo(enemyHQLocation);
		rushDist = (int) Math.sqrt(rushDistSquared);
		cowGrowth = rc.senseCowGrowth();
		mapWidth = rc.getMapWidth();
		mapHeight = rc.getMapHeight();
		idealPastrCount = (int)Math.sqrt(Math.sqrt((mapHeight * mapWidth))) + 2;
	}
	
	/**
	 * A function that updates round variables
	 */
	public static void updateRoundVariables() throws GameActionException {

		if(rc.getType() == RobotType.HQ) {
			ourPastrLocations = rc.sensePastrLocations(rc.getTeam());
			numAlliedPastrs = ourPastrLocations.length;
		}
		
		roundNumber = Clock.getRoundNum();
		broadcastOffset = 0;
		
		numAlliedRobots = rc.senseNearbyGameObjects(Robot.class, 10000, rc.getTeam()).length;
		
		numAlliedSoldiers = numAlliedRobots - numAlliedPastrs - 1 - JobSystem.numPastrsNeeded;
		
		numNearbyAlliedRobots = rc.senseNearbyGameObjects(Robot.class, 14, rc.getTeam()).length;
		//numNearbyAlliedPastrs = pastrLocations.length;
		numNearbyAlliedSoldiers = numNearbyAlliedRobots - numNearbyAlliedPastrs;
		
		nearbyEnemyRobots = rc.senseNearbyGameObjects(Robot.class, 25, rc.getTeam().opponent());
		
		numNearbyEnemySoldiers = 0;
		for (int i = nearbyEnemyRobots.length; --i >= 0; ) {
			RobotInfo robotInfo = rc.senseRobotInfo(nearbyEnemyRobots[i]);
			if (robotInfo.type == RobotType.SOLDIER) {
				numNearbyEnemySoldiers++;
			}
		}
		
		numNearbyEnemyRobots = nearbyEnemyRobots.length;
		numEnemyRobots = rc.senseNearbyGameObjects(Robot.class, 10000, rc.getTeam().opponent()).length;
		
		//if(buildPastrLocations.size() == 0) {
		//	buildPastrLocations = BroadcastSystem.readGoodMapSpots();
		//}
	}

	public static boolean isLargeMap() {
		return (mapHeight * mapWidth > 2500);
	}

	public static boolean isMediumMap() {
		return (mapHeight * mapWidth > 1200);
	}
	
	public static void generateMap() throws GameActionException {

		//System.out.println("Before: " + Clock.getRoundNum());

		///System.out.println("Middle: " + Clock.getRoundNum());
		//DataCache.map.initTopTowerLocations();
		//map.printCowMap(DataCache.cowGrowth);
		//System.out.println("After: " + Clock.getRoundNum());
		
		map = new TerrainTile[mapWidth][mapHeight];
		//Util.startCounter();
		for(int i = 0; i < mapHeight; i++) {
			for(int j = 0; j < mapWidth; j++) {
				map[j][i] = rc.senseTerrainTile(new MapLocation(j, i));
			}
		}
		//System.out.println("Init Map Cost: " + Util.stopCounter());
		
	}

	public void printCowMap(double[][] cowGrowth) {
		for(int k = 0; k < mapHeight; k++) {
			for(int i = 0; i < mapWidth; i++) {
				System.out.print(cowGrowth[i][k]);
			}
			System.out.println();
		}
	}
	
	public void printMap() {
		printMap(map);
	}
	
	public void printMap(TerrainTile[][] map) {
		for(int k = 0; k < mapHeight; k++) {
			for(int i = 0; i < mapWidth; i++) {
				System.out.print(map[i][k].ordinal());
			}
			System.out.println();
		}
	}
	


	
	public static void initTopTowerLocations() {
		//goodTowerSpots = new ArrayList<MapLocation>();
		topTowerLocations = new MapLocation[1000];
		int cowG = 0;
		int opens = 0;
		int counter = 0;
		//Util.startCounter();
		int sizeFactor = 6;
		int squ = sizeFactor * sizeFactor;
		for(int h = 0; h < mapHeight; h+=sizeFactor) {
			for(int w = 0; w < mapWidth; w+=sizeFactor) {
				for(int y = 0; y < sizeFactor && (y + h) < mapHeight; y++) {
					for(int x = 0; x < sizeFactor && (x + w) < mapWidth; x++) {
						if(DataCache.cowGrowth[x + w][y + h] > 0.9) cowG++;
						if(map[x + w][y + h] == TerrainTile.NORMAL || map[x + w][y + h] == TerrainTile.ROAD) opens++;
					}
				}
				//System.out.println("W: " + w + " H: " + h + " Cows: " + cowG + " opens: " + opens);
				if(cowG >= (squ - sizeFactor) && opens > (squ - sizeFactor)) {
					topTowerLocations[counter++] = new MapLocation(w + 2, h + 2);
					//goodTowerSpots.add(new MapLocation(w + 2, h + 2));
					//System.out.println("Added: ");
				} else {
					//System.out.println("No Added: ");
				}
				cowG = 0;
				opens = 0;
			}
		}

		//System.out.println("Good Map Locations: " + "Count: " + counter + " BC: " + Util.stopCounter());
	}
	

	public TerrainTile[][] getCurrentMap() {
		return map;
	}

}
