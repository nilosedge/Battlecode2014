package milker_04;

import java.util.ArrayList;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.TerrainTile;


public class BroadcastSystem {
	
	public static BaseRobot robot;
	public static RobotController rc;
	public static int WIDTH = 0;
	public static int HEIGHT = 0;
	
	public static final int lowJobMessage = 1000;
	public static final int highJobMessage = 1050;
	
	public static final int jboMessagePayload = 10000; // top end will be 10500
	
	public static final int pastrPointsOffset = 59800;
	public static final int mapOffset = 60000;

	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
		WIDTH = DataCache.mapWidth;
		HEIGHT = DataCache.mapHeight;
	}

	public static void sendMap(TerrainTile[][] map) throws GameActionException {
		
		int array[] = new int[(HEIGHT * WIDTH / 16) + 1];
		int count = 0;
		int idx = 0;
		int mod = 0;
		for(int i = 0; i < HEIGHT; i++) {
			for(int j = 0; j < WIDTH; j++) {
				idx = count / 16;
				mod = count % 16;
				array[idx] |= map[j][i].ordinal() << (mod * 2);
				count++;
			}
		}

		rc.broadcast(mapOffset, array.length);
		int i = 0;
		while(i < array.length) {
			rc.broadcast(i + mapOffset + 1, array[i++]);
		}
	}
	
	public static TerrainTile[][] readMap() throws GameActionException {
		TerrainTile[][] map = new TerrainTile[WIDTH][HEIGHT];
		int mapSize = rc.readBroadcast(mapOffset);
		int count = 0;
		for(int i = 0; i < mapSize; i++) {
			int data = rc.readBroadcast(i + mapOffset + 1);
			for(int j = 0; j < 16; j++) {
				map[count % WIDTH][(count / HEIGHT)] = TerrainTile.values()[(3 & (data >> (j * 2)))];
				count++;
				if(count >= (HEIGHT * WIDTH)) {
					break;
				}
			}
			if(count >= (HEIGHT * WIDTH)) {
				break;
			}
		}
		return map;
	}

	public static void sendGoodMapSpots(ArrayList<MapLocation> goodTowerLocations) throws GameActionException {
		rc.broadcast(pastrPointsOffset, goodTowerLocations.size());
		int send = 0;
		for(int i = 0; i < goodTowerLocations.size(); i++) {
			send = Util.convertMapLocationToInt(goodTowerLocations.get(i));
			rc.broadcast(i + pastrPointsOffset + 1, send);
		}
	}
	
	public static ArrayList<MapLocation> readGoodMapSpots() throws GameActionException {
		ArrayList<MapLocation> locs = new ArrayList<MapLocation>();
		int size = rc.readBroadcast(pastrPointsOffset);
		for(int i = 0; i < size; i++) {
			int data = rc.readBroadcast(i + pastrPointsOffset + 1);
			locs.add(Util.convertIntToMapLocation(data));
		}
		return locs;
	}

	public static void sendJobMessage(JobMessage jm) throws GameActionException {
		int broadcastCounter = 0;
		for(int i = lowJobMessage; i < highJobMessage; i++) {
			//System.out.println("Header at i: " + i + " Header: " + rc.readBroadcast(i));
			if(rc.readBroadcast(i) == 0) {
				if(broadcastCounter == DataCache.broadcastOffset) {
					jm.setIndex(i - lowJobMessage);
					jm.setRoundNumber(Clock.getRoundNum());
					int offset = (i * 10);
					rc.broadcast(offset, jm.header);
					for(int k = 0; k < jm.body.length; k++) {
						rc.broadcast(offset + k + 1, jm.body[k]);
					}
					rc.broadcast(i, jm.header);
					//System.out.println("Place Message at location: " + i);
					//Util.printJobMessage(jm, true);
					DataCache.broadcastOffset++;
					return;
				} else {
					broadcastCounter++;
				}
			}
		}
	}
	
	public static JobMessage getJobMessage() throws GameActionException {
		JobMessage jm = null;
		for(int i = lowJobMessage; i < highJobMessage; i++) {
			jm = new JobMessage(rc.readBroadcast(i));
			if(jm.header != 0 && !jm.isAccepted()) {
				jm.setAccepted(true);
				rc.broadcast(i, jm.header);
				for(int k = 0; k < jm.body.length; k++) {
					jm.body[k] = rc.readBroadcast((i * 10) + k + 1);
				}
				return jm;
			} else {
				jm = null;
			}
		}
		return jm;
	}

	public static JobMessage getAllInJob() {
		
		return null;
	}

	public static void jobCompleted(JobMessage currentJob) throws GameActionException {
		rc.broadcast(currentJob.getIndex(), currentJob.header);
	}
}
