package milker_04;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class StrategySoldierLarge implements Strategy {

	public SoldierRobot soldierRobot;
	public RobotController rc;
	
	public boolean first = false;
	public boolean second = false;
	
	public StrategySoldierLarge(SoldierRobot soldierRobot) throws GameActionException {
		this.soldierRobot = soldierRobot;
		this.rc = soldierRobot.rc;
		
		if(Clock.getRoundNum() < 5) {
			first = true;
			// Generate best cow spot
			DataCache.initTopTowerLocations();
			
			// Goto best cow spot
			System.out.println("Generating noise tower");
			rc.construct(RobotType.NOISETOWER);
		} else if(Clock.getRoundNum() < 35) {
			second = true;
			//rc.construct(RobotType.NOISETOWER);
			// This takes 37 rounds to complete
			DataCache.generateMap();
			BroadcastSystem.sendMap(DataCache.map);
			//BroadcastSystem.sendGoodMapSpots(map.topTowerLocations);
			// Goto Best Cow Spot
			rc.construct(RobotType.PASTR);
		}
	}

	@Override
	public void run() {
		if(first || second) return;
		try {
			Util.startCounter();
			DataCache.updateRoundVariables();
			
//			JobMessage allInJob = BroadcastSystem.getAllInJob();
//			if(allInJob != null) {
//				soldierState = SoldierState.ALL_IN;
//			}
			
			if(soldierRobot.currentJob == null) {
				soldierRobot.currentJob = BroadcastSystem.getJobMessage();
				soldierRobot.soldierState = SoldierState.NEW;
			}
//			if(currentJob == null) {
//				soldierState = SoldierState.HAVOCING;
//				NavSystem.addWayPointAndMoveCloser(Util.getPathCenterSlightlyCloserToUs());
//			}
			
			
			
//			System.out.println("Before Num: " + Clock.getRoundNum() + " BC: " + Clock.getBytecodeNum());
//			rallyPoint = getClosestPastrMapLocation(DataCache.buildPastrLocations);
//			System.out.println("After Num: " + Clock.getRoundNum() + " BC: " + Clock.getBytecodeNum());
			
//			
//			if(rallyPoint == null) {
//				NavSystem.addWayPointAndMoveCloser(Util.getPathCenterSlightlyCloserToUs());
//			} else {
//				NavSystem.addWayPointAndMoveCloser(rallyPoint);
//			}
			
			soldierRobot.rc.setIndicatorString(0, "Soldier State: " + soldierRobot.soldierState.toString());
			switch(soldierRobot.soldierState) {
				case NEW:
					soldierRobot.newCode();
					break;
				case HERDING:
					soldierRobot.herdingCode();
					break;
				case ALL_IN:
					soldierRobot.allInCode();
					break;
				case BUILDING:
					soldierRobot.buildCode();
					break;
				case FIGHTING:
					soldierRobot.fightingCode();
					break;
				case HAVOCING:
					soldierRobot.havocingCode();
					break;
				case RIDEOUT:
					soldierRobot.rideoutCode();
					break;
				case DEFENDING:
					soldierRobot.defendingCode();
					break;
				case TRAVELING:
					soldierRobot.travelingCode();
					break;
				default:
					break;
			}
			
			if(soldierRobot.nextSoldierState != null) {
				soldierRobot.soldierState = soldierRobot.nextSoldierState;
				soldierRobot.nextSoldierState = null; // clear the state for the next call of run() to use
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
