package milker_04;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class HQRobot extends BaseRobot {

	public Strategy s;
	
	public HQRobot(RobotController rc) throws GameActionException {
		super(rc);

		if(DataCache.isLargeMap()) {
			s = new StrategyHQLarge(this);
		} else if(DataCache.isMediumMap()) {
			s = new StrategyHQMedium(this);
		} else {
			s = new StrategyHQSmall(this);
		}
	}

	public void run() {
		try {
			DataCache.updateRoundVariables();
			
			Robot[] enemiesList = rc.senseNearbyGameObjects(Robot.class, 100000, rc.getTeam().opponent());
			int[] closestEnemyInfo = getClosestEnemy(enemiesList);
			MapLocation closestEnemyLocation = new MapLocation(closestEnemyInfo[1], closestEnemyInfo[2]);
			
			if(rc.canAttackSquare(closestEnemyLocation) && rc.isActive()) {
				rc.attackSquare(closestEnemyLocation);
			}
			
			s.run();
			
			spawn();
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}

	private void maintainJobs() {
		// Clear any jobs that are completed
		// Reassign Jobs that are old  > (roundNum - 100)
		
	}

	private void buildTowerCode() throws GameActionException {
		
		//maintainTowers();
		if(Clock.getRoundNum() % 100 == 0) {
			if(DataCache.ourPastrLocations.length < DataCache.idealPastrCount - 1) {
				int index = getClosestPastrMapLocationIndex(DataCache.topTowerLocations);
				JobMessage jm = JobSystem.createBuildMessage(RobotType.PASTR, DataCache.topTowerLocations[index]);
				BroadcastSystem.sendJobMessage(jm);
			}
		}
		
	}

	private void spawn() throws GameActionException {
		Direction dir = rc.getLocation().directionTo(DataCache.enemyHQLocation);
		int desiredDirOffset = dir.ordinal();
		for (int i = 0; i < DataCache.directionArray.length; i++) {
			int dirOffset = DataCache.directionArray[i].ordinal();
			Direction currentDirection = DataCache.directionArray[(desiredDirOffset + dirOffset + 8) % 8];
			if (rc.isActive() && rc.canMove(currentDirection) && rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
				rc.spawn(currentDirection);
				return;
			}
		}
	}
	
	public int getClosestPastrMapLocationIndex(MapLocation[] buildPastrLocations) {
		int closestDist = 10000;
		int index = 0;
		MapLocation myLoc = DataCache.ourHQLocation;
		int dist = 0;
		for(int i = 0; i < buildPastrLocations.length && buildPastrLocations[i] != null; i++) {
			MapLocation loc = buildPastrLocations[i];
			dist = loc.distanceSquaredTo(myLoc);
			if(dist < closestDist) {
				boolean found = false;
				for(int k = 0; k < DataCache.ourPastrLocations.length; k++) {
					if(DataCache.ourPastrLocations[k].distanceSquaredTo(loc) < 5) {
						found = true;
					}
				}
				if(!found) {
					// We havent build there already
					closestDist = dist;
					index = i;
				}
			}
			//System.out.println("Location: " + loc + " Dist: " + dist);
		}
		return index;
	}

	public int[] getClosestEnemy(Robot[] enemyRobots) throws GameActionException {
		
		MapLocation closestEnemy=rc.senseEnemyHQLocation(); // default to HQ
		//MapLocation closestEnemy = rc.senseHQLocation();
		int closestDist = 10000;
		
		int dist = 0;
		for (int i = enemyRobots.length; --i >= 0; ) {
			RobotInfo arobotInfo = rc.senseRobotInfo(enemyRobots[i]);
			if(arobotInfo.type != RobotType.HQ) {
				dist = arobotInfo.location.distanceSquaredTo(rc.getLocation());
				if(dist < closestDist) {
					closestDist = dist;
					closestEnemy = arobotInfo.location;
				}
			}
		}
		int[] output = new int[4];
		output[0] = closestDist;
		output[1] = closestEnemy.x;
		output[2] = closestEnemy.y;
		rc.setIndicatorString(2, "Closest: " + output[0] + ", " + output[1] + ", " + output[2] + ", " + output[3]);
		return output;
	}
}