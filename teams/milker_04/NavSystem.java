package milker_04;

import java.util.ArrayList;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class NavSystem {
	
	public static SoldierRobot robot;
	public static RobotController rc;
	
	public static int[] directionOffsets;
	
	//public static MapLocation currentWaypoint;
	public static MapLocation lastLocation;
	public static Direction lastDir;
	public static Direction wantDir;
	//private static MapLocation destination;
	private static ArrayList<MapLocation> path = new ArrayList<MapLocation>();
	
	public static void init(SoldierRobot myRobot) throws GameActionException {
		robot = myRobot;
		rc = robot.rc;
		lastLocation = rc.getLocation();
		// Randomly assign soldiers priorities for trying to move left or right
		if (rc.getRobot().getID() % 4 <= 1) {
			directionOffsets = new int[]{0,1,-1,2,-2,3,-3};
		} else {
			directionOffsets = new int[]{0,-1,1,-2,2,-3,3};
		}

	}

	public static boolean goToLocation(MapLocation location, boolean sneak) throws GameActionException {
		MapLocation myLoc = rc.getLocation();
		Direction dir = myLoc.directionTo(location);
//		System.out.println("Current Want Direction: " + dir);
//		System.out.println("Last Want Direction: " + wantDir);
//		System.out.println("Last Direction actually Traveled: " + lastDir);
//		System.out.println("Direction oppisite of last tile: " + myLoc.directionTo(lastLocation));
//		System.out.println("Current Location: " + myLoc);
//		System.out.println("Last Location: " + lastLocation);
		
		wantDir = dir;
		//System.out.println(dir);
		if(myLoc.directionTo(lastLocation).opposite() == lastDir && (dir != Direction.OMNI && !rc.canMove(dir))) {
			if (dir != Direction.OMNI && dir != Direction.NONE) {
				return goDirection(myLoc, lastDir, sneak);
			}
		} else {
			if (dir != Direction.OMNI && dir != Direction.NONE) {
				return goDirection(myLoc, dir, sneak);
			}
		}
		return false;
	}
	
	public static boolean goAwayFromLocation(MapLocation location, boolean sneak) throws GameActionException {
		MapLocation myLoc = rc.getLocation();
		Direction dir = myLoc.directionTo(location);
		if (dir != Direction.OMNI) {
			return goDirection(myLoc, dir, sneak);
		}
		return false;
	}
	
	private static boolean goDirection(MapLocation myLoc, Direction dir, boolean sneak) throws GameActionException {
		Direction lookingAtCurrently = dir;
		boolean moved = false;
		//System.out.println("Attempting to move: " + dir);
		for (int d : directionOffsets) {
			lookingAtCurrently = DataCache.directionArray[(dir.ordinal() + d + 8) % 8];
			if(sneak) {
				moved = sneak(myLoc, lookingAtCurrently);
			} else {
				moved = move(myLoc, lookingAtCurrently);
			}
			if(moved) {
				break;
			}
		}
		return moved;
	}
	
	private static boolean move(MapLocation myLoc, Direction dir) throws GameActionException {
		if (rc.isActive()) {
			if (rc.canMove(dir)) {
				lastLocation = myLoc;
				lastDir = dir;
				rc.move(dir);
				return true;
			}
			return false;
		}
		return false;
	}
	
	private static boolean sneak(MapLocation myLoc, Direction dir) throws GameActionException {
		if (rc.isActive()) {
			if (rc.canMove(dir)) {
				lastLocation = myLoc;
				lastDir = dir;
				rc.sneak(dir);
				return true;
			} 
			return false;
		}
		return false;
	}
	
	public static void addWayPointAndMoveCloser(ArrayList<MapLocation> locs) {
		path.addAll(locs);
	}
	
	public static void addWayPointAndMoveCloser(MapLocation point) {
		path.add(point);
	}
	
	public static boolean tryMoveCloser() throws GameActionException {
		boolean moved = false;
		if(path.size() > 0) {
			//rc.setIndicatorString(1, "Moving towards: " + path.get(0));
			moved = goToLocation(path.get(0), false);
			if(rc.getLocation().distanceSquaredTo(path.get(0)) <= Constants.WAYPOINT_SQUARED_DISTANCE_CHECK.getValue()) path.remove(0);
		}
		return moved;
	}

	public static boolean atDestination() {
		if(path.size() == 0) {
			return true;
		}
		return false;
	}

	public static ArrayList<MapLocation> getWayPoints(MapLocation source, MapLocation dest, int amount) {
		return new ArrayList<MapLocation>();
		//return null;
	}
}