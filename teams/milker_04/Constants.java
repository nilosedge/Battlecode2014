package milker_04;

public enum Constants {
	
	// The squared distance away from a waypoint at which we can safely go onto the next waypoint
	WAYPOINT_SQUARED_DISTANCE_CHECK("WAYPOINT_SQUARED_DISTANCE_CHECK", 2) { },
	
	
	// how frequently we change the channels we use for broadcasting
	// var = n means channel will cycle every n turns
	// IMPORTANT: YOU MUST RUN THE MAIN FUNCTION IN BROADCASTSYSTEM.JAVA IF YOU CHANGE THIS CONSTANT
	// IMPORTANT: ADDITIONALLY, MAKE SURE TO ADJUST MAX_PRECOMPUTED_ROUNDS SO JAVA DOES NOT COMPLAIN
	//public static final int CHANNEL_CYCLE = 17;	
	
	// the maximum number of precomputed rounds of channels (if too high, Java will spit out wrong numbers)
	//public static final int MAX_PRECOMPUTED_ROUNDS = 2500;
	
	// used primarily for broadcasting in EncampmentJobSystem (resetting channels)
	//public static final int MAX_MESSAGE = 0xFFFFFF;
	

	
	/**
	 * Smart Waypoints
	 */
	
	// How far off we should look each time we need to calculate a new waypoint
	//public static final int PATH_OFFSET_RADIUS = 4;
	
	// How large of a circle we should be checking each time we calculate a new waypoint
	//public static final int PATH_CHECK_RADIUS = 3;
	
	// The squared radius distance at which we stop over-valuing mines, and just go straight for the endLocation
	//public static final int PATH_GO_ALL_IN_SQ_RADIUS = 144;
	
	INITIAL_NUM_PASTRS_NEEDED("INITIAL_NUM_PASTRS_NEEDED", 2) { },
	;
	
	private String name;
	private int value;
	
	Constants(String name, int value) {
		this.name = name;
		this.value = value;
	}

	public String getName() { return name; }
	public int getValue() { return value; }
}