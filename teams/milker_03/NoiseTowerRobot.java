package milker_03;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.TerrainTile;

public class NoiseTowerRobot extends BaseRobot {
	
	public int rangeCounter = 0;
	public MapLocation myLoc;
	public double rotate;
	public int maxDist = 18;
	public int minDist = 7;
	public int dist = maxDist;
	public TerrainTile[][] map;
	public double eightPI = Math.PI / 8;
	public double lookRate = eightPI;
	public int rotCount = 0;

	public NoiseTowerRobot(RobotController rc) throws GameActionException {
		super(rc);
		myLoc = rc.getLocation();
		map = BroadcastSystem.readMap();
	}

	@Override
	public void run() {
		
		try {
			if(rc.isActive()) {
				rotate += lookRate;
				
			
	//			System.out.println("X: " + -Math.sin(rotate + lookRate));
	//			System.out.println("Y: " + Math.cos(rotate + lookRate));
	//			System.out.println("XD: " + -Math.sin(rotate + lookRate) * dist);
	//			System.out.println("YD: " + Math.cos(rotate + lookRate) * dist);
	//
	//			System.out.println("MyLoc: X: " + myLoc.x + " Y: " + myLoc.y);
	//			System.out.println("Checking: X: " + x + " Y: " + y);
	//			
				if(rotCount % 16 == 0) {
					dist-=2;
					if(dist < minDist) {
						dist = maxDist;
					}
				}
				 
				MapLocation attack = null;
				int localDist = dist;
				while(attack == null && localDist > 0) {
					attack = getAttackTile(rotate, localDist);
					localDist--;
				}
				rotCount++;
			
				rc.attackSquare(attack);
			}
			
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}
	
	public MapLocation getAttackTile(double rotate, int dist) {
		int x = (int)(-Math.sin(rotate + lookRate) * dist) + myLoc.x;
		int y = (int)(Math.cos(rotate + lookRate) * dist) + myLoc.y;
		try {
			TerrainTile tile = map[x][y];
			if(tile == TerrainTile.OFF_MAP || tile == TerrainTile.VOID) {
				return null;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			return null;
		}
		MapLocation loc = new MapLocation(x, y);
		if(rc.canAttackSquare(loc)) {
			return loc;
		}
		return null;
	}

}
