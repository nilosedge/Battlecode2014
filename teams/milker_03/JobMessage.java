package milker_03;

import java.util.ArrayList;

import battlecode.common.MapLocation;

public class JobMessage extends Message {
	
	// header =
	// 32 -> 1
	// 
	// Job type 32, 31
	// Build Type 30
	// Accepted 29
	// Complete 28
	// Index of the job 8 bits 27 - 20
	// Round Number 11 bits 19 - 10
	// 
	// body = 
	// 9 ints of way points
	// 0 - 4 are five waypoints to get to the job
	// 5 - 9 are four waypoints used in the job
	// each int = 0000 0000000 0000000 1111111 1111111
	
	public JobMessage() {
		
	}
	
	public JobMessage(int header) {
		this.header = header;
	}

	public enum JobType {
		UNKNOWN(0),   // 00
		HAVOCING(1),  // 01
		BUILDING(2),  // 10
		DEFENDING(3), // 11
		;
		
		private int value;
		
		JobType(int v) {
			value = v;
		}

		public static JobType valueOf(int i) {
			return JobType.values()[i];
		}
	}
	
	public enum BuildType {
		PASTR(0), // 0
		NOISE(1), // 1
		;

		private int value;
		BuildType(int v) {
			value = v;
		}
		
		public static BuildType valueOf(int i) {
			return BuildType.values()[i];
		}
	}

	public ArrayList<MapLocation> getWayPointsToJob() {
		// points are 7 bits each * 9 ints
		// int 0000 0000000 0000000 1111111 1111111
		// If job type is DEFENDING the last two are our "job Points and the first 7 our the way point points
		ArrayList<MapLocation> points = new ArrayList<MapLocation>();
		for(int i: body) {
			if(i != 0) {
				points.add(Util.convertIntToMapLocation(i));
			}
		}
		return points;
	}
	
	public void setWayPointsToJob(ArrayList<MapLocation> points) {
		for(int i = 0; i < points.size(); i++) {
			body[i] = Util.convertMapLocationToInt(points.get(i));
		}
	}

	public ArrayList<MapLocation> getJobPoints() {
		ArrayList<MapLocation> points = new ArrayList<MapLocation>();
		for(int i = 5; i < body.length; i++) {
			if(body[i] != 0) {
				points.add(Util.convertIntToMapLocation(body[i]));
			}
		}
		return points;
	}
	
	public void setJobPoints(ArrayList<MapLocation> points) {
		for(int i = 0; i < points.size(); i++) {
			body[i + 5] = Util.convertMapLocationToInt(points.get(i));
		}
	}

	public void setJobType(JobType type) {
		header |= (type.value << 30);
	}
	
	public JobType getJobType() {
		return JobType.valueOf((header & (3 << 30)) >>> 30);
	}
	
	public void setBuildType(BuildType type) {
		header |= (type.value << 29);
	}
	
	public BuildType getBuildType() {
		return BuildType.valueOf((header & (1 << 29)) >>> 29);
	}

	public boolean isAccepted() {
		return (header & (1 << 28)) > 0;
	}
	
	public void setAccepted(boolean accepted) {
		if(accepted) {
			header |= (1 << 28);
		} else {
			header &= ~(1 << 28);
		}
	}

	public boolean isCompleted() {
		return (header & (1 << 27)) > 0;
	}
	
	public void setCompleted(boolean completed) {
		if(completed) {
			header |= (1 << 27);
		} else {
			header &= ~(1 << 27);
		}
	}
	
	public void setIndex(int index) {
		header |= (index << 19);
	}
	
	public int getIndex() {
		return ((header & (255 << 19)) >>> 19);
	}
	
	public void setRoundNumber(int round) {
		header |= (round << 9);
	}
	
	public int getRoundNumber() {
		return ((header & (2047 << 9)) >>> 9);
	}
		
}