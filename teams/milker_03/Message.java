package milker_03;

public class Message {
	
	public int header;
	public int[] body;
	
	public Message(int header, int body[]) {
		this.header = header;
		this.body = body;
	}
	
	public Message() {
		header = 0;
		body = new int[9];
	}
}