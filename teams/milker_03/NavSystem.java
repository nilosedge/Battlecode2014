package milker_03;

import java.util.ArrayList;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class NavSystem {
	
	public static SoldierRobot robot;
	public static RobotController rc;
	
	public static int[] directionOffsets;
	
	public static MapLocation currentWaypoint;
	//private static MapLocation destination;
	private static ArrayList<MapLocation> path = new ArrayList<MapLocation>();
	
	public static void init(SoldierRobot myRobot) throws GameActionException {
		robot = myRobot;
		rc = robot.rc;
		
		// Randomly assign soldiers priorities for trying to move left or right
		if (rc.getRobot().getID() % 4 <= 1) {
			directionOffsets = new int[]{0,1,-1,2,-2,3,-3};
		} else {
			directionOffsets = new int[]{0,-1,1,-2,2,-3,3};
		}

	}

	public static boolean goToLocation(MapLocation location, boolean sneak) throws GameActionException {
		Direction dir = rc.getLocation().directionTo(location);
		if (dir != Direction.OMNI) {
			return goDirection(dir, sneak);
		}
		return false;
	}
	
	public static boolean goAwayFromLocation(MapLocation location, boolean sneak) throws GameActionException {
		Direction dir = rc.getLocation().directionTo(location).opposite();
		if (dir != Direction.OMNI) {
			return goDirection(dir, sneak);
		}
		return false;
	}
	
	private static boolean goDirection(Direction dir, boolean sneak) throws GameActionException {
		Direction lookingAtCurrently = dir;
		boolean moved = false;
		for (int d : directionOffsets) {
			lookingAtCurrently = Direction.values()[(dir.ordinal() + d + 8) % 8];
			if(sneak) {
				moved = sneak(lookingAtCurrently);
			} else {
				moved = move(lookingAtCurrently);
			}
			if(moved) {
				break;
			}
		}
		return moved;
	}
	
	private static boolean move(Direction dir) throws GameActionException {
		if (rc.isActive()) {
			if (rc.canMove(dir)) {
				rc.move(dir);
				return true;
			}
			return false;
		}
		return false;
	}
	
	private static boolean sneak(Direction dir) throws GameActionException {
		if (rc.isActive()) {
			if (rc.canMove(dir)) {
				rc.sneak(dir);
				return true;
			} 
			return false;
		}
		return false;
	}
	
	public static void addWayPointAndMoveCloser(ArrayList<MapLocation> locs) {
		path.addAll(locs);
	}
	
	public static void addWayPointAndMoveCloser(MapLocation point) {
		path.add(point);
	}
	
	public static boolean tryMoveCloser() throws GameActionException {
		boolean moved = false;
		if(path.size() > 0) {
			rc.setIndicatorString(1, "Moving towards: " + path.get(0));
			moved = goToLocation(path.get(0), false);
			if(rc.getLocation().distanceSquaredTo(path.get(0)) <= Constants.WAYPOINT_SQUARED_DISTANCE_CHECK.getValue()) path.remove(0);
		}
		return moved;
	}

	public static boolean atDestination() {
		if(path.size() == 0) {
			return true;
		}
		return false;
	}
}