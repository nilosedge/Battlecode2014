package milker_03;


import java.util.ArrayList;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class JobSystem {

	public static BaseRobot robot;
	public static RobotController rc;


	public static int numPastrsNeeded;
	public static int numUnreachablePastrs;
	
	public static int pastrCount;
	public static int noiseCount;
	
	public static double supGenRatio;

	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
	}
	
	public static void initializeConstants() throws GameActionException {
		numPastrsNeeded = Constants.INITIAL_NUM_PASTRS_NEEDED.getValue(); 
		numUnreachablePastrs = 0;
		noiseCount = 0;
		pastrCount = 0;

	}

	public static JobMessage createBuildMessage(RobotType type, MapLocation mapLocation) {
		JobMessage jm = new JobMessage();
		ArrayList<MapLocation> points = new ArrayList<MapLocation>();
		points.add(mapLocation);
		
		jm.setJobType(JobMessage.JobType.BUILDING);
		jm.setWayPointsToJob(points);
		if(RobotType.PASTR == type) {
			jm.setBuildType(JobMessage.BuildType.PASTR);
		}
		if(RobotType.NOISETOWER == type) {
			jm.setBuildType(JobMessage.BuildType.NOISE);
		}
		return jm;
	}
}
