package milker_03;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class HQRobot extends BaseRobot {


	public boolean spawned = false;
	public int WIDTH;
	public int HEIGHT;
	public NavSystemMap map;
	

	public HQRobot(RobotController rc) throws GameActionException {
		super(rc);
		WIDTH = DataCache.mapWidth;
		HEIGHT = DataCache.mapHeight;
		
		System.out.println("Before: " + Clock.getRoundNum());
		map = new NavSystemMap(rc);
		map.initMap();
		System.out.println("Middle: " + Clock.getRoundNum());
		map.initTopTowerLocations();
		//map.printCowMap(DataCache.cowGrowth);
		System.out.println("After: " + Clock.getRoundNum());
		DataCache.buildPastrLocations = map.topTowerLocations;
		
		BroadcastSystem.sendMap(map.getCurrentMap());
		//BroadcastSystem.sendGoodMapSpots(m.getGoodTowerLocations());
	}

	public void run() {
		try {
			DataCache.updateRoundVariables();

			// Build initial towers
			
			//maintainJobs();
			
			//buildTowerCode();
			
			

			
			
			
			
			// build some defences rallying around 1/3 at round 450 charge
			// Check map size to build more tower closer to us send two defending soldiers per tower
			// send out 2 - 6 havoc's
			// send out three solders per tower assigning a band channel
			
			//
			
			// Build small map 2 towser
			// medium 3 towers
			// large 6 towers

			if(rc.senseRobotCount() == 0) {
				if(DataCache.ourPastrLocations.length < DataCache.idealPastrCount - 1) {
					int index = getClosestPastrMapLocationIndex(DataCache.buildPastrLocations);
					JobMessage jm = JobSystem.createBuildMessage(RobotType.NOISETOWER, DataCache.buildPastrLocations[index]);
					BroadcastSystem.sendJobMessage(jm);
					jm = JobSystem.createBuildMessage(RobotType.PASTR, DataCache.buildPastrLocations[index]);
					BroadcastSystem.sendJobMessage(jm);
				}
			}
			//if(rc.senseRobotCount() < 2) {
				spawn();
			//}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void maintainJobs() {
		// Clear any jobs that are completed
		// Reassign Jobs that are old  > (roundNum - 100)
		
	}

	private void buildTowerCode() throws GameActionException {
		
		//maintainTowers();
		
		if(DataCache.ourPastrLocations.length < DataCache.idealPastrCount - 1) {
			int index = getClosestPastrMapLocationIndex(DataCache.buildPastrLocations);
			JobMessage jm = JobSystem.createBuildMessage(RobotType.PASTR, DataCache.buildPastrLocations[index]);
			BroadcastSystem.sendJobMessage(jm);
		}
		
	}

	private void spawn() throws GameActionException {
		Direction dir = rc.getLocation().directionTo(DataCache.enemyHQLocation);
		int desiredDirOffset = dir.ordinal();
		for (int i = 0; i < DataCache.directionArray.length; i++) {
			int dirOffset = DataCache.directionArray[i].ordinal();
			Direction currentDirection = DataCache.directionArray[(desiredDirOffset + dirOffset + 8) % 8];
			if (rc.isActive() && rc.canMove(currentDirection) && rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
				rc.spawn(currentDirection);
				return;
			}
		}
	}
	
	public int getClosestPastrMapLocationIndex(MapLocation[] buildPastrLocations) {
		int closestDist = 10000;
		int index = 0;
		MapLocation myLoc = DataCache.ourHQLocation;
		int dist = 0;
		for(int i = 0; i < buildPastrLocations.length && buildPastrLocations[i] != null; i++) {
			MapLocation loc = buildPastrLocations[i];
			dist = loc.distanceSquaredTo(myLoc);
			if(dist < closestDist) {
				boolean found = false;
				for(int k = 0; k < DataCache.ourPastrLocations.length; k++) {
					if(DataCache.ourPastrLocations[k].distanceSquaredTo(loc) < 5) {
						found = true;
					}
				}
				if(!found) {
					// We havent build there already
					closestDist = dist;
					index = i;
				}
			}
			//System.out.println("Location: " + loc + " Dist: " + dist);
		}
		return index;
	}

}