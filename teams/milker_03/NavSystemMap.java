package milker_03;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.TerrainTile;


public class NavSystemMap {

	public int WIDTH;
	public int HEIGHT;

	private boolean[][] visited;
	
	public TerrainTile[][] map;
	public RobotController rc;
	public MapLocation topTowerLocations[];
	
	public NavSystemMap(RobotController rc) {
		this.rc = rc;
		WIDTH = DataCache.mapWidth;
		HEIGHT = DataCache.mapHeight;
		visited = new boolean[WIDTH][HEIGHT];
	}
	
	public void printMap() {
		printMap(map);
	}
	
	public void printMap(TerrainTile[][] map) {
		for(int k = 0; k < HEIGHT; k++) {
			for(int i = 0; i < WIDTH; i++) {
				System.out.print(map[i][k].ordinal());
			}
			System.out.println();
		}
	}

	public boolean blocked(int x, int y) {
		if(map[x][y] == TerrainTile.OFF_MAP) {
			return true;
		}
		if(map[x][y] == TerrainTile.VOID) {
			return true;
		}
		return false;
	}

	public float getCost(int sx, int sy, int tx, int ty) {
		if(map[tx][ty] == TerrainTile.ROAD) {
			return (float)0.5;
		} else {
			return 1;
		}
	}

	public int getHeightInTiles() {
		return WIDTH;
	}

	public int getWidthInTiles() {
		return HEIGHT;
	}

	public void pathFinderVisited(int x, int y) {
		visited[x][y] = true;
	}

	public void initMap() {
		map = new TerrainTile[WIDTH][HEIGHT];
		Util.startCounter();
		for(int i = 0; i < HEIGHT; i++) {
			for(int j = 0; j < WIDTH; j++) {
				map[j][i] = rc.senseTerrainTile(new MapLocation(j, i));
			}
		}
		System.out.println("Init Map Cost: " + Util.stopCounter());
	}
	
	public void initTopTowerLocations() {
		//goodTowerSpots = new ArrayList<MapLocation>();
		topTowerLocations = new MapLocation[1000];
		int cowG = 0;
		int opens = 0;
		int counter = 0;
		//Util.startCounter();
		int sizeFactor = 5;
		for(int h = 0; h < HEIGHT; h+=sizeFactor) {
			for(int w = 0; w < WIDTH; w+=sizeFactor) {
				for(int y = 0; y < sizeFactor && (y + h) < HEIGHT; y++) {
					for(int x = 0; x < sizeFactor && (x + w) < WIDTH; x++) {
						if(DataCache.cowGrowth[x + w][y + h] > 0.9) cowG++;
						if(map[x + w][y + h] == TerrainTile.NORMAL || map[x + w][y + h] == TerrainTile.ROAD) opens++;
					}
				}
				//System.out.println("W: " + w + " H: " + h + " Cows: " + cowG + " opens: " + opens);
				if(cowG >= 23 && opens > 23) {
					topTowerLocations[counter++] = new MapLocation(w + 2, h + 2);
					//goodTowerSpots.add(new MapLocation(w + 2, h + 2));
					//System.out.println("Added: ");
				} else {
					//System.out.println("No Added: ");
				}
				cowG = 0;
				opens = 0;
			}
		}
		//System.out.println("Good Map Locations: " + "Count: " + counter + " BC: " + Util.stopCounter());
	}
	

	public TerrainTile[][] getCurrentMap() {
		return map;
	}

	public void printCowMap(double[][] cowGrowth) {
		for(int k = 0; k < HEIGHT; k++) {
			for(int i = 0; i < WIDTH; i++) {
				System.out.print(cowGrowth[i][k]);
			}
			System.out.println();
		}
	}
}
