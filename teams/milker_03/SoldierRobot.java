package milker_03;

import java.util.ArrayList;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class SoldierRobot extends BaseRobot {

	public SoldierState soldierState = SoldierState.NEW;
	public SoldierState nextSoldierState;
	private SoldierState previousSoldierState;

	public MapLocation rallyPoint;
	
	public boolean debugStates = false;

	public int myBand = 100;
	public boolean recievedOrders = false;
	
	public JobMessage allInJob;
	public JobMessage currentJob;
	private MapLocation rideoutPoint;
	private MapLocation herdingPoint;
	
	
	//public NavSystemMap map;
	
	
	public SoldierRobot(RobotController rc) throws GameActionException {
		super(rc);
		
		NavSystem.init(this);
		
		rallyPoint = findRallyPoint();
		
		// Get map
		//map = new NavSystemMap(rc);
		//map.readMap();
		//map.printMap();
		
		//finder = new PathFinder(map, 500, true);
	}

	public void run() {

		try {
			DataCache.updateRoundVariables();
			
			JobMessage allInJob = BroadcastSystem.getAllInJob();
			if(allInJob != null) {
				soldierState = SoldierState.ALL_IN;
			}
			
			if(currentJob == null) {
				currentJob = BroadcastSystem.getJobMessage();
				soldierState = SoldierState.NEW;
			}
			if(currentJob == null) {
				soldierState = SoldierState.HAVOCING;
				NavSystem.addWayPointAndMoveCloser(Util.getPathCenter());
			}
			
//			System.out.println("Before Num: " + Clock.getRoundNum() + " BC: " + Clock.getBytecodeNum());
//			rallyPoint = getClosestPastrMapLocation(DataCache.buildPastrLocations);
//			System.out.println("After Num: " + Clock.getRoundNum() + " BC: " + Clock.getBytecodeNum());
//			
//			if(rallyPoint == null) {
//				NavSystem.addWayPointAndMoveCloser(Util.getPathCenterSlightlyCloserToUs());
//			} else {
//				NavSystem.addWayPointAndMoveCloser(rallyPoint);
//			}
			

			rc.setIndicatorString(0, "Soldier State: " + soldierState.toString());
			switch(soldierState) {
				case NEW:
					newCode();
					break;
				case HERDING:
					herdingCode();
					break;
				case ALL_IN:
					allInCode();
					break;
				case BUILDING:
					buildCode();
					break;
				case FIGHTING:
					fightingCode();
					break;
				case HAVOCING:
					havocingCode();
					break;
				case RIDEOUT:
					rideoutCode();
					break;
				case DEFENDING:
					defendingCode();
					break;
				case TRAVELING:
					travelingCode();
					break;
				default:
					break;
			}
			
			if(nextSoldierState != null) {
				soldierState = nextSoldierState;
				nextSoldierState = null; // clear the state for the next call of run() to use
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void newCode() throws GameActionException {
		if(debugStates) System.out.println("New Code: ");
		if(DataCache.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.NEW;
			fightingCode();
		} else {
			if(allInJob != null) {
				NavSystem.addWayPointAndMoveCloser(allInJob.getWayPointsToJob());
				allInCode();
			} else if(currentJob != null) {
				if(currentJob != null) {
					NavSystem.addWayPointAndMoveCloser(currentJob.getWayPointsToJob());
				}
				if(currentJob.getJobType() == JobMessage.JobType.HAVOCING) {
					nextSoldierState = SoldierState.HAVOCING;
					havocingCode();
				}
				if(currentJob.getJobType() == JobMessage.JobType.BUILDING) {
					nextSoldierState = SoldierState.BUILDING;
					buildCode();
				}
				if(currentJob.getJobType() == JobMessage.JobType.DEFENDING) {
					nextSoldierState = SoldierState.DEFENDING;
					defendingCode();
				}
			}
		}
		if(debugStates) System.out.println("Finished New Code: ");
	}


	private void havocingCode() throws GameActionException {
		// chances are we will have fallen into fight code before now
		if(debugStates) System.out.println("Havoc Code: ");
		//GameActionException e = new GameActionException(GameActionExceptionType.CANT_MOVE_THERE, "Test");
		//e.printStackTrace();
		// If there are enemies nearby, trigger FIGHTING SoldierState
		if(DataCache.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.HAVOCING;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				ArrayList<MapLocation> patrolPoints = currentJob.getJobPoints();
				if(patrolPoints != null) {
					NavSystem.addWayPointAndMoveCloser(patrolPoints);
					nextSoldierState = SoldierState.TRAVELING;
					previousSoldierState = SoldierState.HAVOCING;
					travelingCode();
				} else {
					BroadcastSystem.jobCompleted(currentJob);
					currentJob = null;
				}
			} else {
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.HAVOCING;
				travelingCode();
			}
		}
	}

	private void allInCode() throws GameActionException {
		if(DataCache.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.ALL_IN;
			fightingCode();
		} else {
			System.out.println("ALLIN: set to travel");
			nextSoldierState = SoldierState.TRAVELING;
			previousSoldierState = SoldierState.ALL_IN;
			travelingCode();
		}
	}

	private void defendingCode() throws GameActionException {
		if(DataCache.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.DEFENDING;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				ArrayList<MapLocation> drivePoints = currentJob.getJobPoints();
				if(drivePoints != null) {
					rideoutPoint = drivePoints.get(1);
					herdingPoint = drivePoints.get(0);
					NavSystem.addWayPointAndMoveCloser(rideoutPoint);
					nextSoldierState = SoldierState.RIDEOUT;
					rideoutCode();
				} else {
					BroadcastSystem.jobCompleted(currentJob);
					currentJob = null;
				}
			} else {
				System.out.println("DEFEND: set to travel");
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.HAVOCING;
				travelingCode();
			}
		}
	}

	private void rideoutCode() throws GameActionException {
		if(DataCache.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.RIDEOUT;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				nextSoldierState = SoldierState.HERDING;
				NavSystem.addWayPointAndMoveCloser(herdingPoint);
				herdingCode();
			} else {
				System.out.println("RIDEOUT: set to travel");
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.RIDEOUT;
				travelingCode();
			}
		}
	}

	private void herdingCode() throws GameActionException {
		if(DataCache.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.HERDING;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				nextSoldierState = SoldierState.RIDEOUT;
				NavSystem.addWayPointAndMoveCloser(rideoutPoint);
				rideoutCode();
			} else {
				System.out.println("HRED: set to travel");
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.HERDING;
				travelingCode();
			}
		}
	}
	
	
	private void buildCode() throws GameActionException {
		if(debugStates) System.out.println("Building Code: At dest: " + NavSystem.atDestination());
		if(NavSystem.atDestination()) {
			if(currentJob.getBuildType() == JobMessage.BuildType.NOISE) {
				if(rc.isActive()) {
					rc.construct(RobotType.NOISETOWER);
				}
			} else if(currentJob.getBuildType() == JobMessage.BuildType.PASTR) {
				if(rc.isActive()) {
					rc.construct(RobotType.PASTR);
				}
			}
			nextSoldierState = SoldierState.BUILDING;
		} else {
			System.out.println("BUILDING: set to travel");
			nextSoldierState = SoldierState.TRAVELING;
			previousSoldierState = SoldierState.BUILDING;
			travelingCode();
		}
	}
	
	private void travelingCode() throws GameActionException {
		if(debugStates) System.out.println("Travel Code: ");
		if(DataCache.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				System.out.println("At dest switch from: " + soldierState + " to: " + previousSoldierState);
				nextSoldierState = previousSoldierState;
			} else {
				NavSystem.tryMoveCloser();
			}
		}
		if(debugStates) System.out.println("Finished Travel Code: ");
	}

	private void fightingCode() throws GameActionException {
		if(DataCache.numNearbyEnemyRobots == 0) {
			System.out.println("Was Figthing switch from: " + soldierState + " to: " + previousSoldierState);
			nextSoldierState = previousSoldierState;
		} else {
			microCode();
		}
	}
	
	public void microCode() throws GameActionException {
		Robot[] enemiesList = rc.senseNearbyGameObjects(Robot.class, 100000, rc.getTeam().opponent());
		int[] closestEnemyInfo = getClosestEnemy(enemiesList);
		MapLocation closestEnemyLocation = new MapLocation(closestEnemyInfo[1], closestEnemyInfo[2]);
		int enemyDistSquared = closestEnemyLocation.distanceSquaredTo(rc.getLocation());

		if(rc.canAttackSquare(closestEnemyLocation) && rc.isActive() && rc.getActionDelay() <= 0) {
			rc.attackSquare(closestEnemyLocation);
		}
		boolean sneaking = false;
		if(enemyDistSquared <= 2) {
			double[] our23 = getEnemies2Or3StepsAway();
			if(our23[0] < 1) {
				if(our23[1] >= 1) {
					NavSystem.goAwayFromLocation(closestEnemyLocation, sneaking);
				}
			}
		} else if(enemyDistSquared == 16 || enemyDistSquared > 18 || DataCache.numNearbyAlliedSoldiers >= 3 * DataCache.numNearbyEnemySoldiers){ // if no enemies in one, two, or three dist
			if(rc.isActive()) {
				NavSystem.goToLocation(closestEnemyLocation, sneaking);
			}
		} else {
			double[] our23 = getEnemies2Or3StepsAway();
			double[] enemy23 = getEnemies2Or3StepsAwaySquare(closestEnemyLocation, rc.getTeam().opponent());

			if(our23[1] > 0) {

				if(enemy23[0] > 0) {
					NavSystem.goToLocation(closestEnemyLocation, sneaking);
				} else {
					if(enemy23[1] + enemy23[0] > our23[1] + our23[2]+1 || our23[1] + our23[2] < 1) {
						NavSystem.goToLocation(closestEnemyLocation, sneaking);
					} else {
						NavSystem.goAwayFromLocation(closestEnemyLocation, sneaking);
					}
				}

			} else { // closest enemy is 3 dist
				if(enemy23[0] > 0) {
					NavSystem.goToLocation(closestEnemyLocation, sneaking);
				} else if(enemy23[1] > 0) { // if enemy 2dist is > 0
					int closestDist = 100;
					int dist;
					MapLocation closestAllyLocation = null;
					Robot[] twoDistAllies = rc.senseNearbyGameObjects(Robot.class, closestEnemyLocation, 8, rc.getTeam());
					for (int i = twoDistAllies.length; --i >= 0; ) {
						Robot ally = twoDistAllies[i];
						RobotInfo arobotInfo = rc.senseRobotInfo(ally);
						dist = arobotInfo.location.distanceSquaredTo(rc.getLocation());
						if(dist<closestDist){
							closestDist = dist;
							closestAllyLocation = arobotInfo.location;
						}
					}

					double[] ally23 = getEnemies2Or3StepsAwaySquare(closestAllyLocation, rc.getTeam());

					if(enemy23[0] + enemy23[1] + enemy23[2] > ally23[1] + ally23[2]) {
						NavSystem.goToLocation(closestEnemyLocation, sneaking);
					} else {
						NavSystem.goAwayFromLocation(closestEnemyLocation, sneaking);
					}
				} else {
					if(enemy23[2] - our23[2] >= 3 || our23[2] < 1) {
						NavSystem.goToLocation(closestEnemyLocation, sneaking);
					} else {
						NavSystem.goAwayFromLocation(closestEnemyLocation, sneaking);
					}
					// otherwise, stay
				}
			}
		}
	}
	


	public int[] getClosestEnemy(Robot[] enemyRobots) throws GameActionException {
		
		MapLocation closestEnemy=rc.senseEnemyHQLocation(); // default to HQ
		//MapLocation closestEnemy = rc.senseHQLocation();
		int closestDist = 10000;
		
		int dist = 0;
		for (int i = enemyRobots.length; --i >= 0; ) {
			RobotInfo arobotInfo = rc.senseRobotInfo(enemyRobots[i]);
			if(arobotInfo.type != RobotType.HQ) {
				dist = arobotInfo.location.distanceSquaredTo(rc.getLocation());
				if(dist < closestDist) {
					closestDist = dist;
					closestEnemy = arobotInfo.location;
				}
			}
		}
		int[] output = new int[4];
		output[0] = closestDist;
		output[1] = closestEnemy.x;
		output[2] = closestEnemy.y;
		rc.setIndicatorString(2, "Closest: " + output[0] + ", " + output[1] + ", " + output[2] + ", " + output[3]);
		return output;
	}
	

	private double[] getEnemies2Or3StepsAway() throws GameActionException {
		double count1 = 0;
		double count2 = 0;
		double count3 = 0;
		Robot[] enemiesInVision = rc.senseNearbyGameObjects(Robot.class, 18, rc.getTeam().opponent());
		for (int i = enemiesInVision.length; --i >= 0; ) {
			Robot enemy = enemiesInVision[i];
			RobotInfo rinfo = rc.senseRobotInfo(enemy);
			int dist = rinfo.location.distanceSquaredTo(rc.getLocation());
			if(rinfo.type == RobotType.SOLDIER) {
				if(dist <= 2) {
					count1++;
				} else if(dist <=8) {
					count2++;
				} else if(dist > 8 && (dist <= 14 || dist == 18)) {
					count3++;
				}
			} else if(rinfo.type != RobotType.HQ) {
				if(dist <= 2) {
					count1 += 0.2;
				} else if(dist <=8) {
					count2 += 0.2;
				} else if(dist > 8 && (dist <= 14 || dist == 18)) {
					count3 += 0.2;
				}
			}
		}

		double[] output = {count1, count2, count3};
		return output;
	}

	private double[] getEnemies2Or3StepsAwaySquare(MapLocation square, Team squareTeam) throws GameActionException {
		double count1 = 0;
		double count2 = 0;
		double count3 = 0;
		if(square == null) {
			double[] output = {count1, count2, count3};
			return output;
		}
		Robot[] enemiesInVision = rc.senseNearbyGameObjects(Robot.class, square, 18, squareTeam.opponent());
		for (int i = enemiesInVision.length; --i >= 0; ) {
			Robot enemy = enemiesInVision[i];
			RobotInfo rinfo = rc.senseRobotInfo(enemy);
			int dist = rinfo.location.distanceSquaredTo(square);
			if(rinfo.type == RobotType.SOLDIER) {
				if(dist <= 2) {
					count1++;
				} else if(dist <=8) {
					count2++;
				} else if(dist <= 14 || dist == 18) {
					count3++;
				}
			} else if(rinfo.type != RobotType.HQ) {
				if(dist <= 2) {
					count1 += 0.2;
				} else if(dist <=8) {
					count2 += 0.2;
				} else if(dist <= 14 || dist == 18) {
					count3 += 0.2;
				}
			}
		}

		int selfDist = square.distanceSquaredTo(rc.getLocation());
		
		if(selfDist <= 2) {
			count1++;
		} else if(selfDist <= 8) {
			count2++;
		} else if(selfDist <= 14 || selfDist == 18) {
			count3++;
		}

		double[] output = {count1, count2, count3};
		return output;
	}

	private MapLocation findRallyPoint() {
		return Util.getPathCenterSlightlyCloserToUs();
	}
	
}
