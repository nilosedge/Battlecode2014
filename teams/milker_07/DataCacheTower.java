package milker_07;

import battlecode.common.GameActionException;

public class DataCacheTower extends DataCache {
	
	public static boolean haveMap = false;

	public static void updateRoundVariables(boolean isPastr) throws GameActionException {

		
		if(isPastr) {
			BroadcastSystem.updatePastrCount();
		} else {
			BroadcastSystem.updateNoiseCount();
		}
		
		if(!haveMap) {
			if(BroadcastSystem.isMapComplete()) {
				map = BroadcastSystem.readMap();
				haveMap = true;
			}
		}
	}
}
