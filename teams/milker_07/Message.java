package milker_07;

public class Message {
	
	public int header;
	public int[] body;
	public int bodySize = 9;
	
	public Message(int header, int body[]) {
		this.header = header;
		this.body = body;
	}
	
	public Message() {
		header = 0;
		body = new int[bodySize];
	}
}