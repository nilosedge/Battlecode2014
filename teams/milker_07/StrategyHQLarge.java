package milker_07;

import java.util.ArrayList;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class StrategyHQLarge implements Strategy {

	private HQRobot hqRobot;
	private RobotController rc;
	
	// VARIABLE for out desired stats

	public StrategyHQLarge(HQRobot hqRobot) {
		this.hqRobot = hqRobot;
		this.rc = hqRobot.rc;
	}

	@Override
	public void run() {
		try {
			
			if(Clock.getRoundNum() == 0) {
				hqRobot.generateMapJob();
				hqRobot.travelJob(DataCache.ourHQLocation.add(DataCache.ourHQLocation.directionTo(DataCache.enemyHQLocation), 5), JobMessage.BroadCastType.GLOBAL);
			}
			
			ArrayList<JobMessage> jobs = BroadcastSystem.getJobMessagesFromRobots();
			
			for(JobMessage jm: jobs) {
				Util.debug_printJobMessage(jm, false, "HQ Jobs Founds from Robots: ");
				
				if(jm.getJobType() == JobMessage.JobType.MESSAGE) {
					if(jm.getMessageType() == JobMessage.MessageType.MAPCOMPLETE) {
						hqRobot.generateComputeGoodSpotsJob();
						//Util.startCounter();
						//BroadcastSystem.readMap();
						//System.out.println("Read Map Cost: " + Util.stopCounter());
					}
					if(jm.getMessageType() == JobMessage.MessageType.GOODSPOTSCOMPLETE) {
						DataCache.goodMapSpots = BroadcastSystem.readGoodMapSpots();
						MapLocation loc = DataCache.goodMapSpots.get(0);
						hqRobot.travelJob(loc.add(loc.directionTo(DataCache.enemyHQLocation), 25), JobMessage.BroadCastType.GLOBAL);
						hqRobot.buildTowerCodeJob(RobotType.NOISETOWER, loc.add(loc.directionTo(DataCache.enemyHQLocation)));
						hqRobot.buildTowerCodeJob(RobotType.PASTR, loc);
						loc = DataCache.goodMapSpots.get(1);
						hqRobot.travelJob(loc.add(loc.directionTo(DataCache.enemyHQLocation), 25), JobMessage.BroadCastType.GLOBAL);
						hqRobot.buildTowerCodeJob(RobotType.NOISETOWER, loc.add(loc.directionTo(DataCache.enemyHQLocation)));
						hqRobot.buildTowerCodeJob(RobotType.PASTR, loc);

					}
				}
			}
		

			// check that we need to build towers
			// check that we need to build pastrs
			// every 50 rounds or so broad defend job of all our towers to band 0
		
			// IF we build a second pastr defend between the two if they are not that far apart
			
			// TODO if we loose a tower rebuild and defend
			// TODO if they are a head of us send all in message and then two pastrs to see if we can catch up
			
			// If he has more pastrs then us we need to do something about it
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}

}
