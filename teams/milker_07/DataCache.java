package milker_07;

import java.util.ArrayList;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import battlecode.common.TerrainTile;

public class DataCache {

	public static BaseRobot robot;
	public static RobotController rc;
	
	public static MapLocation ourHQLocation;
	public static MapLocation enemyHQLocation;
	
	public static RobotInfo closestEnemyInfo;
	public static RobotInfo weakestEnemyInfo;
	
	public static int rushDistSquared;
	public static int rushDist;
	
	public static Direction[] directionArray = Direction.values();
	
	// Map width
	public static int mapWidth;
	public static int mapHeight;
	
	// Round variables - army sizes
	// Allied robots
	
	//public static int numAlliedRobots;
	//public static int numAlliedSoldiers;
	//public static int numAlliedPastrs;
	
	//public static int numNearbyAlliedRobots;	
	//public static int numNearbyAlliedSoldiers;
	//public static int numNearbyAlliedPastrs;

	public static Team myTeam;
	public static Team enemy;
	public static RobotType myType;
	public static RobotInfo myInfo;
	
	// Enemy robots
	//public static int numEnemyRobots;
	
	//public static Robot[] enemiesList;
	//public static ArrayList<RobotInfo> enemiesListInfos = new ArrayList<RobotInfo>();
	//public static Robot[] nearbyEnemyRobots;
	public static int numNearbyEnemyRobots;
	public static int numNearbyEnemySoldiers;
	
	//public static Robot[] friendlyList;
	//public static ArrayList<RobotInfo> friendlyListInfos = new ArrayList<RobotInfo>();
	//public static Robot[] nearbyFriendlyRobots;
	public static int numNearbyFriendlyRobots;
	public static int numNearbyFriendlySoldiers;
	
	public static ArrayList<MapLocation> goodMapSpots;
	public static double[][] cowGrowth;


	public static TerrainTile[][] map = null;
	

	//public static int roundNumber = 0;
	//public static int broadcastOffset = 0;

	
	
	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
		
		myTeam = rc.getTeam();
		enemy = myTeam.opponent();
		myType = rc.getType();
		
		ourHQLocation = rc.senseHQLocation();
		enemyHQLocation = rc.senseEnemyHQLocation();
		rushDistSquared = ourHQLocation.distanceSquaredTo(enemyHQLocation);
		rushDist = (int) Math.sqrt(rushDistSquared);
		cowGrowth = rc.senseCowGrowth();
		mapWidth = rc.getMapWidth();
		mapHeight = rc.getMapHeight();

	}

	public static boolean isLargeMap() {
		return (mapHeight * mapWidth > 2500);
	}

	public static boolean isMediumMap() {
		return (mapHeight * mapWidth > 1200);
	}
	
	public static void generateMap() throws GameActionException {
		map = new TerrainTile[mapWidth][mapHeight];
		for(int i = 0; i < mapHeight; i++) {
			for(int j = 0; j < mapWidth; j++) {
				map[j][i] = rc.senseTerrainTile(new MapLocation(j, i));
			}
		}
	}

	public void debug_printCowMap(double[][] cowGrowth) {
		for(int k = 0; k < mapHeight; k++) {
			for(int i = 0; i < mapWidth; i++) {
				System.out.print(cowGrowth[i][k]);
			}
			System.out.println("");
		}
	}
	
	public void debug_printMap() {
		debug_printMap(map);
	}
	
	public void debug_printMap(TerrainTile[][] map) {
		for(int k = 0; k < mapHeight; k++) {
			for(int i = 0; i < mapWidth; i++) {
				System.out.print(map[i][k].ordinal());
			}
			System.out.println();
		}
	}
	
	public static void generateGoodTowerLocations() {
		

		goodMapSpots = new ArrayList<MapLocation>();
		//goodMapSpots.add(DataCache.ourHQLocation.add(DataCache.enemyHQLocation.directionTo(DataCache.ourHQLocation)));
		
		int cowG = 0;
		int opens = 0;
		int sizeFactor = 25;
		if(isLargeMap()) {
			sizeFactor = 25;
		}
		if(isMediumMap()) {
			sizeFactor = 12;
		}
		sizeFactor = 8;
		double percent = .80;
		int squ = sizeFactor * sizeFactor;
		for(int h = 0; h < mapHeight; h+=sizeFactor) {
			for(int w = 0; w < mapWidth; w+=sizeFactor) {
				for(int y = 0; y < sizeFactor && (y + h) < mapHeight; y++) {
					for(int x = 0; x < sizeFactor && (x + w) < mapWidth; x++) {
						if(DataCacheSoldier.cowGrowth[x + w][y + h] > 0.9) cowG += DataCacheSoldier.cowGrowth[x + w][y + h];
						if(map[x + w][y + h] == TerrainTile.NORMAL || map[x + w][y + h] == TerrainTile.ROAD) opens++;
					}
				}
				//System.out.println("W: " + w + " H: " + h + " Cows: " + cowG + " opens: " + opens);
				if(cowG >= (squ * percent) && opens >= (squ * percent)) {
				//if(cowG >= (squ * percent) && opens > (squ - sizeFactor)) {
					goodMapSpots.add(new MapLocation(w + (sizeFactor / 2), h + (sizeFactor / 2)));
					//goodTowerSpots.add(new MapLocation(w + 2, h + 2));
					//System.out.println("Added: ");
				} else {
					//System.out.println("No Added: ");
				}
				cowG = 0;
				opens = 0;
			}
		}
		// TODO sort by distance to HQ
		// TODO make this really smart
		// Sort order needs to be fixed
		//System.out.println("Good Map Locations: " + "Count: " + counter + " BC: " + Util.stopCounter());
	}

	public static void updateSoldierInfos() throws GameActionException {

		Util.startCounter();
		int closestDist = 10000000;
		
		int dist = 0;
		double weakest = Math.pow(2, 32);
		MapLocation myLoc = rc.getLocation();
		
		closestEnemyInfo = null;
		weakestEnemyInfo = null;
		
		//friendlyListInfos.clear();
		//enemiesListInfos.clear();
		
		//myInfo = rc.senseRobotInfo(rc.getRobot());
		
		numNearbyEnemySoldiers = 0;
		numNearbyEnemyRobots = 0;
		numNearbyFriendlySoldiers = 0;
		numNearbyFriendlyRobots = 0;
		
		Robot[] allRobots = rc.senseNearbyGameObjects(Robot.class, 45);
		for (int i = allRobots.length; --i >= 0; ) {
			
			if(allRobots[i].getTeam() == myTeam) {
				numNearbyFriendlyRobots++;
				//friendlyListInfos.add(robotInfo);
				//if (robotInfo.type == RobotType.SOLDIER) {
				//	numNearbyFriendlySoldiers++;
				//}
				
			} else {
				RobotInfo robotInfo = rc.senseRobotInfo(allRobots[i]);
				numNearbyEnemyRobots++;
				if(robotInfo.type != RobotType.NOISETOWER) {
					
					dist = myLoc.distanceSquaredTo(robotInfo.location);
					if(dist < closestDist) {
						closestDist = dist;
						closestEnemyInfo = robotInfo;
					}
					if(robotInfo.health < weakest) {
						weakest = robotInfo.health;
						weakestEnemyInfo = robotInfo;
					}
				}
				
				//enemiesListInfos.add(robotInfo);
				if (robotInfo.type == RobotType.SOLDIER) {
					numNearbyEnemySoldiers++;
				}
			}
		}
		
		rc.setIndicatorString(1, "Loop Cost: " + Util.stopCounter());
		
	}


}
