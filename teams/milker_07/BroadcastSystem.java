package milker_07;

import java.util.ArrayList;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import battlecode.common.TerrainTile;


public class BroadcastSystem {
	
	public static BaseRobot robot;
	public static RobotController rc;
	public static int WIDTH = 0;
	public static int HEIGHT = 0;
	
	public static final int jobMessageLowOffset = 10000;
	public static final int jobMessageHQLowOffset = 30000;

	public static final int robotDataIndex = 1100;
	public static final int latestJobMessageFromHQIndex = 1101;
	public static final int latestJobMessageToHQIndex = 1102;
	
	public static       int lastReadJobMessageIndex = 0;
	public static       int lastSendJobMessageIndex = 0;
	
	public static final int countOffset01 = 1103;

	public static final int mapComplete = 1104;
	
	public static final int bandChanelBase = 1110; // Used for bands 0 - 4 they will add their band offset to this
		
	public static final int ourPastrLocationsOffset = 1200;
	

	
	public static final int pastrPointsOffset = 59800;
	public static final int mapOffset = 60000;
	
	public static final boolean debug = false;

	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
		WIDTH = DataCache.mapWidth;
		HEIGHT = DataCache.mapHeight;
		if(rc.getType() == RobotType.HQ) {
			lastReadJobMessageIndex = jobMessageHQLowOffset;
			lastSendJobMessageIndex = jobMessageLowOffset;
		} else {
			lastReadJobMessageIndex = jobMessageLowOffset;
		}
	}

	public static void sendMap(TerrainTile[][] map) throws GameActionException {
		
		int array[] = new int[(HEIGHT * WIDTH / 16) + 1];
		int count = 0;
		int idx = 0;
		int mod = 0;
		for(int i = 0; i < HEIGHT; i++) {
			for(int j = 0; j < WIDTH; j++) {
				idx = count / 16;
				mod = count % 16;
				array[idx] |= map[j][i].ordinal() << (mod * 2);
				count++;
			}
		}

		rc.broadcast(mapOffset, array.length);
		int i = 0;
		while(i < array.length) {
			rc.broadcast(i + mapOffset + 1, array[i++]);
		}
	}
	
	public static TerrainTile[][] readMap() throws GameActionException {
		TerrainTile[][] map = new TerrainTile[WIDTH][HEIGHT];
		int mapSize = rc.readBroadcast(mapOffset);
		int count = 0;
		for(int i = 0; i < mapSize; i++) {
			int data = rc.readBroadcast(i + mapOffset + 1);
			for(int j = 0; j < 16; j++) {
				map[count % WIDTH][(count / HEIGHT)] = TerrainTile.values()[(3 & (data >> (j * 2)))];
				count++;
				if(count >= (HEIGHT * WIDTH)) {
					break;
				}
			}
			if(count >= (HEIGHT * WIDTH)) {
				break;
			}
		}
		return map;
	}

	public static void sendGoodMapSpots(ArrayList<MapLocation> goodTowerLocations) throws GameActionException {
		rc.broadcast(pastrPointsOffset, goodTowerLocations.size());
		int send = 0;
		for(int i = 1; i <= goodTowerLocations.size(); i++) {
			send = Util.convertMapLocationToInt(goodTowerLocations.get(i - 1));
			rc.broadcast(i + pastrPointsOffset, send);
		}
	}
	
	public static ArrayList<MapLocation> readGoodMapSpots() throws GameActionException {
		ArrayList<MapLocation> locs = new ArrayList<MapLocation>();
		int size = rc.readBroadcast(pastrPointsOffset);
		for(int i = 1; i <= size; i++) {
			int data = rc.readBroadcast(i + pastrPointsOffset);
			locs.add(Util.convertIntToMapLocation(data));
		}
		return locs;
	}
	
	
	
	
	public static JobMessage getJobMessageFromHQ() throws GameActionException {
		JobMessage jm = null;

		lastSendJobMessageIndex = rc.readBroadcast(latestJobMessageFromHQIndex);
		
		if(lastSendJobMessageIndex - lastReadJobMessageIndex >= 50) {
			lastReadJobMessageIndex += JobMessage.jobMessageSize;
		}
		
		Util.debug_println("Send Location: " + lastSendJobMessageIndex);
		Util.debug_println("Read Location: " + lastReadJobMessageIndex);
		
		JobMessage lastGlobalJob = null;
		int lastGlobalIndex = 0;
		
		for(int i = lastReadJobMessageIndex; i < lastSendJobMessageIndex; i += JobMessage.jobMessageSize) {
			jm = new JobMessage(rc.readBroadcast(i));
			
			if(jm.getBroadcastType() == JobMessage.BroadCastType.GLOBAL) {
				lastGlobalIndex = i;
				lastGlobalJob = jm;
			}
			
			if(jm.header != 0 && !jm.isAccepted()) {
				jm.setAccepted(true);
				rc.broadcast(i, jm.header);
				int size = jm.bodySize;
				for(int j = 0; j < size; j++) {
					jm.body[j] = rc.readBroadcast(i + j + 1);
				}
				Util.debug_printJobMessage(jm, false, i + ": Read Message from HQ: ");
				lastReadJobMessageIndex = i;
				return jm;
			} else {
				jm = null;
			}
		}
		
		if(lastGlobalIndex != 0) {
			int size = lastGlobalJob.bodySize;
			for(int j = 0; j < size; j++) {
				lastGlobalJob.body[j] = rc.readBroadcast(lastGlobalIndex + j + 1);
			}
			Util.debug_printJobMessage(lastGlobalJob, false, lastGlobalIndex + ": Read Global Message from HQ: ");
			return lastGlobalJob;
		}
		
		return jm;
			
	}
	public static void sendJobMessageToHQ(JobMessage jm) throws GameActionException {
		// TODO handle the multiple send from the robots
		lastSendJobMessageIndex = rc.readBroadcast(latestJobMessageToHQIndex);
		//First Round
		if(lastSendJobMessageIndex == 0) {
			lastSendJobMessageIndex = jobMessageHQLowOffset;
			rc.broadcast(latestJobMessageToHQIndex, lastSendJobMessageIndex);	
		}
		
		postJobMessage(jm, lastSendJobMessageIndex);

		Util.debug_printJobMessage(jm, false, lastSendJobMessageIndex + ": Sending Message To HQ: ");
		rc.broadcast(latestJobMessageToHQIndex, lastSendJobMessageIndex + JobMessage.jobMessageSize);

	}



	

	// HQ Calls needs to return the whole list for this round
	public static ArrayList<JobMessage> getJobMessagesFromRobots() throws GameActionException {
		ArrayList<JobMessage> jms = new ArrayList<JobMessage>();

		int lastSentJobMessageToHQIndex = rc.readBroadcast(latestJobMessageToHQIndex);
		
		while(lastReadJobMessageIndex < lastSentJobMessageToHQIndex) {
			JobMessage jm = new JobMessage(rc.readBroadcast(lastReadJobMessageIndex));
			if(jm.header != 0) {
				for(int k = 0; k < jm.body.length; k++) {
					jm.body[k] = rc.readBroadcast(lastReadJobMessageIndex + k + 1);
				}
				jms.add(jm);
			}
			lastReadJobMessageIndex += JobMessage.jobMessageSize;
		}
		return jms;
	}
	
	// TO ROBOTS FROM HQ
	public static void sendJobMessageToRobots(JobMessage jm) throws GameActionException {
		Util.debug_printJobMessage(jm, false, lastSendJobMessageIndex + ": Sending Message To Robots: ");
		postJobMessage(jm, lastSendJobMessageIndex);
		lastSendJobMessageIndex += JobMessage.jobMessageSize;
		rc.broadcast(latestJobMessageFromHQIndex, lastSendJobMessageIndex);
	}
	
	
	
	
	public static void postJobMessage(JobMessage jm, int offset) throws GameActionException {
		rc.broadcast(offset, jm.header);
		for(int k = 0; k < jm.body.length; k++) {
			rc.broadcast(offset + k + 1, jm.body[k]);
		}
	}

	public static JobMessage getAllInJob() {
		
		return null;
	}
	
//	public static void sendRemoteData(int[] in) throws GameActionException {
//		rc.broadcast(robotData, Util.convertMapLocationToInt(new MapLocation(in[0], in[1])));
//		rc.broadcast(robotData + 1, Util.convertMapLocationToInt(new MapLocation(in[2], in[3])));
//	}

//	public static int[] readRemoteData() throws GameActionException {
//		int data[] = new int[4];
//		MapLocation d1 = Util.convertIntToMapLocation(rc.readBroadcast(robotData));
//		MapLocation d2 = Util.convertIntToMapLocation(rc.readBroadcast(robotData + 1));
//		data[0] = d1.x;
//		data[1] = d1.y;
//		data[2] = d2.x;
//		data[3] = d2.y;
//		return data;
//	}
	
//	public static MapLocation[] readOurPastrLocations() throws GameActionException {
//		int len = rc.readBroadcast(ourPastrLocationsOffset);
//		MapLocation[] locs = new MapLocation[len];
//		for(int i = 0; i < len; i++) {
//			locs[i] = Util.convertIntToMapLocation(rc.readBroadcast(i + 1));
//		}
//		return locs;
//	}
//
//	public static void sendOurPastrLocations(MapLocation[] ourPastrLocations) throws GameActionException {
//		rc.broadcast(ourPastrLocationsOffset, ourPastrLocations.length);
//		for(int i = 0; i < ourPastrLocations.length; i++) {
//			rc.broadcast(ourPastrLocationsOffset + (i + 1), Util.convertMapLocationToInt(ourPastrLocations[i]));
//		}
//	}

//	public static void maintainJobs() throws GameActionException {
//		int startIndex = rc.readBroadcast(latestJobMessage);
//		int endIndex = lastSendJobMessageIndex;
//		
//		// First Round
//		if(startIndex == 0) {
//			startIndex = jobMessageLowOffset;
//		}
//		
//		boolean main = false;
//		if(startIndex != endIndex) {
//			main = true;
//			System.out.println("Start Index: " + startIndex + " End Index: " + endIndex);
//		}
//
//		if(endIndex < startIndex) {
//			// Handle the wrap around
//		} else {
//		
//			while(startIndex <= endIndex) {
//				
//				JobMessage jm = new JobMessage(rc.readBroadcast(startIndex));
//				//Util.printJobMessage(jm, true);
//				if(jm.header != 0 && jm.isAccepted()) {
//					startIndex += JobMessage.jobMessageSize;
//				} else {
//					break;
//				}
//			}
//		}
//		rc.broadcast(latestJobMessage, startIndex);
//		if(main) System.out.println("Setting Index to: " + startIndex);
//	}

	public static void updateSolderierCount() throws GameActionException {
		int count[] = Util.unpackFourInts(rc.readBroadcast(countOffset01));
		count[1]++;
		rc.broadcast(countOffset01, Util.packFourInts(count));
	}
	
	public static void updateDefendingCount() throws GameActionException {
		int count[] = Util.unpackFourInts(rc.readBroadcast(countOffset01));
		count[0]++;
		rc.broadcast(countOffset01, Util.packFourInts(count));
	}
	
	public static void updatePastrCount() throws GameActionException {
		int count[] = Util.unpackFourInts(rc.readBroadcast(countOffset01));
		count[2]++;
		rc.broadcast(countOffset01, Util.packFourInts(count));
	}
	
	public static void updateNoiseCount() throws GameActionException {
		int count[] = Util.unpackFourInts(rc.readBroadcast(countOffset01));
		count[3]++;
		rc.broadcast(countOffset01, Util.packFourInts(count));
	}

	public static void getStatusCounts() throws GameActionException {
		
		int readBroadcast = rc.readBroadcast(countOffset01);

		DataCacheHQ.defendingCount = readBroadcast >>> 24;
		DataCacheHQ.defendingCountChanged = (DataCacheHQ.lastDefendingCount == DataCacheHQ.defendingCount);
		DataCacheHQ.lastDefendingCount =DataCacheHQ. defendingCount;
		DataCacheHQ.soldierCount = (readBroadcast >>> 16) & 255;
		DataCacheHQ.soldierCountChanged = (DataCacheHQ.lastSoldierCount == DataCacheHQ.soldierCount);
		DataCacheHQ.lastSoldierCount = DataCacheHQ.soldierCount;
		DataCacheHQ.pastrCount = (readBroadcast >>> 8) & 255;
		DataCacheHQ.pastrCountChanged = (DataCacheHQ.lastPastrCount == DataCacheHQ.pastrCount);
		DataCacheHQ.lastPastrCount = DataCacheHQ.pastrCount;
		DataCacheHQ.noiseCount = readBroadcast & 255;
		DataCacheHQ.noiseCountChanged = (DataCacheHQ.lastNoiseCount == DataCacheHQ.noiseCount);
		DataCacheHQ.lastNoiseCount = DataCacheHQ.noiseCount;

//		array = Util.unpackFourInts(rc.readBroadcast(bandChanelBase));
//		
//		counts[4] = array[0];
//		counts[5] = array[1];
//		counts[6] = array[2];
//		counts[7] = array[3];
		
		// BC 30
		clearCounters();

	}
	
	public static void clearCounters() throws GameActionException {
		rc.broadcast(countOffset01, 0);
		//rc.broadcast(bandChanelBase, 0);
	}

	public static void setMapComplete() throws GameActionException {
		rc.broadcast(mapComplete, 1);
	}
	
	public static boolean isMapComplete() throws GameActionException {
		return (rc.readBroadcast(mapComplete) > 0);
	}
}
