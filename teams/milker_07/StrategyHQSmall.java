package milker_07;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class StrategyHQSmall implements Strategy {

	int count = 0;
	private HQRobot hqRobot;
	private RobotController rc;
	
	public StrategyHQSmall(HQRobot hqRobot) {
		this.hqRobot = hqRobot;
		this.rc = hqRobot.rc;
	}

	@Override
	public void run() {
		try {
			
			if(Clock.getRoundNum() == 0) {
				hqRobot.travelJob(DataCache.enemyHQLocation, JobMessage.BroadCastType.GLOBAL);
				
				MapLocation loc = DataCache.ourHQLocation.add(DataCache.enemyHQLocation.directionTo(DataCache.ourHQLocation));
				hqRobot.buildTowerCodeJob(RobotType.PASTR, loc);
				loc = DataCache.ourHQLocation.subtract(DataCache.enemyHQLocation.directionTo(DataCache.ourHQLocation));
				hqRobot.buildTowerCodeJob(RobotType.NOISETOWER, loc);
				
			}
			
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		
		// No two just two pastrs later in the game defend good pastr location first
		
		// If he has more pastrs then us we need to do something about it
	}

}
