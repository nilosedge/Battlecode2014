package milker_07;

import java.util.ArrayList;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class SoldierRobot extends BaseRobot {

	public SoldierState soldierState = SoldierState.NEW;
	public SoldierState nextSoldierState;
	private SoldierState previousSoldierState;

	
	public boolean debugStates = true;
	
	public JobMessage allInJob;
	public JobMessage currentJob;
	private MapLocation rideoutPoint;
	private MapLocation herdingPoint;
	
	//public boolean returnImmediate = false;
	
	public Strategy s;
	
	public SoldierRobot(RobotController rc) throws GameActionException {
		super(rc);
		
		NavSystem.init(this);
		DataCacheSoldier.init();
	}

	public void run() {
		//if(returnImmediate) return;
		try {
			
			Util.startCounter();
			
			
			// BC 348
			DataCacheSoldier.updateRoundVariables();
			
			
			rc.setIndicatorString(0, "Round Var Cost: " + Util.stopCounter() + " Soldier State: " + soldierState);
			
			// BC 19
			JobMessage allInJob = BroadcastSystem.getAllInJob();
			if(allInJob != null) {
				NavSystem.addWayPointAndMoveCloser(allInJob.getWayPointsToJob());
				soldierState = SoldierState.ALL_IN;
				allInCode();
			}
			
			
			
			if(currentJob == null && soldierState != SoldierState.FIGHTING) {
				currentJob = BroadcastSystem.getJobMessageFromHQ();
				if(currentJob != null) {
					NavSystem.addWayPointAndMoveCloser(currentJob.getWayPointsToJob());
					//System.out.println(currentJob);
					soldierState = SoldierState.NEW;
				}
			}
			
			//rc.setIndicatorString(1, "Soldier Cost: " + Util.stopCounter());
		
			
			// BC 338
			if(currentJob != null && soldierState == SoldierState.NEW) {

				// BC 69 or 201 using else if
				switch(currentJob.getJobType()) {
					case DEFENDING:
						soldierState = SoldierState.DEFENDING;
						break;
					case BUILDING:
						soldierState = SoldierState.BUILDING;
						break;
					case HAVOCING:
						soldierState = SoldierState.HAVOCING;
						break;
					case TRAVEL:
						soldierState = SoldierState.TRAVELING;
						break;
					case MAPBUILD:
						Util.startCounter();
						DataCacheSoldier.generateMap();
						Util.debug_println("Generate Map Cost: " + Util.stopCounter());
						BroadcastSystem.sendMap(DataCacheSoldier.map);
						Util.debug_println("Send Map Cost: " + Util.stopCounter());
						BroadcastSystem.sendJobMessageToHQ(JobSystem.createMapCompletedMessage());
						Util.debug_println("Finished with map");
						currentJob = null;
						return;
					case GENGOODSPOTS:
						//DataCache.goodMapSpots = BroadcastSystem.readGoodMapSpots();
						Util.debug_println("Map read started");
						if(DataCacheSoldier.map == null) {
							DataCacheSoldier.map = BroadcastSystem.readMap();
						}
						Util.debug_println("Map read finished");
						DataCacheSoldier.generateGoodTowerLocations();
						Util.debug_println("Gen Tower Locs finished");
						BroadcastSystem.sendGoodMapSpots(DataCacheSoldier.goodMapSpots);
						Util.debug_println("Tower Locs sent");
						BroadcastSystem.sendJobMessageToHQ(JobSystem.createGoodSpotsCompleteMessage());
						Util.debug_println("Tower Gen Job Completed");
						currentJob = null;
						return;
					default:
						break;
				}
			}
			
			

			
			switch(soldierState) {
				case NEW:
					newCode();
					break;
				case HERDING:
					herdingCode();
					break;
				case ALL_IN:
					allInCode();
					break;
				case BUILDING:
					buildCode();
					break;
				case HAVOCING:
					havocingCode();
					break;
				case RIDEOUT:
					rideoutCode();
					break;
				case TRAVELING:
					travelingCode();
					break;
				case DEFENDING:
					defendingCode();
					break;
				case FIGHTING:
					fightingCode();
					break;
				default:
					break;
			}
			
			if(nextSoldierState != null) {
				soldierState = nextSoldierState;
				nextSoldierState = null; // clear the state for the next call of run() to use
			}
			//rc.setIndicatorString(2, "Soldier Cost: " + Util.stopCounter());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void newCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.NEW;
			fightingCode();
		} 
	}

	public void havocingCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.HAVOCING;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				ArrayList<MapLocation> patrolPoints = currentJob.getJobPoints();
				if(patrolPoints != null) {
					NavSystem.addWayPointAndMoveCloser(patrolPoints);
					nextSoldierState = SoldierState.TRAVELING;
					previousSoldierState = SoldierState.HAVOCING;
					travelingCode();
				} else {
					//BroadcastSystem.jobCompleted(currentJob);
					currentJob = null;
				}
			} else {
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.HAVOCING;
				travelingCode();
			}
		}
	}

	public void allInCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.ALL_IN;
			fightingCode();
		} else {
			Util.debug_println("ALLIN: set to travel");
			nextSoldierState = SoldierState.TRAVELING;
			previousSoldierState = SoldierState.ALL_IN;
			travelingCode();
		}
	}

	public void defendingCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.DEFENDING;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				ArrayList<MapLocation> drivePoints = currentJob.getJobPoints();
				if(drivePoints != null) {
					rideoutPoint = drivePoints.get(1);
					herdingPoint = drivePoints.get(0);
					NavSystem.addWayPointAndMoveCloser(rideoutPoint);
					nextSoldierState = SoldierState.RIDEOUT;
					rideoutCode();
				} else {
					//BroadcastSystem.jobCompleted(currentJob);
					currentJob = null;
				}
			} else {
				//System.out.println("DEFEND: set to travel");
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.DEFENDING;
				travelingCode();
			}
		}
	}

	public void rideoutCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.RIDEOUT;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				nextSoldierState = SoldierState.HERDING;
				NavSystem.addWayPointAndMoveCloser(herdingPoint);
				herdingCode();
			} else {
				//System.out.println("RIDEOUT: set to travel");
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.RIDEOUT;
				travelingCode();
			}
		}
	}

	public void herdingCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.HERDING;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				nextSoldierState = SoldierState.RIDEOUT;
				NavSystem.addWayPointAndMoveCloser(rideoutPoint);
				rideoutCode();
			} else {
				//System.out.println("HERD: set to travel");
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.HERDING;
				travelingCode();
			}
		}
	}

	
	public void buildCode() throws GameActionException {
		if(NavSystem.atDestination()) {
			if(currentJob.getBuildType() == JobMessage.BuildType.NOISE) {
				if(rc.isActive()) {
					rc.construct(RobotType.NOISETOWER);
				}
			} else if(currentJob.getBuildType() == JobMessage.BuildType.PASTR) {
				if(rc.isActive()) {
					rc.construct(RobotType.PASTR);
				}
			}
		} else {
			//System.out.println("BUILDING: set to travel");
			nextSoldierState = SoldierState.TRAVELING;
			previousSoldierState = SoldierState.BUILDING;
			travelingCode();
		}
	}
	

	public void travelingCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			//nextSoldierState = SoldierState.FIGHTING;
			//previousSoldierState = SoldierState.TRAVELING;
			//fightingCode();
			microCode();
		} else {
			
			if(NavSystem.atDestination()) {
				if(currentJob.getJobType() == JobMessage.JobType.TRAVEL) {
					currentJob = null;
				}
				nextSoldierState = previousSoldierState;
			} else {
				rc.setIndicatorString(1, "Moving Towards: " + NavSystem.path.get(0));
				if(previousSoldierState == SoldierState.BUILDING) {
					NavSystem.tryMoveCloser(true);
				} else {
					NavSystem.tryMoveCloser(false);
				}
			}
		}
	}

	public void fightingCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots == 0) {
			nextSoldierState = previousSoldierState;
		} else {
			microCode();
		}
	}
	
	public void microCode() throws GameActionException {
		MapLocation myLoc = rc.getLocation();
		//Robot[] enemiesList = rc.senseNearbyGameObjects(Robot.class, 100000, DataCacheSoldier.enemy);
		int enemyDistSquared = DataCacheSoldier.closestEnemyInfo.location.distanceSquaredTo(myLoc);
		boolean weakestAttack = DataCacheSoldier.weakestEnemyInfo.location.distanceSquaredTo(myLoc) <= DataCacheSoldier.myType.attackRadiusMaxSquared;
		boolean attackableEnemy = enemyDistSquared <= DataCacheSoldier.myType.attackRadiusMaxSquared;
		boolean inMajority = DataCacheSoldier.numNearbyFriendlyRobots >= (1.33 * DataCacheSoldier.numNearbyEnemySoldiers);
		boolean sneaking = false;
		boolean bugging = false;
		
		rc.setIndicatorString(1,
			"Clock: " + Clock.getRoundNum() +
			" Close: " + DataCacheSoldier.closestEnemyInfo.location +
			" W: " + DataCacheSoldier.weakestEnemyInfo.location +
			" D: " + enemyDistSquared +
			" AR: " + DataCacheSoldier.myType.attackRadiusMaxSquared +
			" MJ: " + inMajority +
			" FS: " + DataCacheSoldier.numNearbyFriendlySoldiers +
			" FR: " + DataCacheSoldier.numNearbyFriendlyRobots +
			" ES: " + DataCacheSoldier.numNearbyEnemySoldiers +
			" ER: " + DataCacheSoldier.numNearbyEnemyRobots
		); 
		// in attack range and majority
			// attack
			// if weakest attack weakest
			// else attack closest
		// in attack range and not majority
			// if can move out of attack range
				// move out of range
			// else
				// if close enough to self destruct
					// self destruct
				// else
					// move closer

		// attack weakest first then closest
		if(inMajority && weakestAttack) {
			if(rc.isActive()) {
				rc.attackSquare(DataCacheSoldier.weakestEnemyInfo.location);
			}
		} else if(inMajority && attackableEnemy) {
			if(rc.isActive()) {
				rc.attackSquare(DataCacheSoldier.closestEnemyInfo.location);
			}
		} else {
			// in range
			if(attackableEnemy) {
				Direction moveAway = myLoc.directionTo(DataCacheSoldier.closestEnemyInfo.location).opposite();
				// can move out of range
				if((rc.canMove(moveAway) || rc.canMove(moveAway.rotateLeft()) || rc.canMove(moveAway.rotateRight())) && myLoc.add(moveAway).distanceSquaredTo(DataCacheSoldier.closestEnemyInfo.location) > DataCacheSoldier.closestEnemyInfo.type.attackRadiusMaxSquared) {
					NavSystem.goAwayFromLocation(DataCacheSoldier.closestEnemyInfo.location, sneaking);
				} else {
					// if they are to close self destruct
					if(enemyDistSquared <= 2 && rc.getHealth() < 40) {
						rc.selfDestruct();
					} else {
						// get closer so we can self destruct
						NavSystem.goToLocation(myLoc, DataCacheSoldier.closestEnemyInfo.location, sneaking, bugging);
					}
				}
			} else {
				Direction moveToward = myLoc.directionTo(DataCacheSoldier.closestEnemyInfo.location);
				// if moving closer does not put me in the range
				if(myLoc.add(moveToward).distanceSquaredTo(DataCacheSoldier.closestEnemyInfo.location) > DataCacheSoldier.closestEnemyInfo.type.attackRadiusMaxSquared) {
					// move closer staying out of attack range
					NavSystem.goToLocation(myLoc, DataCacheSoldier.closestEnemyInfo.location, sneaking, bugging);
				} else {
					rc.setIndicatorString(2, "No Moving letting the enemy come to us.");
					// dont move let them come to us
				}
			}
		}
	}
}
