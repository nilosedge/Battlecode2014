package milker;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class HQRobot extends BaseRobot {

	public Strategy s;
	
	public HQRobot(RobotController rc) throws GameActionException {
		super(rc);
		
		if(DataCacheHQ.isLargeMap()) {
			s = new StrategyHQLarge(this);
		} else if(DataCacheHQ.isMediumMap()) {
			s = new StrategyHQMedium(this);
		} else {
			s = new StrategyHQSmall(this);
		}

	}

	public void run() {
		try {
			// BC 516
			DataCacheHQ.updateRoundVariables();
			
			// BC 47
			//BroadcastSystem.maintainJobs();
			
			// BC 28
			s.run();

			// BC 162
		
			
			// BC 355
			if(DataCache.closestEnemyInfo != null) {
				int enemyDistSquared = DataCache.closestEnemyInfo.location.distanceSquaredTo(rc.getLocation());
				if(rc.isActive() && enemyDistSquared < rc.getType().attackRadiusMaxSquared) {
				//if(rc.isActive() && rc.canAttackSquare(closestEnemyLocation)) {
					rc.attackSquare(DataCache.closestEnemyInfo.location);
					//spawn();
					
				}
			} else {
				//if(rc.senseRobotCount() < 4) {
					spawn();
				//}

			}

		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}

	public void buildTowerCodeJob(RobotType type, MapLocation loc) throws GameActionException {
		JobMessage jm = JobSystem.createBuildMessage(type, loc);
		BroadcastSystem.sendJobMessageToRobots(jm);
	}
	
	public void generateMapJob() throws GameActionException {
		JobMessage jm = JobSystem.createGenerateMapMessage();
		BroadcastSystem.sendJobMessageToRobots(jm);
	}
	
	public void generateComputeGoodSpotsJob() throws GameActionException {
		JobMessage jm = JobSystem.createComputeGoodSpotsMessage();
		BroadcastSystem.sendJobMessageToRobots(jm);
	}
	
	public void travelJob(MapLocation location, JobMessage.BroadCastType type) throws GameActionException {
		JobMessage jm = JobSystem.createTravelMessage(location, type);
		BroadcastSystem.sendJobMessageToRobots(jm);
	}

	private void spawn() throws GameActionException {
		Direction dir = DataCacheHQ.ourHQLocation.directionTo(DataCacheSoldier.enemyHQLocation);
		int desiredDirOffset = dir.ordinal();
		for (int i = 0; i < DataCacheHQ.directionArray.length; i++) {
			int dirOffset = DataCacheHQ.directionArray[i].ordinal();
			Direction currentDirection = DataCacheSoldier.directionArray[(desiredDirOffset + dirOffset + 8) % 8];
			if (rc.isActive() && rc.canMove(currentDirection) && rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
				rc.spawn(currentDirection);
				return;
			}
		}
	}

}