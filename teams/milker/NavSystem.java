package milker;

import java.util.ArrayList;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class NavSystem {
	
	public static SoldierRobot robot;
	public static RobotController rc;
	
	public static int[] directionOffsets;
	public static boolean bugging = false;
	
	public static Direction leftWall;
	
	//public static MapLocation currentWaypoint;
	//public static MapLocation lastLocation;
	//public static Direction lastDir;
	//public static Direction wantDir;
	//private static MapLocation destination;
	public static ArrayList<MapLocation> path = new ArrayList<MapLocation>();
	
	private static int	initialDist;
	private static int  currentDist;
	private static boolean buggingLeft;
	private static boolean	turnedLeft;
	
	public static void init(SoldierRobot myRobot) throws GameActionException {
		robot = myRobot;
		rc = robot.rc;
		//lastLocation = rc.getLocation();
		// Randomly assign soldiers priorities for trying to move left or right
		if (rc.getRobot().getID() % 4 <= 1) {
			directionOffsets = new int[]{0,1,-1,2,-2,3,-3};
		} else {
			directionOffsets = new int[]{0,-1,1,-2,2,-3,3};
		}
	}

	public static boolean goToLocation(MapLocation myLoc, MapLocation location, boolean sneak, boolean attemptBugging) throws GameActionException {
		Direction dir = myLoc.directionTo(location);
//		System.out.println("Current Want Direction: " + dir);
//		System.out.println("Last Want Direction: " + wantDir);
//		System.out.println("Last Direction actually Traveled: " + lastDir);
//		System.out.println("Direction oppisite of last tile: " + myLoc.directionTo(lastLocation));
//		System.out.println("Current Location: " + myLoc);
//		System.out.println("Last Location: " + lastLocation);
		
		if(attemptBugging) {
		
			currentDist = myLoc.distanceSquaredTo(location);
			
			if(!rc.canMove(dir) && !bugging) {
				bugging = true;
				initialDist = myLoc.distanceSquaredTo(location);
				boolean moved = goDirection(myLoc, dir, sneak);
				if(moved) {
					buggingLeft = !turnedLeft;
				}
				rc.setIndicatorString(1, "Can't move dir: " + dir + " BL: " + buggingLeft);
				return moved;
			} else {
				if(bugging) {
					if(currentDist < initialDist && rc.canMove(dir)) {
						bugging = false;
						rc.setIndicatorString(1, "Bugging Finish Leaving Wall: " + dir);
						return goDirection(myLoc, dir, sneak);
					} else {
						// find wall direction
						Direction bugDir;
						if(buggingLeft) {
							bugDir = leftWall;
							while(!rc.canMove(bugDir)) {
								bugDir = bugDir.rotateRight();
							}
							rc.setIndicatorString(1, "Bugging Left: " + dir + " BugDir: " + bugDir + " LW: " + leftWall);
						} else {
							bugDir = leftWall.opposite();
							while(!rc.canMove(bugDir)) {
								bugDir = bugDir.rotateLeft();
							}
							rc.setIndicatorString(1, "Bugging Right: " + dir + " BugDir: " + bugDir + " RW: " + leftWall.opposite());
						}
						return goDirection(myLoc, bugDir, sneak);
					}
				} else {
					rc.setIndicatorString(1, "Not bugging: " + dir);
					return goDirection(myLoc, dir, sneak);
				}
			}
		} else {
			bugging = false;
			return goDirection(myLoc, dir, sneak);
		}
	}
	
	public static boolean goAwayFromLocation(MapLocation location, boolean sneak) throws GameActionException {
		MapLocation myLoc = rc.getLocation();
		Direction dir = myLoc.directionTo(location).opposite();
		if (dir != Direction.OMNI) {
			return goDirection(myLoc, dir, sneak);
		}
		return false;
	}
	
	private static boolean goDirection(MapLocation myLoc, Direction dir, boolean sneak) throws GameActionException {
		Direction lookingAtCurrently = dir;
		boolean moved = false;
		//System.out.println("Attempting to move: " + dir);
		for (int d : directionOffsets) {
			lookingAtCurrently = DataCacheSoldier.directionArray[(dir.ordinal() + d + 8) % 8];
			if(sneak) {
				moved = sneak(myLoc, lookingAtCurrently);
			} else {
				moved = move(myLoc, lookingAtCurrently);
			}
			if(moved) {
				if(d <= 0) { turnedLeft = true; }
				if(d > 0) { turnedLeft = false; }
				break;
			}
		}
		rc.setIndicatorString(2, "B: " + bugging + " TL: " + turnedLeft + " Dir: " + dir);
		return moved;
	}
	
	private static boolean move(MapLocation myLoc, Direction dir) throws GameActionException {
		if (rc.isActive()) {
			if (rc.canMove(dir)) {
				rc.move(dir);
				leftWall = dir.rotateLeft().rotateLeft();
				return true;
			}
			return false;
		}
		return false;
	}
	
	private static boolean sneak(MapLocation myLoc, Direction dir) throws GameActionException {
		if (rc.isActive()) {
			if (rc.canMove(dir)) {
				rc.sneak(dir);
				leftWall = dir.rotateLeft().rotateLeft();
				return true;
			} 
			return false;
		}
		return false;
	}
	
	public static void addWayPointAndMoveCloser(ArrayList<MapLocation> locs) {
		for(int i = 0; i < locs.size(); i++) {
			path.add(locs.get(i));
		}
	}
	
	public static void addWayPointAndMoveCloser(MapLocation point) {
		path.add(point);
	}
	
	public static boolean tryMoveCloser(boolean asCloseAsPossiable) throws GameActionException {
		boolean moved = false;
		if(path.size() > 0) {
			MapLocation myLoc = rc.getLocation();
			MapLocation goLoc = path.get(0);
			moved = goToLocation(myLoc, goLoc, false, true);
			if(asCloseAsPossiable) {
				
				if(myLoc.x == goLoc.x && myLoc.y == goLoc.y) {
				//if(!rc.canMove(myLoc.directionTo(goLoc)) && myLoc.distanceSquaredTo(goLoc) <= 1) {
					path.remove(0);
				} else {
					if(!rc.canMove(myLoc.directionTo(goLoc)) && myLoc.distanceSquaredTo(goLoc) < 2) {
						bugging = false;
						path.remove(0);
					}
				}
			} else {
				if(myLoc.distanceSquaredTo(goLoc) <= 7) {
					bugging = false;
					path.remove(0);
				}
			}
		}
		return moved;
	}

	public static boolean atDestination() {
		if(path.size() == 0) {
			return true;
		}
		return false;
	}

	public static ArrayList<MapLocation> getWayPoints(MapLocation source, MapLocation dest, int amount) {
		return new ArrayList<MapLocation>();
	}

}