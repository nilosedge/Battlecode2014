package milker;


import java.util.ArrayList;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class JobSystem {

	public static BaseRobot robot;
	public static RobotController rc;
	
	public static double supGenRatio;

	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
	}

	public static JobMessage createBuildMessage(RobotType type, MapLocation mapLocation) {
		JobMessage jm = new JobMessage();
		ArrayList<MapLocation> points = new ArrayList<MapLocation>();
		points.add(mapLocation);
		
		jm.setJobType(JobMessage.JobType.BUILDING);
		jm.setWayPointsToJob(points);
		if(RobotType.PASTR == type) {
			jm.setBuildType(JobMessage.BuildType.PASTR);
		}
		if(RobotType.NOISETOWER == type) {
			jm.setBuildType(JobMessage.BuildType.NOISE);
		}
		return jm;
	}
	
	public static JobMessage createTravelMessage(MapLocation travelToLocation, JobMessage.BroadCastType broadcastType) {
		JobMessage jm = new JobMessage();
		jm.setJobType(JobMessage.JobType.TRAVEL);
		jm.setBroadcastType(broadcastType);
		ArrayList<MapLocation> points = new ArrayList<MapLocation>();
		points.add(travelToLocation);
		jm.setWayPointsToJob(points);
		return jm;
	}
	
	public static JobMessage createDefendMessage(ArrayList<MapLocation> pointsToLocation, ArrayList<MapLocation> defendPoints, JobMessage.BroadCastType broadcastType) {
		JobMessage jm = new JobMessage();
		jm.setJobType(JobMessage.JobType.DEFENDING);
		jm.setBroadcastType(broadcastType);
		jm.setWayPointsToJob(pointsToLocation);
		jm.setJobPoints(defendPoints);
		return jm;
	}

	public static JobMessage createHavocMessage(ArrayList<MapLocation> pointsToHavoc, ArrayList<MapLocation> partrolPoints, boolean broadcast) {
		JobMessage jm = new JobMessage();
		jm.setJobType(JobMessage.JobType.HAVOCING);
		jm.setWayPointsToJob(pointsToHavoc);
		jm.setJobPoints(partrolPoints);
		return jm;
	}

	public static JobMessage createGenerateMapMessage() {
		JobMessage jm = new JobMessage();
		jm.setJobType(JobMessage.JobType.MAPBUILD);
		return jm;
	}
	
	public static JobMessage createComputeGoodSpotsMessage() {
		JobMessage jm = new JobMessage();
		jm.setJobType(JobMessage.JobType.GENGOODSPOTS);
		return jm;
	}
	
	public static JobMessage createMapCompletedMessage() {
		JobMessage jm = new JobMessage();
		jm.setJobType(JobMessage.JobType.MESSAGE);
		jm.setMessageType(JobMessage.MessageType.MAPCOMPLETE);
		return jm;
	}
	
	public static JobMessage createPastrCompletedMessage() {
		JobMessage jm = new JobMessage();
		jm.setJobType(JobMessage.JobType.MESSAGE);
		jm.setMessageType(JobMessage.MessageType.PASTRCOMPLETE);
		return jm;
	}
	
	public static JobMessage createNoiseCompletedMessage() {
		JobMessage jm = new JobMessage();
		jm.setJobType(JobMessage.JobType.MESSAGE);
		jm.setMessageType(JobMessage.MessageType.NOISECOMPLETE);
		return jm;
	}
	
	public static JobMessage createGoodSpotsCompleteMessage() {
		JobMessage jm = new JobMessage();
		jm.setJobType(JobMessage.JobType.MESSAGE);
		jm.setMessageType(JobMessage.MessageType.GOODSPOTSCOMPLETE);
		return jm;
	}

}
