package milker;

public interface Strategy {

	public void run();

}
