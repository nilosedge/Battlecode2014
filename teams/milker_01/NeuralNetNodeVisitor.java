package milker_01;

public interface NeuralNetNodeVisitor {

	void Visit(NeuralNetNeuron sNeuron);
	void Visit(NeuralNetNeuronLayer sNeuronLayer);
	void Visit(NeuralNet neuralNet);
	void Visit(NeuralNetNeuronConnection neuronConnection);

}
