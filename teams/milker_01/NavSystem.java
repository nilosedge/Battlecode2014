package milker_01;

import java.util.ArrayList;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import battlecode.common.Upgrade;

public class NavSystem {
	
	public static SoldierRobot robot;
	public static RobotController rc;
	
	public static int[] directionOffsets;
	
	// Used by both smart and backdoor nav
	public static NavMode navMode = NavMode.REGULAR;
	public static MapLocation currentWaypoint;
	private static MapLocation destination;
	
	// Used to store the waypoints of a backdoor nav computation
	public static MapLocation[] backdoorWaypoints; // Should always have four MapLocations
	public static int backdoorWaypointsIndex;

	// Map size
	public static int mapHeight;
	public static int mapWidth;
	public static MapLocation mapCenter;
	
	// For BFS
	public static int BFSRound = 0;
	public static int[] BFSTurns;
	public static int BFSIdle = 0;
	
	// For swarm coefficients
	public static double swarmC;
	public static double swarmD = 0;
	
	public static ArrayList<MapLocation> lastLocations = new ArrayList<MapLocation>();
	
	/**
	 * MUST CALL THIS METHOD BEFORE USING NavSystem
	 * @param myRC
	 */
	public static void init(SoldierRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
		
		// Randomly assign soldiers priorities for trying to move left or right
		if (rc.getRobot().getID() % 4 <= 1) {
			directionOffsets = new int[]{0,1,-1,2,-2};
		} else {
			directionOffsets = new int[]{0,-1,1,-2,2};
		}

		// Get map dimensions
		mapHeight = rc.getMapHeight();
		mapWidth = rc.getMapWidth();
		// Calculate the center of the map
		mapCenter = new MapLocation(mapWidth / 2, mapHeight / 2);
		
//		swarmC = 0;
	}
	
	public static void goToLocation(MapLocation location) throws GameActionException {
		Direction dir = rc.getLocation().directionTo(location);
		if (dir != Direction.OMNI) {
			goDirection(dir);
		}
		//rc.setIndicatorString(0, "Going to: X: " + location.x + " Y: "  + location.y);
	}
	
	public static void goAwayFromLocation(MapLocation location) throws GameActionException {
		Direction dir = rc.getLocation().directionTo(location).opposite();
		if (dir != Direction.OMNI) {
			goDirection(dir);
		}
	}
	
	public static void goDirection(Direction dir) throws GameActionException {
		if (rc.isActive()) {
			Direction lookingAtCurrently = dir;
			lookAround: for (int d : directionOffsets) {
				lookingAtCurrently = Direction.values()[(dir.ordinal() + d + 8) % 8];
				if (rc.isActive() && rc.canMove(lookingAtCurrently)) {
					lastLocations.add(rc.getLocation());
					rc.move(lookingAtCurrently);
					break lookAround;
				}
			}
		}
	}
	
	public static boolean move(Direction dir) throws GameActionException {
		if (rc.isActive()) {
			if (rc.canMove(dir)) {
				lastLocations.add(rc.getLocation());
				rc.move(dir);
				return true;
			} 
			return false;
		}
		return false;
	}

	public static void followWaypoints(boolean swarm) throws GameActionException {
		// If we're close to currentWaypoint, find the next one
		if (rc.getLocation().distanceSquaredTo(destination) <= Constants.PATH_GO_ALL_IN_SQ_RADIUS) {

			navMode = NavMode.REGULAR;
			// We're done following waypoints
			goToLocation(destination);

		} else if (rc.getLocation().distanceSquaredTo(currentWaypoint) <= Constants.WAYPOINT_SQUARED_DISTANCE_CHECK){
			// We're close to currentWaypoint, so find the next one
			switch (navMode) {
			case SMART:
				getSmartWaypoint();
				break;
			case BACKDOOR:
				getBackdoorWaypoint();
				break;
			default:
				break;
			}

			if (swarm) {
				goToLocationSwarm(currentWaypoint);
			} else {
				goToLocation(currentWaypoint);
			}

		} else {

			if (swarm) {
				goToLocationSwarm(currentWaypoint);
			} else {
				goToLocation(currentWaypoint);
			}

		}
	}
	
	private static void goToLocationSwarm(MapLocation location) throws GameActionException {
		// Swarming
		double dxToLocation = location.x - rc.getLocation().x;
		double dyToLocation = location.y - rc.getLocation().y;
		double distanceToLocation = Math.sqrt(rc.getLocation().distanceSquaredTo(location));
		
		Robot[] nearbyAlliedRobots = rc.senseNearbyGameObjects(Robot.class, 14, rc.getTeam());
		int totalDx = 0;
		int totalDy = 0;
		int totalNearbyerDx = 0;
		int totalNearbyerDy = 0;
		for (int i = nearbyAlliedRobots.length; --i >= 0; ) {
			RobotInfo robotInfo = rc.senseRobotInfo(nearbyAlliedRobots[i]);
			if (robotInfo.type == RobotType.SOLDIER) {
				MapLocation iterLocation = robotInfo.location;
				totalDx += (iterLocation.x - rc.getLocation().x);
				totalDy += (iterLocation.y - rc.getLocation().y);
				if (rc.getLocation().distanceSquaredTo(iterLocation) <= 5) {
					// for repelling
					totalNearbyerDx += (iterLocation.x - rc.getLocation().x);
					totalNearbyerDy += (iterLocation.y - rc.getLocation().y);
				}
			}
		}
		
		double denom = Math.sqrt(totalDx*totalDx + totalDy*totalDy);
		double nearbyerDenom = Math.sqrt(totalNearbyerDx*totalNearbyerDx + totalNearbyerDy*totalNearbyerDy);
		double addX, addY;
		double addXNearbyer, addYNearbyer;
		
		if (Math.abs(totalDx) < 0.01) {
			addX = 0;
		} else {
			addX = swarmC * totalDx / denom;
		}
		if (Math.abs(totalDy) < 0.01) {
			addY = 0;
		} else {
			addY = swarmC * totalDy / denom;
		}
		
		if (Math.abs(totalNearbyerDx) < 0.01) {
			addXNearbyer = 0;
		} else {
			addXNearbyer = swarmD * totalNearbyerDx / nearbyerDenom;
		}
		if (Math.abs(totalNearbyerDy) < 0.01) {
			addYNearbyer = 0;
		} else {
			addYNearbyer = swarmD * totalNearbyerDy / nearbyerDenom;
		}
		double finalDx = dxToLocation / distanceToLocation + addX - addXNearbyer;
		double finalDy = dyToLocation / distanceToLocation + addY - addYNearbyer;
		
//		rc.setIndicatorString(0, "totalDx: " + Integer.toString(totalDx) + ", finalDx: " + Double.toString(finalDx));
//		rc.setIndicatorString(1, "totalDy: " + Integer.toString(totalDy) + ", finalDy: " + Double.toString(finalDy));
		
		int dirOffset;
		double ratioCutoff = 2.5;
		
		double ratio = Math.abs(finalDx / finalDy);
		
		if (ratio > ratioCutoff) {
			// go along x-axis
			if (finalDx > 0) {
				dirOffset = 2;
			} else {
				dirOffset = 6;
			}
		} else if (ratio < 1 / ratioCutoff) {
			// go along y-axis
			if (finalDy > 0) {
				dirOffset = 4;
			} else {
				dirOffset = 0;
			}
		} else {
			if (finalDx > 0) {
				if (finalDy >= 0) {
					dirOffset = 3;
				} else {
					dirOffset = 1;
				}
			} else {
				if (finalDy > 0) {
					dirOffset = 5;
				} else {
					dirOffset = 7;
				}
			}
		}
		
		Direction dirToMoveIn = Direction.values()[dirOffset];
		NavSystem.goDirection(dirToMoveIn);

	}

//	/**
//	 * Sets up the backdoor navigation system for a given endLocation.
//	 * @param endLocation
//	 * @throws GameActionException
//	 */
//	public static void setupBackdoorNav(MapLocation endLocation) throws GameActionException {
//		navMode = NavMode.BACKDOOR;
//		destination = endLocation;
//		// Calculate all the waypoints first and save them (this is different from smart nav)
//		MapLocation currentLocation = rc.getLocation();
//		Direction dirToEndLocation = currentLocation.directionTo(endLocation);
//		// How close are we to the wall?
//		int horzDistanceToWall = mapWidth / 2 - Math.abs(endLocation.x - mapCenter.x);
//		int vertDistanceToWall = mapHeight / 2 - Math.abs(endLocation.y - mapCenter.y);
//		int distanceToWall = (int)(0.8 * Math.min(horzDistanceToWall, vertDistanceToWall));
//		MapLocation firstWaypoint = currentLocation.add(dirToEndLocation, -distanceToWall);
//		MapLocation lastWaypoint = endLocation.add(dirToEndLocation, distanceToWall);
//		backdoorWaypoints = new MapLocation[]{firstWaypoint, null, null, lastWaypoint};
//		// Now let's try to find some intermediate waypoints to follow
//		// Let's see if we should move horizontally first or vertically first
//		int dx = endLocation.x - currentLocation.x;
//		int dy = endLocation.y - currentLocation.y;
//		if (Math.abs(dx) > Math.abs(dy)) {
//			// We're vertically really close, but not horizontally, so move vertically first
//			if (Util.Random() < 0.5) {
//				// Try moving up, then horizontally, then down to endLocation
//				backdoorWaypoints[1] = new MapLocation(currentLocation.x, Constants.BACKDOOR_WALL_BUFFER);
//				// We need to know if endLocation is closer to the left wall or the right wall
//				if (endLocation.x < mapWidth - endLocation.x) { // left wall
//					backdoorWaypoints[2] = new MapLocation(Constants.BACKDOOR_WALL_BUFFER, Constants.BACKDOOR_WALL_BUFFER);
//				} else { // right wall
//					backdoorWaypoints[2] = new MapLocation(mapWidth - 1 - Constants.BACKDOOR_WALL_BUFFER, Constants.BACKDOOR_WALL_BUFFER);
//				}
//			} else {
//				// Try moving down, then horizontally, then up to endLocation
//				backdoorWaypoints[1] = new MapLocation(currentLocation.x, mapHeight - Constants.BACKDOOR_WALL_BUFFER);
//				// We need to know if endLocation is closer to the left wall or the right wall
//				if (endLocation.x < mapWidth - endLocation.x) { // left wall
//					backdoorWaypoints[2] = new MapLocation(Constants.BACKDOOR_WALL_BUFFER, mapHeight - 1 - Constants.BACKDOOR_WALL_BUFFER);
//				} else { // right wall
//					backdoorWaypoints[2] = new MapLocation(mapWidth - 1 - Constants.BACKDOOR_WALL_BUFFER, mapHeight - 1 - Constants.BACKDOOR_WALL_BUFFER);
//				}
//			}
//		} else {
//			// We're horizontally really close, but not vertically, so move horizontally first
//			if (Util.Random() < 0.5) {
//				// Try moving left, then vertically, then right to endLocation
//				backdoorWaypoints[1] = new MapLocation(Constants.BACKDOOR_WALL_BUFFER, currentLocation.y);
//				// We need to know if endLocation is closer to the top wall or the bottom wall
//				if (endLocation.y < mapHeight - endLocation.y) { // top wall
//					backdoorWaypoints[2] = new MapLocation(Constants.BACKDOOR_WALL_BUFFER, Constants.BACKDOOR_WALL_BUFFER);
//				} else { // bottom wall
//					backdoorWaypoints[2] = new MapLocation(Constants.BACKDOOR_WALL_BUFFER, mapHeight - 1 - Constants.BACKDOOR_WALL_BUFFER);
//				}
//			} else {
//				// Try moving right, then vertically, then left to endLocation
//				backdoorWaypoints[1] = new MapLocation(mapWidth - Constants.BACKDOOR_WALL_BUFFER, currentLocation.y);
//				// We need to know if endLocation is closer to the top wall or the bottom wall
//				if (endLocation.y < mapHeight - endLocation.y) { // top wall
//					backdoorWaypoints[2] = new MapLocation(mapWidth - 1 - Constants.BACKDOOR_WALL_BUFFER, Constants.BACKDOOR_WALL_BUFFER);
//				} else { // bottom wall
//					backdoorWaypoints[2] = new MapLocation(mapWidth - 1 - Constants.BACKDOOR_WALL_BUFFER, mapHeight - 1 - Constants.BACKDOOR_WALL_BUFFER);
//				}
//			}
//		}
//		// Upon calling getBackdoorWaypoint(), backdoorWaypointsIndex will be incremented by 1
//		backdoorWaypointsIndex = -1;
//		getBackdoorWaypoint();
//	}
	
	/**
	 * Sets up the smart navigation system for a given endLocation.
	 * This means that we will set navMode = navMode.SMART
	 * @param endLocation
	 * @throws GameActionException
	 */
	public static void setupSmartNav(MapLocation endLocation) throws GameActionException {
		navMode = NavMode.SMART;
		destination = endLocation;
		getSmartWaypoint();
	}
	
	/**
	 * Gets the next backdoor waypoint that was calculated when setupBackdoorNav() was called.
	 * @throws GameActionException
	 */
	public static void getBackdoorWaypoint() throws GameActionException {
		if (backdoorWaypointsIndex == backdoorWaypoints.length - 1){
			// We're at the last waypoint, so just go straight to the destination
			currentWaypoint = destination;
		} else {
			// Get the next waypoint
			currentWaypoint = backdoorWaypoints[++backdoorWaypointsIndex];
		}
	}
	
	/**
	 * Calculates the next smart waypoint to take and writes it to currentWaypoint.
	 * @throws GameActionException
	 */
	public static void getSmartWaypoint() throws GameActionException {
		MapLocation currentLocation = rc.getLocation();
		if (currentLocation.distanceSquaredTo(destination) <= Constants.PATH_GO_ALL_IN_SQ_RADIUS) {
			// If we're already really close to the destination, just go straight in
			currentWaypoint = destination;
			return;
		}
		
		int bestScore = Integer.MAX_VALUE;
		MapLocation bestLocation = null;
		Direction dirLookingAt = currentLocation.directionTo(destination);
		for (int i = -3; i <= 3; i++) {
			Direction dir = Direction.values()[(dirLookingAt.ordinal() + i + 8) % 8];
			MapLocation iterLocation = currentLocation.add(dir, Constants.PATH_OFFSET_RADIUS);
			int currentScore = smartScore(iterLocation, Constants.PATH_CHECK_RADIUS, destination);
			if (currentScore < bestScore) {
				bestScore = currentScore;
				bestLocation = iterLocation;
			}
		}
		currentWaypoint = bestLocation;
	}

	/**
	 * Smart scoring function for calculating how favorable it is to move in a certain direction.
	 * @param location
	 * @param radius
	 * @throws GameActionException 
	 */
	public static int smartScore(MapLocation location, int radius, MapLocation endLocation) throws GameActionException {
		//int numPastrs = rc.sensePastrLocations(rc.getTeam()).length;
		int distanceSquared = location.distanceSquaredTo(endLocation);
		for(int i = 0; i < lastLocations.size(); i++) {
			if(location.x == lastLocations.get(i).x && location.y == lastLocations.get(i).y) {
				distanceSquared += 2000;
			}
		}
		//System.out.println("Score: " + distanceSquared + " X: " + location.x + " Y: " + location.y);
		//rc.setIndicatorString(2, "Score: " + distanceSquared);
		return distanceSquared;
	}
	
	
	public static void setupGetCloser(MapLocation endLocation) throws GameActionException {
		navMode = NavMode.GETCLOSER;
		destination = endLocation;
	}
	/**
	 * Moves to a square that is closer to the goal
	 * returns true if was able to move, false otherwise
	 * @param goalLoc
	 * @return
	 * @throws GameActionException
	 */
	public static boolean moveCloser() throws GameActionException {
		// first try to get closer
		double distance = rc.getLocation().distanceSquaredTo(destination);
		for (int i = 8; --i >= 0; ) {
			if (rc.canMove(DataCache.directionArray[i])) {
				MapLocation nextLoc = rc.getLocation().add(DataCache.directionArray[i]);
				if (nextLoc.distanceSquaredTo(destination) < distance) {
					NavSystem.move(DataCache.directionArray[i]);
					return true;
				}
			}
		}
		return false;
	}
	
	public static void tryMoveCloser() throws GameActionException {
		boolean moved = NavSystem.moveCloser();
		System.out.println("moved: " + moved);
		if (moved == false) {	
			NavSystem.setupBFSMode(destination);
		}
	}
	public static void setupBFSMode(MapLocation endLocation) throws GameActionException {
		navMode = NavMode.BFSMODE;
		destination = endLocation;
		int[][] encArray = NavSystem.populate5by5board();
		int[] goalCoord = NavSystem.locToIndex(rc.getLocation(), destination, 2);
		BFSRound = 0;
		BFSIdle = 0;
		BFSTurns = NavSystem.runBFS(encArray, goalCoord[1], goalCoord[0]);
//		System.out.println("BFSTurns :" + BFSTurns.length);
		if (BFSTurns.length == 0) { // if unreachable, tell to HQ and unassign himself
//			System.out.println("unreachable, unassigned: " + robot.unassigned + " soldierstate: " + ((SoldierRobot) robot).soldierState);
			//EncampmentJobSystem.postUnreachableMessage(destination);
			navMode = NavMode.REGULAR;
			//robot.unassigned = true;
		}
	}
	
	public static void tryBFSNextTurn() throws GameActionException{
		if (BFSIdle >= 50) {
			setupGetCloser(destination);
		} else {
			Direction dir = Direction.values()[BFSTurns[BFSRound]];
			boolean hasMoved = NavSystem.move(dir);
			if (hasMoved) {
				BFSRound++;
			} else {
				BFSIdle++;
			}
		}
	}

	public static int[] runBFS(int[][] encArray, int goalx, int goaly) {
		int[][] distanceArray = new int[encArray.length][encArray[0].length];
		distanceArray[2][2] = 1;
		for (int y = 5; --y >= 0; ) {
			for (int x = 5; --x >= 0; ) {
				if (encArray[y][x] > 0) {
					distanceArray[y][x] = -1;
				}
			}
		}
		
		int currValue = 1;
		boolean reached = false;
		whileLoop: while(currValue < 20) {
			for (int y = 5; --y >= 0; ) {
				for (int x = 5; --x >= 0; ) {
					if (distanceArray[y][x] == currValue) {
						if (y == goaly && x == goalx) {
							reached = true;
							break whileLoop;
						} else {
							propagate(distanceArray, x, y, currValue + 1);
						}
					}
				}
			}
			currValue++;
		}
		
		if (!reached || currValue == 1) { // if unreachable
			return new int[0]; // return empty list
		}
		
		int shortestDist = distanceArray[goaly][goalx] - 1;
		int[] output = new int[shortestDist];
		currValue = shortestDist;
		int currx = goalx;
		int curry = goaly;
		int turn = 0;
		
		while(currValue > 1) {
			int[][] neighbors = getNeighbors(currx, curry);
			forloop: for (int i = neighbors.length; --i >= 0; ) {
				int[] neighbor = neighbors[i];
				int nx = neighbor[0];
				int ny = neighbor[1];
				if (ny < 5 && ny >= 0 && nx < 5 && nx >= 0) {
					if (distanceArray[ny][nx] == currValue) {
						turn = computeTurn(currx, curry, nx, ny);						
						output[currValue-1] = turn;
						currx = nx;
						curry = ny;
						currValue--;
						break forloop;
					}
				}
			}
		}
		output[0] = computeTurn(currx, curry, 2, 2);

		return output;
		
	}	

	public static void propagate(int[][] distanceArray, int x, int y, int value) {
		int[][] neighbors = getNeighbors(x,y);
		for (int i = neighbors.length; --i >= 0; ) {
			int[] neighbor = neighbors[i];
			int ny = neighbor[1];
			int nx = neighbor[0];
			if (ny < 5 && ny >= 0 && nx < 5 && nx >= 0) {
				if (distanceArray[ny][nx] > value || distanceArray[ny][nx] == 0) {
					distanceArray[ny][nx] = value;					
				}
			}
		}
	}
	
	public static int[][] getNeighbors(int x, int y) {
		int[][] output = {{x-1, y}, {x-1, y+1}, {x, y+1}, {x+1, y+1}, {x+1, y}, {x+1, y-1}, {x, y-1}, {x-1, y-1}};
		return output;
	}
	
	public static int computeTurn(int x, int y, int nx, int ny) {
		if (ny == y+1 && nx == x) {
			return 0;
		} else if (ny == y+1 && nx == x-1) {
			return 1;
		} else if (ny == y && nx == x-1) {
			return 2;
		} else if (ny == y-1 && nx == x-1) {
			return 3;
		} else if (ny == y-1 && nx == x) {
			return 4;
		} else if (ny == y-1 && nx == x+1) {
			return 5;
		} else if (ny == y && nx == x+1) {
			return 6;
		} else if (ny == y+1 && nx == x+1) {
			return 7;
		} else {
			return 8;
		}
	}
	
	public static int[] locToIndex(MapLocation ref, MapLocation test,int offset){
		int[] index = new int[2];
		index[0] = test.y-ref.y+offset;
		index[1] = test.x-ref.x+offset;
		return index;
	}
	
	public static int[][] populate5by5board() throws GameActionException{		
		MapLocation myLoc = rc.getLocation();
		int[][] array = new int[5][5];

		Robot[] nearbyRobots = rc.senseNearbyGameObjects(Robot.class, 8);
//		rc.setIndicatorString(2, "number of bots: "+nearbyRobots.length);
		for (int i = nearbyRobots.length; --i >= 0; ) {
			Robot aRobot = nearbyRobots[i];
			RobotInfo info = rc.senseRobotInfo(aRobot);
			int[] index = locToIndex(myLoc,info.location, 2);
			if(index[0] >= 0 && index[0] <= 4 && index[1] >= 0 && index[1] <= 4){
				if (info.type != RobotType.SOLDIER) {
					array[index[0]][index[1]]=100;
				}
			}
		}
		
		for (int i = 5; --i >=0; ) {
			for (int j = 5; --j >= 0; ) {
				int yLoc = myLoc.y + 2-i;
				int xLoc = myLoc.x + 2-j;
				if (xLoc >= rc.getMapWidth() || xLoc < 0 || yLoc >= rc.getMapHeight() || yLoc < 0) {
					array[i][j] = 100;
				}
			}
		}
		return array;
	}
	
	public static MapLocation getDestination() {
		return destination;
	}
}