package milker_01;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class DataCache {
	
	public static BaseRobot robot;
	public static RobotController rc;
	
	public static MapLocation ourHQLocation;
	public static MapLocation enemyHQLocation;
	public static int rushDistSquared;
	public static int rushDist;
	
	public static Direction[] directionArray = Direction.values();
	
	public static boolean onCycle;
	
	// Map width
	public static int mapWidth;
	public static int mapHeight;
	
	// Round variables - army sizes
	// Allied robots
	public static int numAlliedRobots;
	public static int numAlliedSoldiers;
	public static int numAlliedPastrs;
	
	public static int numNearbyAlliedRobots;	
	public static int numNearbyAlliedSoldiers;
	public static int numNearbyAlliedPastrs;

	// Enemy robots
	public static int numEnemyRobots;
	
	public static Robot[] nearbyEnemyRobots;
	
	public static int numNearbyEnemyRobots;
	public static int numNearbyEnemySoldiers;

	
	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
		
		ourHQLocation = rc.senseHQLocation();
		enemyHQLocation = rc.senseEnemyHQLocation();
		rushDistSquared = ourHQLocation.distanceSquaredTo(enemyHQLocation);
		rushDist = (int) Math.sqrt(rushDistSquared);
		
		mapWidth = rc.getMapWidth();
		mapHeight = rc.getMapHeight();
	}
	
	/**
	 * A function that updates round variables
	 */
	public static void updateRoundVariables() throws GameActionException {
		onCycle = Clock.getRoundNum() % Constants.CHANNEL_CYCLE == 0 && Clock.getRoundNum() > 0;
		
		numAlliedRobots = rc.senseNearbyGameObjects(Robot.class, 10000, rc.getTeam()).length;
		numAlliedPastrs = rc.sensePastrLocations(rc.getTeam()).length;
		
		//rc.sensePastrLocations(arg0)
		
		numAlliedSoldiers = numAlliedRobots - numAlliedPastrs - 1 - JobSystem.numPastrsNeeded;
		
		numNearbyAlliedRobots = rc.senseNearbyGameObjects(Robot.class, 14, rc.getTeam()).length;
		numNearbyAlliedPastrs = rc.sensePastrLocations(rc.getTeam()).length;
		numNearbyAlliedSoldiers = numNearbyAlliedRobots - numNearbyAlliedPastrs;
		
		nearbyEnemyRobots = rc.senseNearbyGameObjects(Robot.class, 25, rc.getTeam().opponent());
		
		numNearbyEnemySoldiers = 0;
		for (int i = nearbyEnemyRobots.length; --i >= 0; ) {
			RobotInfo robotInfo = rc.senseRobotInfo(nearbyEnemyRobots[i]);
			if (robotInfo.type == RobotType.SOLDIER) {
				numNearbyEnemySoldiers++;
			}
		}
		
		numNearbyEnemyRobots = nearbyEnemyRobots.length;
		numEnemyRobots = rc.senseNearbyGameObjects(Robot.class, 10000, rc.getTeam().opponent()).length;

	}
}
