package milker_01;

import battlecode.common.GameConstants;

public enum ChannelType {

	MOVE_OUT,
	GEN_COUNT,
	
	NOISETOWER_SEEN,
	PASTRTOWER_SEEN,
	
	PST1,
	PST2,
	PST3,
	PST4,
	PST5,
	
	COMP1,
	COMP2,
	COMP3,
	COMP4,
	COMP5,

	RETREAT_CHANNEL;
	
	public static final int size = ChannelType.values().length;
	public static final int range = GameConstants.BROADCAST_MAX_CHANNELS / size;
}