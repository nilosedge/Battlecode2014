package milker_01;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;

public class HQRobot extends BaseRobot {

	public ChannelType genCountChannel = ChannelType.GEN_COUNT;
	
	public HQRobot(RobotController rc) {
		super(rc);
	}

	public void run() {
		try {
			DataCache.updateRoundVariables();
			// Check for enemys
			// Attack them
			// Spawn more soldiers
			
			
			spawn();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void spawn() throws GameActionException {
		Direction desiredDir = rc.getLocation().directionTo(DataCache.enemyHQLocation);
		Direction dir = getSpawnDirection(desiredDir);
		if (dir != null) {
			rc.spawn(dir);
		}
	}
	
	private Direction getSpawnDirection(Direction dir) {
		Direction canMoveDirection = null;
		int desiredDirOffset = dir.ordinal();
		int[] dirOffsets = new int[]{4, -3, 3, -2, 2, -1, 1, 0};
		for (int i = dirOffsets.length; --i >= 0; ) {
			int dirOffset = dirOffsets[i];
			Direction currentDirection = DataCache.directionArray[(desiredDirOffset + dirOffset + 8) % 8];
			if (rc.isActive() && rc.canMove(currentDirection) && rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
				if (canMoveDirection == null) {
					canMoveDirection = currentDirection;
					break;
				}
			}
		}
		// Otherwise, let's just spawn in the desired direction, and make sure to clear out a path later
		return canMoveDirection;
   }

}
