package random;

import battlecode.common.Clock;

public class Util {
	
	static int bn = Clock.getBytecodeNum();
	static int rn = Clock.getRoundNum();

	public static void randInit(int seed1, int seed2) {
    	bn = seed1;
    	rn = seed2;
	}

	private static int gen() {
    	bn = 36969 * (bn & 65535) + (bn >> 16);
    	rn = 18000 * (rn & 65535) + (rn >> 16);
    	return (bn << 16) + rn;
	}

	public static int randInt() {
		return gen();
	}

	public static double randDouble() {
		return (gen() * 2.32830644e-10 + 0.5);
	}

	public static double Random() {
		return randDouble();
	}
}