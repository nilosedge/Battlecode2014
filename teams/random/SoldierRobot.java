package random;

import java.util.Random;

import battlecode.common.Direction;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class SoldierRobot extends BaseRobot {

	public Random random;
	
	public SoldierRobot(RobotController rc) {
		super(rc);
		random = new Random(rc.getRobot().getID());
	}

	@Override
	public void run() {
		try {
			
			if(Math.random() < 0.02) {
				rc.construct(RobotType.PASTR);
			}
			
			Direction dir;
			if(Math.random() * 10 < 2) {
				dir = rc.getLocation().directionTo(rc.senseEnemyHQLocation());
			} else {
				dir = Direction.values()[(int)(Math.random() * 8)];
			}
			if(rc.isActive() && rc.canMove(dir)) {
				rc.move(dir);
			}
			
		} catch (Exception e) {
			
		}

	}

}
