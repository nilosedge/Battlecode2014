package milker_02;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class SoldierRobot extends BaseRobot {

	public SoldierState soldierState = SoldierState.NEW;
	public SoldierState nextSoldierState;
	
	public int move_out_round = 10000;
	
	public MapLocation currentLocation;

	public MapLocation rallyPoint;
	
	
	public SoldierRobot(RobotController rc) throws GameActionException {
		super(rc);
		
		NavSystem.init(this);
		
		rallyPoint = findRallyPoint();
	}

	public void run() {
		try {
			// Determine State
			DataCache.updateRoundVariables();
			currentLocation = rc.getLocation();
			rc.setIndicatorString(1, "Soldier State: " + soldierState.toString());
			switch(soldierState) {
				case NEW:
					newCode();
					break;
				case HERDING:
					herdingCode();
					break;
				case RIDEOUT:
					rideoutCode();
					break;
				case DEFENDING:
					defendingCode();
					break;
				case NOISETOWER:
					noiseTowerCode();
					break;
				case PASTRTOWER:
					pastrTowerCode();
					break;
				case ALL_IN:
					allInCode();
					break;
				case FIGHTING:
					fightingCode();
					break;
				case PUSHING:
					pushingCode();
					break;
				case RALLYING:
					rallyingCode();
					break;
			}
			
			if (nextSoldierState != null) {
				soldierState = nextSoldierState;
				nextSoldierState = null; // clear the state for the next call of run() to use
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void pushingCode() throws GameActionException {
		if (DataCache.numEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			fightingCode();
		} else {
			pushCodeGetCloser();
		}
	}
	
	public void pushCodeGetCloser() throws GameActionException {
		Robot[] enemiesList = rc.senseNearbyGameObjects(Robot.class, 100000, rc.getTeam().opponent());
		int[] closestEnemyInfo = getClosestEnemy(enemiesList);
		MapLocation closestEnemyLocation = new MapLocation(closestEnemyInfo[1], closestEnemyInfo[2]);
		pushCodeGetCloser(closestEnemyLocation);
	}
	
	public void pushCodeGetCloser(MapLocation destination) throws GameActionException {
		if (NavSystem.navMode != NavMode.GETCLOSER || (NavSystem.getDestination().x != destination.x && NavSystem.getDestination().y != destination.y)) {
			NavSystem.setupGetCloser(destination);
		}
		System.out.println("Pushing Trying to get closer");
		if (rc.isActive()) {
			NavSystem.tryMoveCloser();
		}
	}
	
	private void pastrTowerCode() {
	}

	private void noiseTowerCode() {
	}

	private void allInCode() {
	}

	private void defendingCode() {
	}

	private void rideoutCode() {
	}

	private void herdingCode() {
	}

	private void newCode() throws GameActionException {
		if (Clock.getRoundNum() < 50) {
			nextSoldierState = SoldierState.RALLYING;
			rallyingCode();
			//rideOutCode();
		} else {
			nextSoldierState = SoldierState.PUSHING;
			//havocingCode();
			pushingCode();
		}
	}


	private void rallyingCode() throws GameActionException {
		// If there are enemies nearby, trigger FIGHTING SoldierState
		if (DataCache.numEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			fightingCode();
		} else {
			nextSoldierState = SoldierState.PUSHING;
			pushingCode();
		}

	}

	private void fightingCode() throws GameActionException {
		if (DataCache.numEnemyRobots == 0) {
			if (DataCache.numAlliedSoldiers < Constants.FIGHTING_NOT_ENOUGH_ALLIED_SOLDIERS) {
				if (move_out_round <= Clock.getRoundNum()) {
					nextSoldierState = SoldierState.PUSHING;
					pushingCode();
				} else {
					Message msg;
					if (DataCache.onCycle) {
						msg = BroadcastSystem.readLastCycle(ChannelType.PASTRTOWER_SEEN);
					} else {
						msg = BroadcastSystem.read(ChannelType.PASTRTOWER_SEEN);
					}
					
					if (Clock.getRoundNum() >= 200 || msg.body == Constants.TRUE) {
						nextSoldierState = SoldierState.RALLYING;
						rallyingCode();
					} else {
						nextSoldierState = SoldierState.PUSHING;
						pushingCode();
					}
				}
			} else {
				nextSoldierState = SoldierState.PUSHING;
				pushingCode();
			}
		} else {
			microCode();
		}

	}

	public void microCode() throws GameActionException {
		Robot[] enemiesList = rc.senseNearbyGameObjects(Robot.class, 100000, rc.getTeam().opponent());
		int[] closestEnemyInfo = getClosestEnemy(enemiesList);
		MapLocation closestEnemyLocation = new MapLocation(closestEnemyInfo[1], closestEnemyInfo[2]);
		int enemyDistSquared = closestEnemyLocation.distanceSquaredTo(rc.getLocation());

		if (enemyDistSquared <= 2) {
			double[] our23 = getEnemies2Or3StepsAway();
			if (our23[0] < 1) {
				if (our23[1] >= 1) {
					NavSystem.goAwayFromLocation(closestEnemyLocation);
				}
			}
		} else if (enemyDistSquared == 16 || enemyDistSquared > 18 || DataCache.numNearbyAlliedSoldiers >= 3 * DataCache.numNearbyEnemySoldiers){ // if no enemies in one, two, or three dist
			if (rc.isActive()) {
				NavSystem.goToLocation(closestEnemyLocation);
			}
		} else {
			double[] our23 = getEnemies2Or3StepsAway();
			double[] enemy23 = getEnemies2Or3StepsAwaySquare(closestEnemyLocation, rc.getTeam().opponent());

			if (our23[1] > 0) {

				if (enemy23[0] > 0) {
					NavSystem.goToLocation(closestEnemyLocation);
				} else {
					if (enemy23[1] + enemy23[0] > our23[1] + our23[2]+1 || our23[1] + our23[2] < 1) {
						NavSystem.goToLocation(closestEnemyLocation);
					} else {
						NavSystem.goAwayFromLocation(closestEnemyLocation);
					}
				}

			} else { // closest enemy is 3 dist
				if (enemy23[0] > 0) {
					NavSystem.goToLocation(closestEnemyLocation);
//					rc.setIndicatorString(0, "forward4");
				} else if (enemy23[1] > 0) { // if enemy 2dist is > 0
					int closestDist = 100;
					int dist;
					MapLocation closestAllyLocation = null;
					Robot[] twoDistAllies = rc.senseNearbyGameObjects(Robot.class, closestEnemyLocation, 8, rc.getTeam());
					for (int i = twoDistAllies.length; --i >= 0; ) {
						Robot ally = twoDistAllies[i];
						RobotInfo arobotInfo = rc.senseRobotInfo(ally);
						dist = arobotInfo.location.distanceSquaredTo(rc.getLocation());
						if (dist<closestDist){
							closestDist = dist;
							closestAllyLocation = arobotInfo.location;
						}
					}

					double[] ally23 = getEnemies2Or3StepsAwaySquare(closestAllyLocation, rc.getTeam());

					if (enemy23[0] + enemy23[1] + enemy23[2] > ally23[1] + ally23[2]) {
						NavSystem.goToLocation(closestEnemyLocation);
					} else {
						NavSystem.goAwayFromLocation(closestEnemyLocation);
					}
				} else {
					if (enemy23[2] - our23[2] >= 3 || our23[2] < 1) {
						NavSystem.goToLocation(closestEnemyLocation);
					} else {
						NavSystem.goAwayFromLocation(closestEnemyLocation);
					}
					// otherwise, stay
				}
			}
		}
		if(rc.canAttackSquare(closestEnemyLocation) && rc.isActive() && rc.getActionDelay() <= 0) {
			rc.attackSquare(closestEnemyLocation);
		}
	}

	public int[] getClosestEnemy(Robot[] enemyRobots) throws GameActionException {
		
		MapLocation closestEnemy=rc.senseEnemyHQLocation(); // default to HQ
		//MapLocation closestEnemy = rc.senseHQLocation();
		int closestDist = 10000;
		
		int dist = 0;
		for (int i = enemyRobots.length; --i >= 0; ) {
			RobotInfo arobotInfo = rc.senseRobotInfo(enemyRobots[i]);
			if(arobotInfo.type != RobotType.HQ) {
				dist = arobotInfo.location.distanceSquaredTo(rc.getLocation());
				if (dist < closestDist) {
					closestDist = dist;
					closestEnemy = arobotInfo.location;
				}
			}
		}
		int[] output = new int[4];
		output[0] = closestDist;
		output[1] = closestEnemy.x;
		output[2] = closestEnemy.y;
		rc.setIndicatorString(0, "Closest: " + output[0] + ", " + output[1] + ", " + output[2] + ", " + output[3]);
		return output;
	}
	

	private double[] getEnemies2Or3StepsAway() throws GameActionException {
		double count1 = 0;
		double count2 = 0;
		double count3 = 0;
		Robot[] enemiesInVision = rc.senseNearbyGameObjects(Robot.class, 18, rc.getTeam().opponent());
		for (int i = enemiesInVision.length; --i >= 0; ) {
			Robot enemy = enemiesInVision[i];
			RobotInfo rinfo = rc.senseRobotInfo(enemy);
			int dist = rinfo.location.distanceSquaredTo(rc.getLocation());
			if (rinfo.type == RobotType.SOLDIER) {
				if (dist <= 2) {
					count1++;
				} else if (dist <=8) {
					count2++;
				} else if (dist > 8 && (dist <= 14 || dist == 18)) {
					count3++;
				}
			} else if(rinfo.type != RobotType.HQ) {
				if (dist <= 2) {
					count1 += 0.2;
				} else if (dist <=8) {
					count2 += 0.2;
				} else if (dist > 8 && (dist <= 14 || dist == 18)) {
					count3 += 0.2;
				}
			}
		}

		double[] output = {count1, count2, count3};
		return output;
	}

	private double[] getEnemies2Or3StepsAwaySquare(MapLocation square, Team squareTeam) throws GameActionException {
		double count1 = 0;
		double count2 = 0;
		double count3 = 0;
		if(square == null) {
			double[] output = {count1, count2, count3};
			return output;
		}
		Robot[] enemiesInVision = rc.senseNearbyGameObjects(Robot.class, square, 18, squareTeam.opponent());
		for (int i = enemiesInVision.length; --i >= 0; ) {
			Robot enemy = enemiesInVision[i];
			RobotInfo rinfo = rc.senseRobotInfo(enemy);
			int dist = rinfo.location.distanceSquaredTo(square);
			if (rinfo.type == RobotType.SOLDIER) {
				if (dist <= 2) {
					count1++;
				} else if (dist <=8) {
					count2++;
				} else if (dist <= 14 || dist == 18) {
					count3++;
				}
			} else if(rinfo.type != RobotType.HQ) {
				if (dist <= 2) {
					count1 += 0.2;
				} else if (dist <=8) {
					count2 += 0.2;
				} else if (dist <= 14 || dist == 18) {
					count3 += 0.2;
				}
			}
		}

		int selfDist = square.distanceSquaredTo(rc.getLocation());
		
		if (selfDist <= 2) {
			count1++;
		} else if (selfDist <= 8) {
			count2++;
		} else if (selfDist <= 14 || selfDist == 18) {
			count3++;
		}

		double[] output = {count1, count2, count3};
		return output;
	}

	private MapLocation findRallyPoint() {
		return DataCache.enemyHQLocation;
//		MapLocation enemyLoc = DataCache.enemyHQLocation;
//		MapLocation ourLoc = DataCache.ourHQLocation;
//		int x, y;
//		x = (enemyLoc.x + 3 * ourLoc.x) / 3;
//		y = (enemyLoc.y + 3 * ourLoc.y) / 3;
//		return new MapLocation(x,y);
	}
	
}
