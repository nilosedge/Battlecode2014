package milker_02;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.GameObject;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class JobSystem {
	public static BaseRobot robot;
	public static RobotController rc;
	public static MapLocation goalLoc;
	
	public static MapLocation pathCenter;
	public static MapLocation pathCenterSlightlyCloserToUs;
	
	public static ChannelType[] pastrJobChannelList = 
		{ChannelType.PST1,
		 ChannelType.PST2,
		 ChannelType.PST3,
		 ChannelType.PST4,
		 ChannelType.PST5};

	public static ChannelType[] pastrCompletionChannelList =
		{ChannelType.COMP1,
		 ChannelType.COMP2,
		 ChannelType.COMP3,
		 ChannelType.COMP4,
		 ChannelType.COMP5};
	
	public static int maxPastrJobs = pastrJobChannelList.length;
	public static int maxMessage = Constants.MAX_MESSAGE; //24-bit message all 1s	
	
	public static MapLocation[] pastrJobs = new MapLocation[maxPastrJobs];
	public static ChannelType[] pastrChannels = new ChannelType[maxPastrJobs];

	public static int numPastrsNeeded; // must be less than pastrJobChannelList.length
	
	public static FastLocSet unreachablePastrs = new FastLocSet();
	public static int numUnreachablePastrs;
	
	public static RobotType assignedRobotType;
	public static ChannelType assignedChannel;
	public static ChannelType pastrChannel;
	public static ChannelType pastrCompChannel;
	
	public static int supCount;
	public static int genCount;
	
	public static double supGenRatio;
	
	public static int artCount;
	
	public static int hardPastrsLimit;

	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
	}
	
	public static void initializeConstants() throws GameActionException {
		numPastrsNeeded = Constants.INITIAL_NUM_PASTRS_NEEDED; 
		numUnreachablePastrs = 0;
		supCount = 0;
		genCount = 0;
		artCount = 0;
		hardPastrsLimit = 2;
		pathCenter = getPathCenter();
		pathCenterSlightlyCloserToUs = getPathCenterSlightlyCloserToUs();
	}
	
	public static MapLocation getPathCenter() {
		int x1 = DataCache.ourHQLocation.x;
		int y1 = DataCache.ourHQLocation.y;
		int x2 = DataCache.enemyHQLocation.x;
		int y2 = DataCache.enemyHQLocation.y;
		return new MapLocation((x1+x2)/2, (y1+y2)/2);
	}
	
	public static MapLocation getPathCenterSlightlyCloserToUs() {
		int x1 = DataCache.ourHQLocation.x;
		int y1 = DataCache.ourHQLocation.y;
		int x2 = DataCache.enemyHQLocation.x;
		int y2 = DataCache.enemyHQLocation.y;
		return new MapLocation((int) (0.6 * x1 + 0.4 * x2), (int) (0.6 * y1 + 0.4 * y2));
	}
	

	/**
	 * HQ uses this to post a new job, and increments genCount and supCount
	 * @param channel
	 * @param loc
	 */
	public static void postJob(ChannelType channel, MapLocation loc, int robType) {
		BroadcastSystem.write(channel, createMessage(loc, false, true, robType));
		switch(robType) {
		case 1:
			genCount++;
			break;
		case 0:
			supCount++;
			break;
		case 2:
			artCount++;
			break;
		}
	}
	
	/**
	 * same as above, except without incrementing
	 * @param channel
	 * @param loc
	 */
	public static void postJobWithoutIncrementing(ChannelType channel, MapLocation loc, int robType) {
		BroadcastSystem.write(channel, createMessage(loc, false, true, robType));
	}
	
	/**
	 * Soldiers use this to update the job to claim that it's taken
	 * They also use this every turn to ping back to others
	 * @param channel
	 */
	public static void updateJobTaken() {
		int robotTypeInt = 0;
		switch(assignedRobotType) {
		case HQ:
			robotTypeInt = 0;
			break;
		case NOISETOWER:
			robotTypeInt = 1;
			break;
		case PASTR:
			robotTypeInt = 2;
			break;
		case SOLDIER:
			robotTypeInt = 3;
			break;
		default:
			break;	
		}
		BroadcastSystem.write(assignedChannel, createMessage(goalLoc, true, true, robotTypeInt));
	}
	
	/**
	 * Soldiers use this to find an untaken job or a taken job that has been inactive
	 * @return ChannelType channel
	 * @throws GameActionException 
	 */
	public static ChannelType findJob() throws GameActionException {
		int currentRoundNum = Clock.getRoundNum();
		
		
		ChannelType channel = pastrChannel;
		Message message = BroadcastSystem.read(channel);
		int onOrOff;
		int isTaken;
		
		if (message.isValid && message.body != maxMessage) {
			
//			System.out.println("msgbody: " + message.body);
			onOrOff = parseOnOrOff(message.body);
			isTaken = parseTaken(message.body);
			
//			String s = "onOrOff: " + onOrOff + " isTaken: " + isTaken + " location: " + parseLocation(message.body) + " robType: " + parseRobotType(message.body);
			
//			rc.setIndicatorString(0, s);
			
			if (onOrOff == 1 && isTaken == 0) { //if job is on and untaken
				goalLoc = parseLocation(message.body);
				if (rc.canSenseSquare(goalLoc)) {
					GameObject robotOnSquare = rc.senseObjectAtLocation(goalLoc);
					if (robotOnSquare == null || !robotOnSquare.getTeam().equals(rc.getTeam())) {
						assignedRobotType = parseRobotType(message.body);
						assignedChannel = channel;
//						rc.setIndicatorString(1, goalLoc.toString());
						return channel;
					}
				} else {
					assignedRobotType = parseRobotType(message.body);
					assignedChannel = channel;
//					rc.setIndicatorString(1, goalLoc.toString());
					return channel;
				}
			} else if (onOrOff == 1 && isTaken == 1) {
				int postedRoundNum = parseRoundNum(message.body);
				if ((16+currentRoundNum - postedRoundNum)%16 >= 4) { // it hasn't been written on for 5 turns
					goalLoc = parseLocation(message.body);

//					System.out.println("posted: " + postedRoundNum);
//					System.out.println("current: " + currentRoundNum % 16);
//
					if (rc.canSenseSquare(goalLoc)) {
						GameObject robotOnSquare = rc.senseObjectAtLocation(goalLoc);
						if (robotOnSquare == null || !robotOnSquare.getTeam().equals(rc.getTeam())) {
							assignedRobotType = parseRobotType(message.body);
							assignedChannel = channel;
//							rc.setIndicatorString(1, assignedRobotType.toString());
							return channel;
						}
					} else {
						assignedRobotType = parseRobotType(message.body);
						assignedChannel = channel;
//						rc.setIndicatorString(1, assignedRobotType.toString());
						return channel;
					}
				}
			}
			
		}
		
//		rc.setIndicatorString(1, "not shield");
		
		for (int i = pastrJobChannelList.length; --i >= 0; ) {
			channel = pastrJobChannelList[i];
			message = BroadcastSystem.read(channel);
			
			
//			System.out.println("findJobChannel: " + channel.toString());
//			System.out.println("channelIsValid: " + message.isValid);
//			System.out.println("channelBody: " + message.body);

			if (message.isValid && message.body != maxMessage) {
				onOrOff = parseOnOrOff(message.body);
				isTaken = parseTaken(message.body);
				if (onOrOff == 1 && isTaken == 0) { //if job is on and untaken
					goalLoc = parseLocation(message.body);
					if (rc.canSenseSquare(goalLoc)) {
						GameObject robotOnSquare = rc.senseObjectAtLocation(goalLoc);
						if (robotOnSquare == null || !robotOnSquare.getTeam().equals(rc.getTeam())) {
							assignedRobotType = parseRobotType(message.body);
							assignedChannel = channel;
//							rc.setIndicatorString(1, goalLoc.toString());
							return channel;
						}
					} else {
						assignedRobotType = parseRobotType(message.body);
						assignedChannel = channel;
//						rc.setIndicatorString(1, goalLoc.toString());
						return channel;
					}
				} else if (onOrOff == 1) {
					int postedRoundNum = parseRoundNum(message.body);
					if ((16+currentRoundNum - postedRoundNum)%16 >= 4) { // it hasn't been written on for 5 turns
						goalLoc = parseLocation(message.body);
//						System.out.println("posted: " + postedRoundNum);
//						System.out.println("current: " + currentRoundNum % 16);
//
						if (rc.canSenseSquare(goalLoc)) {
							GameObject robotOnSquare = rc.senseObjectAtLocation(goalLoc);
							if (robotOnSquare == null || !robotOnSquare.getTeam().equals(rc.getTeam())) {
								assignedRobotType = parseRobotType(message.body);
								assignedChannel = channel;
//								rc.setIndicatorString(1, goalLoc.toString());
								return channel;
							}
						} else {
							assignedRobotType = parseRobotType(message.body);
							assignedChannel = channel;
//							rc.setIndicatorString(1, goalLoc.toString());
							return channel;
						}
					}
				}

			}
		}
		return null;
	}
	
	/**
	 * Encampments use this at its birth to indicate to the HQ
	 * that the encampment is finished
	 * @param currLoc
	 * @return ChannelType
	 */
	public static void postCompletionMessage(MapLocation currLoc) {
		forloop: for (int i = pastrCompletionChannelList.length; --i >= 0; ) {
			ChannelType channel = pastrCompletionChannelList[i];
			Message message = BroadcastSystem.read(channel);
			if (message.isUnwritten || (message.isValid && message.body == maxMessage)) {
				int newmsg = (currLoc.x << 8) + currLoc.y;
				BroadcastSystem.write(channel, newmsg);
				break forloop; 
			}
		}
	}
	
	public static void postUnreachableMessage(MapLocation goalLoc) {
		if (assignedRobotType == RobotType.NOISETOWER) {
			Message message = BroadcastSystem.read(pastrCompChannel);
			if (message.isUnwritten || (message.isValid && message.body == maxMessage)){
				int newmsg = (1 << 16) + (goalLoc.x << 8) + goalLoc.y;
//				System.out.println("unreachable msg: " + newmsg);
				BroadcastSystem.write(pastrCompChannel, newmsg);
			}
		} else {
			forloop: for (int i = pastrCompletionChannelList.length; --i >= 0; ) {
				ChannelType channel = pastrCompletionChannelList[i];
				Message message = BroadcastSystem.read(channel);
				if (message.isUnwritten || (message.isValid && message.body == maxMessage)){
					int newmsg = (1 << 16) + (goalLoc.x << 8) + goalLoc.y;
//					System.out.println("unreachable msg: " + newmsg);
					BroadcastSystem.write(channel, newmsg);
					break forloop; 
				}
			}
		}
		
	}
	
	/**
	 * In the round after its birth (or 5 after), the encampment "cleans up after itself"
	 * and posts 0 in the channel
	 * @param channel
	 */
	public static void postCleanUp(ChannelType channel) {
		BroadcastSystem.writeMaxMessage(channel);
	}

	/**
	 * HQ uses this to see if a certain channel contains a maplocation
	 * of a certain completed encampment. Returns null if 
	 * @param channel
	 * @return
	 */
	public static Message.Type checkCompletion(ChannelType channel) {
		Message message = BroadcastSystem.read(channel);
		if (message.isValid && message.body != maxMessage) {
			
			int locY = message.body & 0xFF;
			int locX = (message.body >> 8) & 0xFF;
			int unreachableBit = message.body >> 16;
			postCleanUp(channel); // cleanup
//			System.out.println("locy: " + locY + " locx: " + locX);
			if (unreachableBit == 1) { // if unreachable
				unreachablePastrs.add(new MapLocation(locY, locX));
				numUnreachablePastrs++;
				return Message.Type.FAILURE;
			} else { // it's a completion message
				return Message.Type.COMPLETION;
			}
		}
		return Message.Type.EMPTY;
	}
	

	
	/**
	 * HQ uses this to see if a certain channel contains a maplocation
	 * of a certain completed encampment. Returns null if 
	 * @param channel
	 * @return
	 */
	public static Message.Type checkCompletionOnCycle(ChannelType channel) {
		Message message = BroadcastSystem.readLastCycle(channel);
		if (message.isValid && message.body != maxMessage) {
			
			int locY = message.body & 0xFF;
			int locX = (message.body >> 8) & 0xFF;
			int unreachableBit = message.body >> 16;
//			System.out.println("locy: " + locY + " locx: " + locX);
			if (unreachableBit == 1) { // if unreachable
				unreachablePastrs.add(new MapLocation(locY, locX));
				numUnreachablePastrs++;
				return Message.Type.FAILURE;
			} else { // it's a completion message
				return Message.Type.COMPLETION;
			}
		}
		return Message.Type.EMPTY;
	}
	
	
	/** 
	 * Returns a boolean: true if one of the channels has a 
	 * completion or a failure message, false otherwise
	 * @return
	 */
	public static boolean checkAllCompletion() {
		for (int i = pastrCompletionChannelList.length; --i >= 0; ) {
			ChannelType channel = pastrCompletionChannelList[i];
			Message.Type msgType = checkCompletion(channel);
			if (msgType != Message.Type.EMPTY) { // if a completion or a failure message
				return true;
			} 
		}

		return false;
	}
	
	public static boolean checkAllCompletionOnCycle() {
		for (int i = pastrCompletionChannelList.length; --i >= 0; ) {
			ChannelType channel = pastrCompletionChannelList[i];
			Message.Type msgType = checkCompletionOnCycle(channel);
			if (msgType != Message.Type.EMPTY) { // if a completion or a failure message
				return true;
			} 
		}
		
		return false;
	}
	
	/**
	 * The HQ calls this at the beginning of every broadcast cycle
	 * to persist old job messages that were untaken
	 */
	public static void persistChannelsOnCycle() {
		for (int i = pastrChannels.length; --i >= 0; ) { // persist on encampment channels
			ChannelType channel = pastrChannels[i];
			Message msgLastCycle = BroadcastSystem.readLastCycle(channel);
//			System.out.println("isValid: " + msgLastCycle.isValid);
//			System.out.println("body: " + msgLastCycle.body);
			if (msgLastCycle.isValid && msgLastCycle.body != maxMessage) {
				// check if the job was on and was untaken
				if (parseOnOrOff(msgLastCycle.body) == 1) {
					if (parseTaken(msgLastCycle.body) == 0) {
	//					System.out.println("hello!!!!!");
	//					System.out.println("channel: " + channel.toString());
						int robotTypeToBuild = parseRobotTypeInt(msgLastCycle.body);
						postJobWithoutIncrementing(channel, parseLocation(msgLastCycle.body), robotTypeToBuild);
					} else if (parseTaken(msgLastCycle.body) == 1) {
						int postedRoundNum = parseRoundNum(msgLastCycle.body);
						if (((16+Clock.getRoundNum() - postedRoundNum) & 0xF) >= 4) { // it hasn't been written on for 5 turns
	//						System.out.println("hello!!!!!");
	//						System.out.println("channel: " + channel.toString());
							int robotTypeToBuild = parseRobotTypeInt(msgLastCycle.body);
							postJobWithoutIncrementing(channel, parseLocation(msgLastCycle.body), robotTypeToBuild);
						}
					}
				}
			}
		}
		
		// persist for shields
		Message msgLastCycle = BroadcastSystem.readLastCycle(pastrChannel);
//		System.out.println("isValid: " + msgLastCycle.isValid);
//		System.out.println("body: " + msgLastCycle.body);

		if (msgLastCycle.isValid && msgLastCycle.body != maxMessage) {
			// check if the job was on and was untaken
			if (parseOnOrOff(msgLastCycle.body) == 1 && parseTaken(msgLastCycle.body) == 0) {
//				System.out.println("hello!!!!!");
//				System.out.println("channel: " + channel.toString());
				int robotTypeToBuild = 3;
				postJobWithoutIncrementing(pastrChannel, parseLocation(msgLastCycle.body), robotTypeToBuild);
			} else if (parseOnOrOff(msgLastCycle.body) == 1 && parseTaken(msgLastCycle.body) == 1) {
				int postedRoundNum = parseRoundNum(msgLastCycle.body);
				if ((16+Clock.getRoundNum() - postedRoundNum)%16 >= 4) { // it hasn't been written on for 5 turns
//					System.out.println("hello!!!!!");
//					System.out.println("channel: " + channel.toString());
					int robotTypeToBuild = 3;
					postJobWithoutIncrementing(pastrChannel, parseLocation(msgLastCycle.body), robotTypeToBuild);
				}
			}
		}
		
	}
	/**
	 * returns new list of channels corresponding to the new jobs
	 * that will maintain the same channels for the old jobs
	 * but find new channels for the new ones
	 * @param newJobsList
	 * @param oldJobsList
	 * @param oldChannelsList
	 * @return
	 */
	public static ChannelType[] assignChannels(MapLocation[] newJobsList, MapLocation[] oldJobsList, ChannelType[] oldChannelsList) {
		ChannelType[] channelList = new ChannelType[maxPastrJobs];

		int arrayIndices[] = new int[newJobsList.length];

		int channelIndex = -1;
		// keep old channels for non-new jobs
		for (int i = newJobsList.length; --i >= 0; ) {
			arrayIndices[i] = arrayIndex(newJobsList[i], oldJobsList);
			if (arrayIndices[i] != -1 && newJobsList[i] != null) { // if the job is not new, keep the same channel
				channelList[i] = oldChannelsList[arrayIndices[i]];
			}
		}
		
		// allocate unused channels for new jobs
		for (int i = newJobsList.length; --i >= 0; ) {
			if (arrayIndices[i] == -1 && newJobsList[i] != null) {
				channelLoop: for (ChannelType channel: pastrJobChannelList) {
					channelIndex = arrayIndex(channel, channelList);
					if (channelIndex == -1) { // if not already used, use it and post job
						channelList[i] = channel;
						int robotTypeToBuild = getRobotTypeToBuild(newJobsList[i]);
						postJob(channel, newJobsList[i], robotTypeToBuild);
						break channelLoop;
					}
				}
			}
		}
		return channelList;
	}
	
	/** 
	 * returns index of element e in array
	 * @param e
	 * @param array
	 * @return
	 */
	public static int arrayIndex(Object e, Object[] array) {
		if (e == null) {
			return -1;
		}

		for (int i = array.length; --i >= 0; ) {
			if (array[i] != null && array[i].equals(e)) {
				return i;
			}
		}
		return -1;
	}
	
//	/**
//	 * checks nearby encampments to see if jobs need to be changed
//	 * @throws GameActionException
//	 */
//	public static void updateJobs() throws GameActionException {
//		MapLocation[] neutralEncampments = null;
//		MapLocation[] alliedEncampments = null;
//		if (Clock.getRoundNum() == 0) {
//			neutralEncampments = rc.senseEncampmentSquares(DataCache.ourHQLocation, 10000, Team.NEUTRAL);
//			
//			if (Clock.getRoundNum() > 50) {
//				hardPastrsLimit = Integer.MAX_VALUE;
//				int numReachableEncampments = neutralEncampments.length - numUnreachablePastrs;
//				if (numReachableEncampments == 0) {
//					numPastrsNeeded = 0;
//				} else if (DataCache.rushDist < 70) {
//					numPastrsNeeded = 3;
//				} else {
//					numPastrsNeeded = 4;
//				}
//			}
//			
//			if (numPastrsNeeded > neutralEncampments.length - numUnreachablePastrs){
//				numPastrsNeeded = neutralEncampments.length - numUnreachablePastrs;
//			}
//			
//			if (DataCache.numAlliedPastrs >= hardPastrsLimit) {
//				numPastrsNeeded = 0;
//			}
//
//		} else {
//			alliedEncampments = rc.senseEncampmentSquares(DataCache.ourHQLocation, 10000, rc.getTeam());
//			
//			if (Clock.getRoundNum() > 50) {
//				hardPastrsLimit = Integer.MAX_VALUE;
//				int numReachableEncampments = sortedNeutralEncLocs.length - alliedEncampments.length - numUnreachablePastrs;
//				if (numReachableEncampments == 0) {
//					numPastrsNeeded = 0;
//				} else if (DataCache.rushDist < 70) {
//					numPastrsNeeded = 3;
//				} else {
//					numPastrsNeeded = 4;
//				}
//			}
//			
//			if (numPastrsNeeded > sortedNeutralEncLocs.length - alliedEncampments.length - numUnreachablePastrs) {
//				numPastrsNeeded = sortedNeutralEncLocs.length - alliedEncampments.length - numUnreachablePastrs;
//			}
//			
//			if (DataCache.numAlliedPastrs >= hardPastrsLimit) {
//				numPastrsNeeded = 0;
//			}
//		}
//		
//
//		if (numPastrsNeeded != 0) {
//			MapLocation[] newJobsList;
//			if (Clock.getRoundNum() == 0) {
//				newJobsList = getBestEncampmentLocations(DataCache.ourHQLocation, neutralEncampments, numEncampmentsNeeded);
//			} else {
//				newJobsList = getBestEncampmentLocationsFromAlliedLoc(DataCache.ourHQLocation, alliedEncampments, numEncampmentsNeeded);
//			}
//
//			ChannelType[] channelList = EncampmentJobSystem.assignChannels(newJobsList, EncampmentJobSystem.encampmentJobs, EncampmentJobSystem.encampmentChannels);
//
//			for (int i = numEncampmentsNeeded; --i >= 0; ) { // update lists
//				EncampmentJobSystem.encampmentJobs[i] = newJobsList[i];
//				EncampmentJobSystem.encampmentChannels[i] = channelList[i];
//			}
//
//
//			// clear unused channels
//			for (int i = encampmentJobChannelList.length; --i >= 0; ) {
//				ChannelType channel = encampmentJobChannelList[i];
//				if (arrayIndex(channel, EncampmentJobSystem.encampmentChannels) == -1) { // if unused
//					BroadcastSystem.writeMaxMessage(channel); // reset the channel
//				}
//			}
//		}
//	}

	/**
	 * HQ uses this to check if any jobs are completed, and then updates new jobs
	 * @throws GameActionException
	 */
	public static void updateJobsAfterChecking() throws GameActionException {
		if (JobSystem.checkAllCompletion()) { // if it contains non-null elements
			//updateJobs();
		}
	}
	
	public static void updateJobsOnCycle() throws GameActionException {
		persistChannelsOnCycle();
		checkAllCompletionOnCycle();
		//updateJobs();
	}
	

	
	public static int getRobotTypeToBuild(MapLocation loc) {
		if (supCount < 2 && genCount == 0) {
//			System.out.println("supplier");

			return 0; // supplier
		}
		if (((double) supCount)/(supCount + genCount) > supGenRatio) {
//			System.out.println("generator");

			return 1; // generator
		} else {
//			System.out.println("supplier");

			return 0; // supplier
		}		
	}
	




	
	/**
	 * Creates a 24-bit job message to send from the goal location
	 * @param goalLoc
	 * @return
	 */
	public static int createMessage(MapLocation goalLoc, boolean isTaken, boolean onOrOff, int robType) {
		int msg = robType << 22;
		
		msg += (goalLoc.x << 14);
		msg += (goalLoc.y << 6);
		if (isTaken) {
			msg += 0x20;
		}
		if (onOrOff) {
			msg += 0x10;
		}
		msg += Clock.getRoundNum() & 0xF;
		
		return msg;
	}
	
	/**
	 * creates shield message with the location of the shield
	 * @param shieldLoc
	 * @return
	 */
	public static int createShieldLocMessage(MapLocation shieldLoc) {
		return shieldLoc.x + (shieldLoc.y << 8);
	}
	/** parses message in the channel SHIELD_LOCATION
	 * 
	 * @param msgBody
	 * @return
	 */
	public static MapLocation parseShieldLocation(int msgBody) {
		int x = msgBody & 0xFF;
		int y = (msgBody >> 8) & 0xFF;
		return new MapLocation(x,y);
	}
	/**
	 * Parses a 24-bit job-message
	 * @param msgBody
	 * @return an array containing round mod 16, onOrOff, job taken or not, y-coord, x-coord, and encampment type
	 */
	public static int[] parseEntireJobMessage(int msgBody) {
		int[] output = new int[6];
		output[0] = msgBody & 0xF; // round # mod 16
		output[1] = (msgBody >> 4) & 0x1; // on or off
		output[2] = (msgBody >> 5) & 0x1; // job taken or not
		output[3] = (msgBody >> 6) & 0xFF; // y-coord of location
		output[4] = (msgBody >> 14) & 0xFF; // x-coord of location
		output[5] = (msgBody >> 22);
		
		return output;
	}
	
	public static int parseRoundNum(int msgBody) { // round number mod 16
		return msgBody & 0xF;
	}
	
	public static int parseOnOrOff(int msgBody) { // job on or off
		return (msgBody >> 4) & 0x1;
	}
	
	public static int parseTaken(int msgBody) { // job taken or not
		return (msgBody >> 5) & 0x1; 
	}
	
	public static MapLocation parseLocation(int msgBody) {
		int y = (msgBody >> 6) & 0xFF; // y-coord of location
		int x = (msgBody >> 14) & 0xFF; // x-coord of location
		return new MapLocation(x,y);
	}
	
	public static RobotType parseRobotType(int msgBody) {
		int robotTypeInt = msgBody >> 22;
		if (robotTypeInt == 0) {
			return RobotType.HQ;
		} else if (robotTypeInt == 1) {
			return RobotType.NOISETOWER;
		} else if (robotTypeInt == 2) {
			return RobotType.PASTR;
		} else { // 3
			return RobotType.SOLDIER;
		}
	}
	
	public static int parseRobotTypeInt(int msgBody) {
		return msgBody >> 22;
	}
	
//	public static void main(String[] arg0) {
//		int[] output = parseEntireJobMessage(0b0000111100001110011111);
//		System.out.println("parsed: " + output[0]);
//		System.out.println("parsed: " + output[1]);
//		System.out.println("parsed: " + output[2]);
//		System.out.println("parsed: " + output[3]);
//		System.out.println("parsed: " + output[4]);
//		System.out.println("parsed: " + output[5]);
//
//		
//		System.out.println("message: " + createMessage(new MapLocation(15,14), false, true, 0));
//	}
}
