package milker_02;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.TerrainTile;

public class HQRobot extends BaseRobot {

	public ChannelType genCountChannel = ChannelType.GEN_COUNT;
	public boolean spawned = false;

	public HQRobot(RobotController rc) throws GameActionException {
		super(rc);
		
		int array[] = getMapLocations();
		rc.broadcast(60000, array.length);
		for(int i = 0; i < array.length; i++) {
			rc.broadcast(i + 60001, array[i]);
		}
	}

	public void run() {
		try {
			DataCache.updateRoundVariables();
			// Check for enemys
			// Attack them
			// Spawn more soldiers

			if(!spawned) {
				spawn();
				spawned = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void spawn() throws GameActionException {
		Direction desiredDir = rc.getLocation().directionTo(DataCache.enemyHQLocation);
		Direction dir = getSpawnDirection(desiredDir);
		if (dir != null) {
			rc.spawn(dir);
		}
	}

	private Direction getSpawnDirection(Direction dir) {
		Direction canMoveDirection = null;
		int desiredDirOffset = dir.ordinal();
		int[] dirOffsets = new int[]{4, -3, 3, -2, 2, -1, 1, 0};
		for (int i = dirOffsets.length; --i >= 0; ) {
			int dirOffset = dirOffsets[i];
			Direction currentDirection = DataCache.directionArray[(desiredDirOffset + dirOffset + 8) % 8];
			if (rc.isActive() && rc.canMove(currentDirection) && rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
				if (canMoveDirection == null) {
					canMoveDirection = currentDirection;
					break;
				}
			}
		}
		// Otherwise, let's just spawn in the desired direction, and make sure to clear out a path later
		return canMoveDirection;
	}


	private int[] getMapLocations() {
		int array[] = new int[(rc.getMapHeight() * rc.getMapWidth() / 16) + 1];
		int count = 0;
		int idx = 0;
		int mod = 0;
		for(int i = 0; i < rc.getMapHeight(); i++) {
			for(int j = 0; j < rc.getMapWidth(); j++) {
				TerrainTile tile = rc.senseTerrainTile(new MapLocation(j, i));
				idx = count / 16;
				mod = count % 16;
				array[idx] |= tile.ordinal() << (mod * 2);
				count++;
			}
		}
		return array;
	}

}
