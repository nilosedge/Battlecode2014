package milker_02;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.TerrainTile;


/**
 * The data map from our example game. This holds the state and context of each tile
 * on the map. It also implements the interface required by the path finder. It's implementation
 * of the path finder related methods add specific handling for the types of units
 * and terrain in the example game.
 * 
 * @author Kevin Glass
 */
public class NavSystemMap {

	public int WIDTH;
	public int HEIGHT;

	private boolean[][] visited;
	
	public TerrainTile[][] map;
	public RobotController rc;
	
	public NavSystemMap(RobotController rc) {
		this.rc = rc;
		WIDTH = rc.getMapWidth();
		HEIGHT = rc.getMapHeight();
		visited = new boolean[WIDTH][HEIGHT];
	}
	
	public void printMap() {
		for(int k = 0; k < rc.getMapHeight(); k++) {
			for(int i = 0; i < rc.getMapWidth(); i++) {
				System.out.print(map[i][k].ordinal());
			}
			System.out.println();
		}
	}
	
	public void readMap() throws GameActionException {
		map = new TerrainTile[rc.getMapWidth()][rc.getMapHeight()];
		int mapSize = rc.readBroadcast(60000);
		int count = 0;
		for(int i = 0; i < mapSize; i++) {
			int data = rc.readBroadcast(i + 60001);
			for(int j = 0; j < 16; j++) {
				map[count % rc.getMapWidth()][(count / rc.getMapHeight())] = TerrainTile.values()[(3 & (data >> (j * 2)))];
				count++;
				if(count >= (rc.getMapHeight() * rc.getMapWidth())) {
					break;
				}
			}
			if(count >= (rc.getMapHeight() * rc.getMapWidth())) {
				break;
			}
		}
	}

	public boolean blocked(int x, int y) {
		if(map[x][y] == TerrainTile.OFF_MAP) {
			return true;
		}
		if(map[x][y] == TerrainTile.VOID) {
			return true;
		}
		return false;
	}

	public float getCost(int sx, int sy, int tx, int ty) {
		if(map[tx][ty] == TerrainTile.ROAD) {
			return (float)0.5;
		} else {
			return 1;
		}
	}

	public int getHeightInTiles() {
		return WIDTH;
	}

	public int getWidthInTiles() {
		return HEIGHT;
	}

	public void pathFinderVisited(int x, int y) {
		visited[x][y] = true;
	}
	
}
