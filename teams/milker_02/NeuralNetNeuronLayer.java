package milker_02;

import java.util.ArrayList;
import java.util.LinkedList;

public class NeuralNetNeuronLayer implements NeuralNetNode {
	
	public LinkedList<NeuralNetNeuron> neuronsList = new LinkedList<NeuralNetNeuron>();
	public NeuralNetNeuron bias = new NeuralNetNeuron();

	public NeuralNetNeuronLayer(int numNeurons) {
		for (int i = 0; i < numNeurons; i++) {
			neuronsList.add(new NeuralNetNeuron());
		}
	}
	
	public void feedForward(ArrayList<Double> inputValues) {
		for(int i = 0; i < neuronsList.size(); i++) {
			neuronsList.get(i).feedForward(inputValues.get(i));
		}
		bias.feedForward(1.0);
	}
	
	public void feedForward() {
		for(int i = 0; i < neuronsList.size(); i++) {
			neuronsList.get(i).feedForward();
		}
		if(bias.getOutputs().size() > 0) {
			bias.feedForward(1.0);
		}
	}
	
	public void calcDelta(ArrayList<Double> targetValues) {
		for(int i = 0; i < neuronsList.size(); i++) {
			neuronsList.get(i).calcDelta(targetValues.get(i));
		}
	}
	
	public void calcDelta() {
		for(NeuralNetNeuron n: neuronsList) {
			n.calcDelta();
		}
		bias.calcDelta();
	}
	
	public void calcGradients() {
		for(NeuralNetNeuron n: neuronsList) {
			n.calcGradients();
		}
	}
	
	public ArrayList<Double> getResults() {
		ArrayList<Double> ret = new ArrayList<Double>();
		for(int i = 0; i < neuronsList.size(); i++) {
			ret.add(neuronsList.get(i).getOutputValue());
		}
		return ret;
	}
	
	public void Accept(NeuralNetNodeVisitor visitor) {
		visitor.Visit(this);
	}

}