package milker_02;

import java.util.ArrayList;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class NavSystem {
	
	public static SoldierRobot robot;
	public static RobotController rc;
	
	public static int[] directionOffsets;
	
	// Used by both smart and backdoor nav
	public static NavMode navMode = NavMode.REGULAR;
	public static MapLocation currentWaypoint;
	private static MapLocation destination;
	
	//private static PathFinder finder;
	private static NavSystemMap map;
	
	
	

	/**
	 * MUST CALL THIS METHOD BEFORE USING NavSystem
	 * @param myRC
	 * @throws GameActionException 
	 */
	public static void init(SoldierRobot myRobot) throws GameActionException {
		robot = myRobot;
		rc = robot.rc;
		
		// Randomly assign soldiers priorities for trying to move left or right
		if (rc.getRobot().getID() % 4 <= 1) {
			directionOffsets = new int[]{0,1,-1,2,-2,3,-3};
		} else {
			directionOffsets = new int[]{0,-1,1,-2,2,-3,3};
		}
		
		
		map = new NavSystemMap(rc);
		map.readMap();
		
		//finder = new PathFinder(map, 500, true);
		// Get map
		
		//map.printMap();

	}

	public static void goToLocation(MapLocation location) throws GameActionException {
		Direction dir = rc.getLocation().directionTo(location);
		if (dir != Direction.OMNI) {
			goDirection(dir);
		}
	}
	
	public static void goAwayFromLocation(MapLocation location) throws GameActionException {
		Direction dir = rc.getLocation().directionTo(location).opposite();
		if (dir != Direction.OMNI) {
			goDirection(dir);
		}
	}
	
	private static void goDirection(Direction dir) throws GameActionException {
		if(rc.isActive()) {
			Direction lookingAtCurrently = dir;
			for (int d : directionOffsets) {
				lookingAtCurrently = Direction.values()[(dir.ordinal() + d + 8) % 8];
				if (rc.isActive() && rc.canMove(lookingAtCurrently)) {
					rc.move(lookingAtCurrently);
					break;
				}
			}
		}
	}
	
	private static boolean move(Direction dir) throws GameActionException {
		if (rc.isActive()) {
			if (rc.canMove(dir)) {
				rc.move(dir);
				return true;
			} 
			return false;
		}
		return false;
	}
	
	
	public static void setupGetCloser(MapLocation endLocation) throws GameActionException {
		navMode = NavMode.GETCLOSER;
		destination = endLocation;
	}
	
	public static boolean tryMoveCloser() throws GameActionException {
		// first try to get closer
		//ArrayList<PathFinder.Node> steps = finder.findPath(rc.getLocation(), destination);
		//System.out.println("Path: " + steps);
		
		double distance = rc.getLocation().distanceSquaredTo(destination);
		for (int i = 8; --i >= 0; ) {
			if (rc.canMove(DataCache.directionArray[i])) {
				MapLocation nextLoc = rc.getLocation().add(DataCache.directionArray[i]);
				if (nextLoc.distanceSquaredTo(destination) < distance) {
					move(DataCache.directionArray[i]);
					return true;
				}
			}
		}
		return false;
	}

	
	public static MapLocation getDestination() {
		return destination;
	}
}