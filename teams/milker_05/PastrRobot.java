package milker_05;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class PastrRobot extends BaseRobot {

	public PastrRobot(RobotController rc) throws GameActionException {
		super(rc);
		JobMessage jm = JobSystem.createPastrCompletedMessage();
		BroadcastSystem.sendJobMessage(jm);
	}

	@Override
	public void run() {
		try {
			DataCacheTower.updateRoundVariables(true);
			
			// TODO recieve broadcast messages that I could work on
			
			// TODO send messages to HQ for my defending
			
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}

}
