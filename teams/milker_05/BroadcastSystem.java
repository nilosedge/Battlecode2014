package milker_05;

import java.util.ArrayList;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.TerrainTile;


public class BroadcastSystem {
	
	public static BaseRobot robot;
	public static RobotController rc;
	public static int WIDTH = 0;
	public static int HEIGHT = 0;
	
	public static final int jobMessageLowOffset = 10000;
	public static final int jobMessageHighOffset = 20000;
	public static final int jobMessageSize = 10;
	public static final int jobMessageRange = (jobMessageHighOffset - jobMessageLowOffset) / jobMessageSize;
	public static       int jobMessageLastIndex = 0;

	public static final int jobMessageHQLowOffset = 20000;
	public static final int jobMessageHQHighOffset = 30000;
	public static final int jobMessageHQSize = 10;
	public static final int jobMessageHQRange = (jobMessageHQHighOffset - jobMessageHQLowOffset) / jobMessageHQSize;
	public static       int jobMessageHQLastIndex = 0;
	
	public static final int robotData = 1100;
	public static final int latestJobMessage = 1101;
	public static final int latestJobMessageHQ = 1105;
	
	public static       int lastJobMessageIndex = 0;
	
	public static final int defendingCount = 1102;
	public static final int soldierCount = 1103;
	public static final int pastrCount = 1104;
	public static final int noiseCount = 1106;
	public static final int mapComplete = 1107;
	
	public static final int bandChanelBase = 1110; // Used for bands 0 - 4 they will add their band offset to this
		
	public static final int ourPastrLocationsOffset = 1200;
	
	public static final int jobMessagePayloads = 10000; // top end will be 19999
	
	public static final int pastrPointsOffset = 59800;
	public static final int mapOffset = 60000;
	
	public static final boolean debug = false;

	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
		WIDTH = DataCache.mapWidth;
		HEIGHT = DataCache.mapHeight;
	}

	public static void sendMap(TerrainTile[][] map) throws GameActionException {
		
		int array[] = new int[(HEIGHT * WIDTH / 16) + 1];
		int count = 0;
		int idx = 0;
		int mod = 0;
		for(int i = 0; i < HEIGHT; i++) {
			for(int j = 0; j < WIDTH; j++) {
				idx = count / 16;
				mod = count % 16;
				array[idx] |= map[j][i].ordinal() << (mod * 2);
				count++;
			}
		}

		rc.broadcast(mapOffset, array.length);
		int i = 0;
		while(i < array.length) {
			rc.broadcast(i + mapOffset + 1, array[i++]);
		}
	}
	
	public static TerrainTile[][] readMap() throws GameActionException {
		TerrainTile[][] map = new TerrainTile[WIDTH][HEIGHT];
		int mapSize = rc.readBroadcast(mapOffset);
		int count = 0;
		for(int i = 0; i < mapSize; i++) {
			int data = rc.readBroadcast(i + mapOffset + 1);
			for(int j = 0; j < 16; j++) {
				map[count % WIDTH][(count / HEIGHT)] = TerrainTile.values()[(3 & (data >> (j * 2)))];
				count++;
				if(count >= (HEIGHT * WIDTH)) {
					break;
				}
			}
			if(count >= (HEIGHT * WIDTH)) {
				break;
			}
		}
		return map;
	}

	public static void sendGoodMapSpots(ArrayList<MapLocation> goodTowerLocations) throws GameActionException {
		rc.broadcast(pastrPointsOffset, goodTowerLocations.size());
		int send = 0;
		for(int i = 0; i < goodTowerLocations.size(); i++) {
			send = Util.convertMapLocationToInt(goodTowerLocations.get(i));
			rc.broadcast(i + pastrPointsOffset + 1, send);
		}
	}
	
	public static ArrayList<MapLocation> readGoodMapSpots() throws GameActionException {
		ArrayList<MapLocation> locs = new ArrayList<MapLocation>();
		int size = rc.readBroadcast(pastrPointsOffset);
		for(int i = 0; i < size; i++) {
			int data = rc.readBroadcast(i + pastrPointsOffset + 1);
			locs.add(Util.convertIntToMapLocation(data));
		}
		return locs;
	}
	
	public static void sendJobMessageToHQ(JobMessage jm) throws GameActionException {
		jobMessageHQLastIndex = rc.readBroadcast(latestJobMessageHQ);
		int offset = ((jobMessageHQLastIndex * jobMessageHQSize) + jobMessageHQLowOffset);
		postJobMessage(jm, offset);
		jobMessageHQLastIndex++;
		jobMessageHQLastIndex %= jobMessageHQRange;
		rc.broadcast(latestJobMessageHQ, jobMessageHQLastIndex);
	}

	public static void sendJobMessage(JobMessage jm) throws GameActionException {

		//jm.setIndex(lastJobMessageIndex);
		//jm.setRoundNumber(Clock.getRoundNum());
		int offset = ((jobMessageLastIndex * jobMessageSize) + jobMessageLowOffset);
		//System.out.println("Writting Job: " + offset);
		
		postJobMessage(jm, offset);
		
		//if(debug) Util.printJobMessage(jm, true);
		//if(debug) System.out.println("latestMessage: " + latestMessage);
		//if(debug) System.out.println("lastJobMessageIndex: " + lastJobMessageIndex);
		
		jobMessageLastIndex++;
		jobMessageLastIndex %= jobMessageRange;
		//Util.printJobMessage(jm, true);

		//if(debug) System.out.println("lastJobMessageIndex: " + lastJobMessageIndex);

	}
	
	public static void postJobMessageHeader(JobMessage jm, int offset) throws GameActionException {
		rc.broadcast(offset, jm.header);
	}
	
	public static void postJobMessage(JobMessage jm, int offset) throws GameActionException {
		postJobMessageHeader(jm, offset);
		for(int k = 0; k < jm.body.length; k++) {
			rc.broadcast(offset + k + 1, jm.body[k]);
		}
	}
	
	public static JobMessage getJobMessage() throws GameActionException {
		JobMessage jm = null;

		int readLocation = rc.readBroadcast(latestJobMessage);
		
		for(int i = readLocation; i >= 0 && i > readLocation - 5; i--) {
			int offset = ((i * jobMessageSize) + jobMessageLowOffset);
			//System.out.print(readLocation + ", ");
			jm = new JobMessage(rc.readBroadcast(offset));
			if(jm.header != 0 && (!jm.isAccepted() || jm.getBroadcastType() == JobMessage.BroadCastType.GLOBAL)) {
				jm.setAccepted(true);
				postJobMessageHeader(jm, offset);
				for(int k = 0; k < jm.body.length; k++) {
					jm.body[k] = rc.readBroadcast(offset + k + 1);
				}
				//System.out.println();
				return jm;
			} else {
				jm = null;
			}
		}
		//System.out.println();
		return jm;
			
	}

	public static JobMessage getJobMessageHQ() throws GameActionException {
		JobMessage jm = null;

		int readLocation = rc.readBroadcast(latestJobMessageHQ);
		
		for(int i = lastJobMessageIndex; i < readLocation; i++) {
			int offset = ((i * jobMessageHQSize) + jobMessageHQLowOffset);
			jm = new JobMessage(rc.readBroadcast(offset));
			if(jm.header != 0) {
				jm.setAccepted(true);
				postJobMessageHeader(jm, offset);
				for(int k = 0; k < jm.body.length; k++) {
					jm.body[k] = rc.readBroadcast(offset + k + 1);
				}
			} else {
				jm = null;
			}
		}
		return jm;
	}
	

	public static JobMessage getAllInJob() {
		
		return null;
	}
	
	public static void sendRemoteData(int[] in) throws GameActionException {
		rc.broadcast(robotData, Util.convertMapLocationToInt(new MapLocation(in[0], in[1])));
		rc.broadcast(robotData + 1, Util.convertMapLocationToInt(new MapLocation(in[2], in[3])));
	}

	public static int[] readRemoteData() throws GameActionException {
		int data[] = new int[4];
		MapLocation d1 = Util.convertIntToMapLocation(rc.readBroadcast(robotData));
		MapLocation d2 = Util.convertIntToMapLocation(rc.readBroadcast(robotData + 1));
		data[0] = d1.x;
		data[1] = d1.y;
		data[2] = d2.x;
		data[3] = d2.y;
		return data;
	}
	
	public static MapLocation[] readOurPastrLocations() throws GameActionException {
		int len = rc.readBroadcast(ourPastrLocationsOffset);
		MapLocation[] locs = new MapLocation[len];
		for(int i = 0; i < len; i++) {
			locs[i] = Util.convertIntToMapLocation(rc.readBroadcast(i + 1));
		}
		return locs;
	}

	public static void sendOurPastrLocations(MapLocation[] ourPastrLocations) throws GameActionException {
		rc.broadcast(ourPastrLocationsOffset, ourPastrLocations.length);
		for(int i = 0; i < ourPastrLocations.length; i++) {
			rc.broadcast(ourPastrLocationsOffset + (i + 1), Util.convertMapLocationToInt(ourPastrLocations[i]));
		}
	}

	public static void maintainJobs() throws GameActionException {
		int startIndex = rc.readBroadcast(latestJobMessage);
		int endIndex = jobMessageLastIndex;

		while(startIndex < endIndex) {
			int offset = ((startIndex * jobMessageSize) + jobMessageLowOffset);
			//System.out.println("Reading: " + offset);
			JobMessage jm = new JobMessage(rc.readBroadcast(offset));
			//Util.printJobMessage(jm, true);
			if(jm.header != 0 && jm.isAccepted()) {
				startIndex++;
			} else {
				break;
			}
		}

		rc.broadcast(latestJobMessage, startIndex);
	}

	public static void updateSolderierCount() throws GameActionException {
		int count = rc.readBroadcast(soldierCount);
		count++;
		rc.broadcast(soldierCount, count);
	}
	
	public static void updatePastrCount() throws GameActionException {
		int count = rc.readBroadcast(pastrCount);
		count++;
		rc.broadcast(pastrCount, count);
	}
	
	public static void updateNoiseCount() throws GameActionException {
		int count = rc.readBroadcast(noiseCount);
		count++;
		rc.broadcast(noiseCount, count);
	}

	public static int[] getStatusCounts() throws GameActionException {
		int counts[] = new int[10];
		
		counts[0] = rc.readBroadcast(defendingCount);
		counts[1] = rc.readBroadcast(soldierCount);
		counts[2] = rc.readBroadcast(pastrCount);
		counts[3] = rc.readBroadcast(noiseCount);
		
		counts[5] = rc.readBroadcast(bandChanelBase);
		counts[6] = rc.readBroadcast(bandChanelBase + 1);
		counts[7] = rc.readBroadcast(bandChanelBase + 2);
		counts[8] = rc.readBroadcast(bandChanelBase + 3);
		counts[9] = rc.readBroadcast(bandChanelBase + 4);
		clearCounters();
		return counts;
	}
	
	public static void clearCounters() throws GameActionException {
		rc.broadcast(defendingCount, 0);
		rc.broadcast(soldierCount, 0);
		rc.broadcast(pastrCount, 0);
		rc.broadcast(noiseCount, 0);
		
		rc.broadcast(bandChanelBase, 0);
		rc.broadcast(bandChanelBase + 1, 0);
		rc.broadcast(bandChanelBase + 2, 0);
		rc.broadcast(bandChanelBase + 3, 0);
		rc.broadcast(bandChanelBase + 4, 0);
	}

	public static boolean isMapComplete() throws GameActionException {
		return (rc.readBroadcast(mapComplete) > 0);
	}
}
