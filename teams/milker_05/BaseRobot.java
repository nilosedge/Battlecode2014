package milker_05;

import battlecode.common.RobotController;

public abstract class BaseRobot {
	
	public RobotController rc;
	public int id;

	public BaseRobot(RobotController rc) {
		this.rc = rc;
		id = rc.getRobot().getID();
		
		DataCache.init(this);
		BroadcastSystem.init(this);
		JobSystem.init(this);
	}
	
	abstract public void run();
	
	public void loop() {
		while (true) {
			try {
				run();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rc.yield();
		}
	}
}