package milker_05;

import java.util.ArrayList;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class SoldierRobot extends BaseRobot {

	public SoldierState soldierState = SoldierState.NEW;
	public SoldierState nextSoldierState;
	private SoldierState previousSoldierState;

	
	public boolean debugStates = true;
	
	public JobMessage allInJob;
	public JobMessage currentJob;
	private MapLocation rideoutPoint;
	private MapLocation herdingPoint;
	
	public boolean returnImmediate = false;
	
	public Strategy s;
	
	public SoldierRobot(RobotController rc) throws GameActionException {
		super(rc);
		
		NavSystem.init(this);
		DataCacheSoldier.init();
	}

	public void run() {
		if(returnImmediate) return;
		try {

			DataCacheSoldier.updateRoundVariables();
			
			JobMessage allInJob = BroadcastSystem.getAllInJob();
			if(allInJob != null) {
				NavSystem.addWayPointAndMoveCloser(allInJob.getWayPointsToJob());
				soldierState = SoldierState.ALL_IN;
				allInCode();
			}
			
			if(currentJob == null) {
				currentJob = BroadcastSystem.getJobMessage();
				//System.out.println(currentJob);
				soldierState = SoldierState.NEW;
			}

			if(currentJob != null && soldierState == SoldierState.NEW) {
				NavSystem.addWayPointAndMoveCloser(currentJob.getWayPointsToJob());
				switch(currentJob.getJobType()) {
					case DEFENDING:
						soldierState = SoldierState.DEFENDING;
						break;
					case BUILDING:
						soldierState = SoldierState.BUILDING;
						break;
					case HAVOCING:
						soldierState = SoldierState.HAVOCING;
						break;
					case TRAVEL:
						soldierState = SoldierState.TRAVELING;
						break;
					case MAPBUILD:
						DataCacheSoldier.generateMap();
						BroadcastSystem.sendMap(DataCacheSoldier.map);
						BroadcastSystem.sendJobMessageToHQ(JobSystem.createMapCompletedMessage());
						currentJob = null;
						return;
					default:
						break;
				}
			}

			
			rc.setIndicatorString(0, "Soldier State: " + soldierState.toString());
			switch(soldierState) {
				case NEW:
					newCode();
					break;
				case HERDING:
					herdingCode();
					break;
				case ALL_IN:
					allInCode();
					break;
				case BUILDING:
					buildCode();
					break;
				case HAVOCING:
					havocingCode();
					break;
				case RIDEOUT:
					rideoutCode();
					break;
				case TRAVELING:
					travelingCode();
					break;
				case DEFENDING:
					defendingCode();
					break;
				default:
					break;
			}
			
			if(nextSoldierState != null) {
				soldierState = nextSoldierState;
				nextSoldierState = null; // clear the state for the next call of run() to use
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void newCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.NEW;
			fightingCode();
		} 
	}

	public void havocingCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.HAVOCING;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				ArrayList<MapLocation> patrolPoints = currentJob.getJobPoints();
				if(patrolPoints != null) {
					NavSystem.addWayPointAndMoveCloser(patrolPoints);
					nextSoldierState = SoldierState.TRAVELING;
					previousSoldierState = SoldierState.HAVOCING;
					travelingCode();
				} else {
					//BroadcastSystem.jobCompleted(currentJob);
					currentJob = null;
				}
			} else {
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.HAVOCING;
				travelingCode();
			}
		}
	}

	public void allInCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.ALL_IN;
			fightingCode();
		} else {
			System.out.println("ALLIN: set to travel");
			nextSoldierState = SoldierState.TRAVELING;
			previousSoldierState = SoldierState.ALL_IN;
			travelingCode();
		}
	}

	public void defendingCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.DEFENDING;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				ArrayList<MapLocation> drivePoints = currentJob.getJobPoints();
				if(drivePoints != null) {
					rideoutPoint = drivePoints.get(1);
					herdingPoint = drivePoints.get(0);
					NavSystem.addWayPointAndMoveCloser(rideoutPoint);
					nextSoldierState = SoldierState.RIDEOUT;
					rideoutCode();
				} else {
					//BroadcastSystem.jobCompleted(currentJob);
					currentJob = null;
				}
			} else {
				//System.out.println("DEFEND: set to travel");
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.DEFENDING;
				travelingCode();
			}
		}
	}

	public void rideoutCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.RIDEOUT;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				nextSoldierState = SoldierState.HERDING;
				NavSystem.addWayPointAndMoveCloser(herdingPoint);
				herdingCode();
			} else {
				//System.out.println("RIDEOUT: set to travel");
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.RIDEOUT;
				travelingCode();
			}
		}
	}

	public void herdingCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			nextSoldierState = SoldierState.FIGHTING;
			previousSoldierState = SoldierState.HERDING;
			fightingCode();
		} else {
			if(NavSystem.atDestination()) {
				nextSoldierState = SoldierState.RIDEOUT;
				NavSystem.addWayPointAndMoveCloser(rideoutPoint);
				rideoutCode();
			} else {
				//System.out.println("HERD: set to travel");
				nextSoldierState = SoldierState.TRAVELING;
				previousSoldierState = SoldierState.HERDING;
				travelingCode();
			}
		}
	}

	
	public void buildCode() throws GameActionException {
		if(NavSystem.atDestination()) {
			if(currentJob.getBuildType() == JobMessage.BuildType.NOISE) {
				if(rc.isActive()) {
					returnImmediate = true;
					rc.construct(RobotType.NOISETOWER);
				}
			} else if(currentJob.getBuildType() == JobMessage.BuildType.PASTR) {
				if(rc.isActive()) {
					returnImmediate = true;
					rc.construct(RobotType.PASTR);
				}
			}
		} else {
			//System.out.println("BUILDING: set to travel");
			nextSoldierState = SoldierState.TRAVELING;
			previousSoldierState = SoldierState.BUILDING;
			travelingCode();
		}
	}
	

	public void travelingCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots > 0) {
			microCode();
		} else {
			if(NavSystem.atDestination()) {
				if(currentJob.getJobType() == JobMessage.JobType.TRAVEL) {
					currentJob = null;
				}
				nextSoldierState = previousSoldierState;
			} else {
				if(previousSoldierState == SoldierState.BUILDING) {
					NavSystem.tryMoveCloser(true);
				} else {
					NavSystem.tryMoveCloser(false);
				}
			}
		}
	}

	public void fightingCode() throws GameActionException {
		if(DataCacheSoldier.numNearbyEnemyRobots == 0) {
			nextSoldierState = previousSoldierState;
		} else {
			microCode();
		}
	}
	
	public void microCode() throws GameActionException {
		MapLocation myLoc = rc.getLocation();
		Robot[] enemiesList = rc.senseNearbyGameObjects(Robot.class, 100000, DataCacheSoldier.enemy);
		int[] closestEnemyInfo = getClosestEnemy(myLoc, enemiesList);
		MapLocation closestEnemyLocation = new MapLocation(closestEnemyInfo[1], closestEnemyInfo[2]);
		int enemyDistSquared = closestEnemyLocation.distanceSquaredTo(myLoc);

		
		if(rc.isActive() && enemyDistSquared < rc.getType().attackRadiusMaxSquared) {
			rc.attackSquare(closestEnemyLocation);
		}
			
		//if(rc.canAttackSquare(closestEnemyLocation) && rc.isActive() && rc.getActionDelay() <= 0) {
		//	rc.attackSquare(closestEnemyLocation);
		//}
		boolean sneaking = false;
		if(enemyDistSquared <= 2) {
			double[] our23 = getEnemies2Or3StepsAway(myLoc);
			if(our23[0] < 1) {
				if(our23[1] >= 1) {
					NavSystem.goAwayFromLocation(closestEnemyLocation, sneaking);
				}
			}
		} else if(enemyDistSquared == 16 || enemyDistSquared > 18 || DataCacheSoldier.numNearbyAlliedSoldiers >= 3 * DataCacheSoldier.numNearbyEnemySoldiers) {
			if(rc.isActive()) {
				NavSystem.goToLocation(myLoc, closestEnemyLocation, sneaking);
			}
		} else {
			double[] our23 = getEnemies2Or3StepsAway(myLoc);
			double[] enemy23 = getEnemies2Or3StepsAwaySquare(myLoc, closestEnemyLocation, DataCacheSoldier.enemy);

			if(our23[1] > 0) {

				if(enemy23[0] > 0) {
					NavSystem.goToLocation(myLoc, closestEnemyLocation, sneaking);
				} else {
					if(enemy23[1] + enemy23[0] > our23[1] + our23[2]+1 || our23[1] + our23[2] < 1) {
						NavSystem.goToLocation(myLoc, closestEnemyLocation, sneaking);
					} else {
						NavSystem.goAwayFromLocation(closestEnemyLocation, sneaking);
					}
				}

			} else { // closest enemy is 3 dist
				if(enemy23[0] > 0) {
					NavSystem.goToLocation(myLoc, closestEnemyLocation, sneaking);
				} else if(enemy23[1] > 0) { // if enemy 2dist is > 0
					int closestDist = 100;
					int dist;
					MapLocation closestAllyLocation = null;
					Robot[] twoDistAllies = rc.senseNearbyGameObjects(Robot.class, closestEnemyLocation, 8, DataCacheSoldier.myTeam);
					for (int i = twoDistAllies.length; --i >= 0; ) {
						Robot ally = twoDistAllies[i];
						RobotInfo arobotInfo = rc.senseRobotInfo(ally);
						dist = arobotInfo.location.distanceSquaredTo(myLoc);
						if(dist<closestDist){
							closestDist = dist;
							closestAllyLocation = arobotInfo.location;
						}
					}

					double[] ally23 = getEnemies2Or3StepsAwaySquare(myLoc, closestAllyLocation, DataCacheSoldier.myTeam);

					if(enemy23[0] + enemy23[1] + enemy23[2] > ally23[1] + ally23[2]) {
						NavSystem.goToLocation(myLoc, closestEnemyLocation, sneaking);
					} else {
						NavSystem.goAwayFromLocation(closestEnemyLocation, sneaking);
					}
				} else {
					if(enemy23[2] - our23[2] >= 3 || our23[2] < 1) {
						NavSystem.goToLocation(myLoc, closestEnemyLocation, sneaking);
					} else {
						NavSystem.goAwayFromLocation(closestEnemyLocation, sneaking);
					}
					// otherwise, stay
				}
			}
		}
	}
	


	public int[] getClosestEnemy(MapLocation myLoc, Robot[] enemyRobots) throws GameActionException {
		MapLocation closestEnemy=DataCacheSoldier.enemyHQLocation; // default to HQ
		//MapLocation closestEnemy = rc.senseHQLocation();
		int closestDist = 10000000;
		
		int dist = 0;
		for (int i = enemyRobots.length; --i >= 0; ) {
			RobotInfo robotInfo = rc.senseRobotInfo(enemyRobots[i]);
			if(robotInfo.type != RobotType.HQ) {
				dist = myLoc.distanceSquaredTo(robotInfo.location);
				if(dist < closestDist) {
					closestDist = dist;
					closestEnemy = robotInfo.location;
				}
			}
		}
		int[] output = new int[4];
		output[0] = closestDist;
		output[1] = closestEnemy.x;
		output[2] = closestEnemy.y;
		//rc.setIndicatorString(2, "Closest: " + output[0] + ", " + output[1] + ", " + output[2] + ", " + output[3]);
		return output;
	}
	

	private double[] getEnemies2Or3StepsAway(MapLocation myLoc) throws GameActionException {
		double count1 = 0;
		double count2 = 0;
		double count3 = 0;
		Robot[] enemiesInVision = rc.senseNearbyGameObjects(Robot.class, 18, DataCacheSoldier.enemy);
		for (int i = enemiesInVision.length; --i >= 0; ) {
			Robot enemy = enemiesInVision[i];
			RobotInfo rinfo = rc.senseRobotInfo(enemy);
			int dist = rinfo.location.distanceSquaredTo(myLoc);
			if(rinfo.type == RobotType.SOLDIER) {
				if(dist <= 2) {
					count1++;
				} else if(dist <=8) {
					count2++;
				} else if(dist > 8 && (dist <= 14 || dist == 18)) {
					count3++;
				}
			} else if(rinfo.type != RobotType.HQ) {
				if(dist <= 2) {
					count1 += 0.2;
				} else if(dist <=8) {
					count2 += 0.2;
				} else if(dist > 8 && (dist <= 14 || dist == 18)) {
					count3 += 0.2;
				}
			}
		}

		double[] output = {count1, count2, count3};
		return output;
	}

	private double[] getEnemies2Or3StepsAwaySquare(MapLocation myLoc, MapLocation square, Team squareTeam) throws GameActionException {
		double count1 = 0;
		double count2 = 0;
		double count3 = 0;
		if(square == null) {
			double[] output = {count1, count2, count3};
			return output;
		}
		Robot[] enemiesInVision = rc.senseNearbyGameObjects(Robot.class, square, 18, squareTeam.opponent());
		for (int i = enemiesInVision.length; --i >= 0; ) {
			Robot enemy = enemiesInVision[i];
			RobotInfo rinfo = rc.senseRobotInfo(enemy);
			int dist = rinfo.location.distanceSquaredTo(square);
			if(rinfo.type == RobotType.SOLDIER) {
				if(dist <= 2) {
					count1++;
				} else if(dist <=8) {
					count2++;
				} else if(dist <= 14 || dist == 18) {
					count3++;
				}
			} else if(rinfo.type != RobotType.HQ) {
				if(dist <= 2) {
					count1 += 0.2;
				} else if(dist <=8) {
					count2 += 0.2;
				} else if(dist <= 14 || dist == 18) {
					count3 += 0.2;
				}
			}
		}

		int selfDist = square.distanceSquaredTo(myLoc);
		
		if(selfDist <= 2) {
			count1++;
		} else if(selfDist <= 8) {
			count2++;
		} else if(selfDist <= 14 || selfDist == 18) {
			count3++;
		}

		double[] output = {count1, count2, count3};
		return output;
	}
	
}
