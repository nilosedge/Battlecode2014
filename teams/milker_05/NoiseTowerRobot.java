package milker_05;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class NoiseTowerRobot extends BaseRobot {
	
	public int rangeCounter = 0;
	public MapLocation myLoc;
	public double rotate;
	public int maxDist = 20;
	public int minDist = 7;
	public int dist = maxDist;
	public int localDist = 0;
	public double eightPI = Math.PI / 12;
	public double lookRate = eightPI;
	public int rotCount = 0;

	public NoiseTowerRobot(RobotController rc) throws GameActionException {
		super(rc);
		myLoc = rc.getLocation();
		//map = BroadcastSystem.readMap();
	}

	@Override
	public void run() {

		try {
			
			DataCacheTower.updateRoundVariables(false);
			
			if(rc.isActive()) {
				rotate += lookRate;
				
	//			System.out.println("X: " + -Math.sin(rotate + lookRate));
	//			System.out.println("Y: " + Math.cos(rotate + lookRate));
	//			System.out.println("XD: " + -Math.sin(rotate + lookRate) * dist);
	//			System.out.println("YD: " + Math.cos(rotate + lookRate) * dist);
	//
	//			System.out.println("MyLoc: X: " + myLoc.x + " Y: " + myLoc.y);
	//			System.out.println("Checking: X: " + x + " Y: " + y);
	//			
				
				
				if(dist < minDist) {
					dist = maxDist;
				}
				if(rotCount % 12 == 0) {
					dist-=1;
				}
				 
				MapLocation attack = null;
				localDist = dist;
				while(attack == null && localDist > 0) {
					attack = getAttackTile(rotate, localDist);
					localDist--;
				}
				rotCount++;
			
				rc.attackSquare(attack);
			}
			
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}
	
	public MapLocation getAttackTile(double rotate, int dist) {
		int x = (int)(-Math.sin(rotate + lookRate) * dist) + myLoc.x;
		int y = (int)(Math.cos(rotate + lookRate) * dist) + myLoc.y;
		if(x < 0 || y < 0 || x > DataCacheSoldier.mapWidth || y > DataCacheSoldier.mapHeight) {
			return null;
		}
		MapLocation loc = new MapLocation(x, y);
		if(rc.canAttackSquare(loc)) {
			return loc;
		}
		return null;
	}

}
