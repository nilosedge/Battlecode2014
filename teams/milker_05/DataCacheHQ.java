package milker_05;

import java.util.ArrayList;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.TerrainTile;

public class DataCacheHQ extends DataCache {
	

	public static int defendingCount;
	public static int soldierCount;
	public static int pastrCount;
	public static int noiseCount;
	public static int band1Count;
	public static int band3Count;
	public static int band2Count;
	public static int band4Count;
	public static int band5Count;
	public static MapLocation[] ourPastrLocations;
	public static int idealPastrCount;
	public static ArrayList<MapLocation> topTowerLocations;
	
	
	public static double[][] cowGrowth;
	
	public static void init() {
		idealPastrCount = (int)Math.sqrt(Math.sqrt((mapHeight * mapWidth))) + 2;
	}

	public static void updateRoundVariables() throws GameActionException {
		broadcastOffset = 0;
		
		ourPastrLocations = rc.sensePastrLocations(myTeam);
		//numAlliedPastrs = ourPastrLocations.length;
		
		// TODO sense how much milk everyone has in order to go all in
		
		BroadcastSystem.sendOurPastrLocations(ourPastrLocations);
		
		int counts[] = BroadcastSystem.getStatusCounts();
		
		defendingCount = counts[0];
		soldierCount = counts[1];
		pastrCount = counts[2];
		noiseCount = counts[3];
		
		band1Count = counts[5];
		band2Count = counts[6];
		band3Count = counts[7];
		band4Count = counts[8];
		band5Count = counts[9];
		//System.out.println("Defending: " + defendingCount + " SoldierCount: " + soldierCount + " PastrCount: " + pastrCount + " NoiseCount: " + noiseCount);
		
//		//numAlliedRobots = rc.senseNearbyGameObjects(Robot.class, 10000, myTeam).length;
		enemiesList = rc.senseNearbyGameObjects(Robot.class, 100000, enemy);
//		
//		numEnemyRobots = enemiesList.length;
//		
//		int sendData[] = new int[4];
//		sendData[0] = numAlliedRobots;
//		sendData[1] = numEnemyRobots;
//		BroadcastSystem.sendRemoteData(sendData);
		

		
		nearbyEnemyRobots = rc.senseNearbyGameObjects(Robot.class, 25, enemy);
		
		numNearbyEnemySoldiers = 0;
		for (int i = nearbyEnemyRobots.length; --i >= 0; ) {
			RobotInfo robotInfo = rc.senseRobotInfo(nearbyEnemyRobots[i]);
			if (robotInfo.type == RobotType.SOLDIER) {
				numNearbyEnemySoldiers++;
			}
		}
		
		numNearbyEnemyRobots = nearbyEnemyRobots.length;

	}

	public static void initTopTowerLocations() {
		topTowerLocations = new ArrayList<MapLocation>();
		topTowerLocations.add(DataCache.ourHQLocation.add(DataCache.enemyHQLocation.directionTo(DataCache.ourHQLocation)));
		
		int cowG = 0;
		int opens = 0;
		int sizeFactor = 6;
		int squ = sizeFactor * sizeFactor;
		for(int h = 0; h < mapHeight; h+=sizeFactor) {
			for(int w = 0; w < mapWidth; w+=sizeFactor) {
				for(int y = 0; y < sizeFactor && (y + h) < mapHeight; y++) {
					for(int x = 0; x < sizeFactor && (x + w) < mapWidth; x++) {
						if(DataCacheSoldier.cowGrowth[x + w][y + h] > 0.9) cowG++;
						if(map[x + w][y + h] == TerrainTile.NORMAL || map[x + w][y + h] == TerrainTile.ROAD) opens++;
					}
				}
				//System.out.println("W: " + w + " H: " + h + " Cows: " + cowG + " opens: " + opens);
				if(cowG >= (squ - sizeFactor) && opens > (squ - sizeFactor)) {
					topTowerLocations.add(new MapLocation(w + 2, h + 2));
					//goodTowerSpots.add(new MapLocation(w + 2, h + 2));
					//System.out.println("Added: ");
				} else {
					//System.out.println("No Added: ");
				}
				cowG = 0;
				opens = 0;
			}
		}
		// TODO sort by distance to HQ
		//System.out.println("Good Map Locations: " + "Count: " + counter + " BC: " + Util.stopCounter());
	}

}
