package milker_05;

import battlecode.common.Clock;
import battlecode.common.MapLocation;

public class Util {
	
	static int bn = Clock.getBytecodeNum();
	static int rn = Clock.getRoundNum();
	static int start = 0;
	static int end = 0;

	public static void randInit(int seed1, int seed2) {
    	bn = seed1;
    	rn = seed2;
	}

	private static int gen() {
    	bn = 36969 * (bn & 65535) + (bn >> 16);
    	rn = 18000 * (rn & 65535) + (rn >> 16);
    	return (bn << 16) + rn;
	}

	public static int randInt() {
		return gen();
	}

	public static double randDouble() {
		return (gen() * 2.32830644e-10 + 0.5);
	}

	public static double Random() {
		return randDouble();
	}
	
	public static int convertMapLocationToInt(MapLocation m) {
		return (m.x << 7) + m.y;
	}
	
	public static MapLocation convertIntToMapLocation(int in) {
		return new MapLocation((in >> 7), (in & 127));
	}
	
	public static MapLocation getPathCenter() {
		int x1 = DataCacheSoldier.ourHQLocation.x;
		int y1 = DataCacheSoldier.ourHQLocation.y;
		int x2 = DataCacheSoldier.enemyHQLocation.x;
		int y2 = DataCacheSoldier.enemyHQLocation.y;
		return new MapLocation((x1+x2)/2, (y1+y2)/2);
	}
	
	public static MapLocation getPathCenterSlightlyCloserToUs(int percent) {
		double x1 = DataCacheSoldier.ourHQLocation.x;
		double y1 = DataCacheSoldier.ourHQLocation.y;
		double x2 = DataCacheSoldier.enemyHQLocation.x;
		double y2 = DataCacheSoldier.enemyHQLocation.y;
		double newx = (((100.0 - (double)percent) / 100.0) * x1) + ((double)percent / 100.0 * x2);
		double newy = (((100.0 - (double)percent) / 100.0) * y1) + ((double)percent / 100.0 * y2);
		return new MapLocation((int)newx, (int)newy);
	}

	public static void printJobMessage(JobMessage jm, boolean detail) {
		if(detail) {
			System.out.println("Job Message: ");
			System.out.println("  Header: " + Integer.toBinaryString(jm.header));
			for(int i = 0; i < jm.body.length; i++) {
				System.out.println("  Body[" + i + "]: " + Integer.toBinaryString(jm.body[i]));
			}
		}
		System.out.println("Job Type:          " + jm.getJobType());
		System.out.println("Build Type:        " + jm.getBuildType());
		System.out.println("Accepted:          " + jm.isAccepted());
		System.out.println("Way Points to Job: " + jm.getWayPointsToJob());
		System.out.println("Point of Job:      " + jm.getJobPoints());
	}
	
	public static void startCounter() {
		start = (Clock.getRoundNum() * 10000) + Clock.getBytecodeNum();
	}
	
	public static int stopCounter() {
		end = (Clock.getRoundNum() * 10000) + Clock.getBytecodeNum();
		return end - start;
	}

}