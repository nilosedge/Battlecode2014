package milker_05;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.Team;
import battlecode.common.TerrainTile;

public class DataCache {

	public static BaseRobot robot;
	public static RobotController rc;
	
	public static MapLocation ourHQLocation;
	public static MapLocation enemyHQLocation;
	public static int rushDistSquared;
	public static int rushDist;
	
	public static Direction[] directionArray = Direction.values();
	
	// Map width
	public static int mapWidth;
	public static int mapHeight;
	
	// Round variables - army sizes
	// Allied robots
	
	//public static int numAlliedRobots;
	//public static int numAlliedSoldiers;
	//public static int numAlliedPastrs;
	
	//public static int numNearbyAlliedRobots;	
	//public static int numNearbyAlliedSoldiers;
	//public static int numNearbyAlliedPastrs;

	public static Team myTeam;
	public static Team enemy;
	
	// Enemy robots
	//public static int numEnemyRobots;
	
	public static Robot[] enemiesList;
	public static Robot[] nearbyEnemyRobots;
	
	public static int numNearbyEnemyRobots;
	public static int numNearbyEnemySoldiers;
	
	//public static MapLocation[] buildPastrLocations;
	public static double[][] cowGrowth;



	public static TerrainTile[][] map;
	

	//public static int roundNumber = 0;
	public static int broadcastOffset = 0;
	
	public static void init(BaseRobot myRobot) {
		robot = myRobot;
		rc = robot.rc;
		
		myTeam = rc.getTeam();
		enemy = myTeam.opponent();
		
		ourHQLocation = rc.senseHQLocation();
		enemyHQLocation = rc.senseEnemyHQLocation();
		rushDistSquared = ourHQLocation.distanceSquaredTo(enemyHQLocation);
		rushDist = (int) Math.sqrt(rushDistSquared);
		cowGrowth = rc.senseCowGrowth();
		mapWidth = rc.getMapWidth();
		mapHeight = rc.getMapHeight();

	}

	public static boolean isLargeMap() {
		return (mapHeight * mapWidth > 2500);
	}

	public static boolean isMediumMap() {
		return (mapHeight * mapWidth > 1200);
	}
	
	public static void generateMap() throws GameActionException {
		map = new TerrainTile[mapWidth][mapHeight];
		for(int i = 0; i < mapHeight; i++) {
			for(int j = 0; j < mapWidth; j++) {
				map[j][i] = rc.senseTerrainTile(new MapLocation(j, i));
			}
		}
	}

	public void printCowMap(double[][] cowGrowth) {
		for(int k = 0; k < mapHeight; k++) {
			for(int i = 0; i < mapWidth; i++) {
				System.out.print(cowGrowth[i][k]);
			}
			System.out.println();
		}
	}
	
	public void printMap() {
		printMap(map);
	}
	
	public void printMap(TerrainTile[][] map) {
		for(int k = 0; k < mapHeight; k++) {
			for(int i = 0; i < mapWidth; i++) {
				System.out.print(map[i][k].ordinal());
			}
			System.out.println();
		}
	}

}
