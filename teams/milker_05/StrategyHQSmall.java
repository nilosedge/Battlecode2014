package milker_05;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public class StrategyHQSmall implements Strategy {

	int count = 0;
	
	public StrategyHQSmall(HQRobot hqRobot) {
		
	}

	@Override
	public void run() {
		try {
			
			if(Clock.getRoundNum() % 75 == 0) {
				if(count % 5 == 0) {
					JobMessage jm = JobSystem.createTravelMessage(new MapLocation(27, 27), JobMessage.BroadCastType.GLOBAL);
					BroadcastSystem.sendJobMessage(jm);
				} else if (count % 5 == 1) {
					JobMessage jm = JobSystem.createTravelMessage(new MapLocation(2, 2), JobMessage.BroadCastType.GLOBAL);
					BroadcastSystem.sendJobMessage(jm);
				} else if (count % 5 == 2) {
					JobMessage jm = JobSystem.createTravelMessage(new MapLocation(2, 27), JobMessage.BroadCastType.GLOBAL);
					BroadcastSystem.sendJobMessage(jm);
				} else if (count % 5 == 3) {
					JobMessage jm = JobSystem.createTravelMessage(new MapLocation(27, 2), JobMessage.BroadCastType.GLOBAL);
					BroadcastSystem.sendJobMessage(jm);
				} else if (count % 5 == 4) {
					JobMessage jm = JobSystem.createTravelMessage(new MapLocation(2, 2), JobMessage.BroadCastType.GLOBAL);
					BroadcastSystem.sendJobMessage(jm);
				}
				count++;
			}
			
		} catch (GameActionException e) {
			
		}
		
		
		
		
	}

}
