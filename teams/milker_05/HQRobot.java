package milker_05;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class HQRobot extends BaseRobot {

	public Strategy s;
	
	public HQRobot(RobotController rc) throws GameActionException {
		super(rc);
		
		DataCacheHQ.init();
		
		if(DataCacheSoldier.isLargeMap()) {
			s = new StrategyHQLarge(this);
		} else if(DataCacheSoldier.isMediumMap()) {
			s = new StrategyHQMedium(this);
		} else {
			s = new StrategyHQSmall(this);
		}
	}

	public void run() {
		try {
			DataCacheHQ.updateRoundVariables();
			BroadcastSystem.maintainJobs();
			
			s.run();
			
			int[] closestEnemyInfo = getClosestEnemy(DataCache.enemiesList);
			MapLocation closestEnemyLocation = new MapLocation(closestEnemyInfo[1], closestEnemyInfo[2]);
			
			if(rc.isActive() && rc.canAttackSquare(closestEnemyLocation)) {
				rc.attackSquare(closestEnemyLocation);
			} else {
				spawn();
			}
			
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}

	public void buildTowerCodeJob(RobotType type, MapLocation loc) throws GameActionException {
		JobMessage jm = JobSystem.createBuildMessage(type, loc);
		BroadcastSystem.sendJobMessage(jm);
	}
	
	public void generateMapJob() throws GameActionException {
		JobMessage jm = JobSystem.createGenerateMapMessage();
		BroadcastSystem.sendJobMessage(jm);
	}
	
	public void travelJob(MapLocation location, JobMessage.BroadCastType type) throws GameActionException {
		JobMessage jm = JobSystem.createTravelMessage(location, type);
		BroadcastSystem.sendJobMessage(jm);
	}

	private void spawn() throws GameActionException {
		Direction dir = DataCacheHQ.ourHQLocation.directionTo(DataCacheSoldier.enemyHQLocation);
		int desiredDirOffset = dir.ordinal();
		for (int i = 0; i < DataCacheHQ.directionArray.length; i++) {
			int dirOffset = DataCacheHQ.directionArray[i].ordinal();
			Direction currentDirection = DataCacheSoldier.directionArray[(desiredDirOffset + dirOffset + 8) % 8];
			if (rc.isActive() && rc.canMove(currentDirection) && rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
				rc.spawn(currentDirection);
				return;
			}
		}
	}
	
	public int getClosestPastrMapLocationIndex(MapLocation[] buildPastrLocations) {
		int closestDist = 10000;
		int index = 0;
		MapLocation myLoc = DataCache.ourHQLocation;
		int dist = 0;
		for(int i = 0; i < buildPastrLocations.length && buildPastrLocations[i] != null; i++) {
			MapLocation loc = buildPastrLocations[i];
			dist = loc.distanceSquaredTo(myLoc);
			if(dist < closestDist) {
				boolean found = false;
				for(int k = 0; k < DataCacheHQ.ourPastrLocations.length; k++) {
					if(DataCacheHQ.ourPastrLocations[k].distanceSquaredTo(loc) < 5) {
						found = true;
					}
				}
				if(!found) {
					// We havent build there already
					closestDist = dist;
					index = i;
				}
			}
			//System.out.println("Location: " + loc + " Dist: " + dist);
		}
		return index;
	}

	public int[] getClosestEnemy(Robot[] enemyRobots) throws GameActionException {
		
		MapLocation closestEnemy=rc.senseEnemyHQLocation(); // default to HQ
		//MapLocation closestEnemy = rc.senseHQLocation();
		int closestDist = 10000;
		
		int dist = 0;
		for (int i = enemyRobots.length; --i >= 0; ) {
			RobotInfo arobotInfo = rc.senseRobotInfo(enemyRobots[i]);
			if(arobotInfo.type != RobotType.HQ) {
				dist = arobotInfo.location.distanceSquaredTo(rc.getLocation());
				if(dist < closestDist) {
					closestDist = dist;
					closestEnemy = arobotInfo.location;
				}
			}
		}
		int[] output = new int[4];
		output[0] = closestDist;
		output[1] = closestEnemy.x;
		output[2] = closestEnemy.y;
		rc.setIndicatorString(2, "Closest: " + output[0] + ", " + output[1] + ", " + output[2] + ", " + output[3]);
		return output;
	}

}