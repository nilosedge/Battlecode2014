package milker_05;

import battlecode.common.Clock;
import battlecode.common.RobotController;


public class RobotPlayer {
	
	public static void run(RobotController rc) {
		
		BaseRobot robot = null;

		int rseed = rc.getRobot().getID();
		Util.randInit(rseed, rseed * Clock.getRoundNum());
		
		try {
			switch(rc.getType()) {
				case HQ:
					robot = new HQRobot(rc);
					break;
				case NOISETOWER:
					robot = new NoiseTowerRobot(rc);
					break;
				case PASTR:
					robot = new PastrRobot(rc);
					break;
				case SOLDIER:
					robot = new SoldierRobot(rc);
					break;
				default:
					break;
			}
			robot.loop();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
