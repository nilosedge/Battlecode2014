package milker_05;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class StrategyHQLarge implements Strategy {

	private HQRobot hqRobot;
	private RobotController rc;

	public StrategyHQLarge(HQRobot hqRobot) {
		this.hqRobot = hqRobot;
		this.rc = hqRobot.rc;
	}

	@Override
	public void run() {
		try {
		
			// if map is not generated create map job
			// if map is generated check that we have read it
			// check that we need to build towers
			// check that we need to build pastrs
			// every 50 rounds or so broad defend job of all our towers to band 0
		
			if(Clock.getRoundNum() == 0) {
	
				// Generate best cow spot
				//int index = getClosestPastrMapLocationIndex(DataCache.topTowerLocations);
				
				MapLocation loc = DataCache.ourHQLocation.add(DataCache.ourHQLocation.directionTo(DataCache.enemyHQLocation));
				
				hqRobot.buildTowerCodeJob(RobotType.NOISETOWER, loc);
				
			} else if(Clock.getRoundNum() == 20) {
				//DataCache.initTopTowerLocations();
				//rc.construct(RobotType.NOISETOWER);
				// This takes 37 rounds to complete
				
				hqRobot.generateMapJob();
				
				MapLocation loc = DataCache.ourHQLocation.add(DataCache.enemyHQLocation.directionTo(DataCache.ourHQLocation));
				
				hqRobot.buildTowerCodeJob(RobotType.PASTR, loc);
				
				//BroadcastSystem.sendGoodMapSpots(map.topTowerLocations);
				// Goto Best Cow Spot
	
			} else if(Clock.getRoundNum() == 200) {
				hqRobot.travelJob(Util.getPathCenterSlightlyCloserToUs(30), JobMessage.BroadCastType.GLOBAL);
				
				
			} else if(Clock.getRoundNum() == 365) {
				hqRobot.travelJob(DataCache.enemyHQLocation, JobMessage.BroadCastType.GLOBAL);

			}
			
	
			// Build initial towers
			
			
			
			//buildTowerCode();
			
			// build some defences rallying around 1/3 at round 450 charge
			// Check map size to build more tower closer to us send two defending soldiers per tower
			// send out 2 - 6 havoc's
			// send out three solders per tower assigning a band channel
			
			//
			
			// Build small map 2 towser
			// medium 3 towers
			// large 6 towers
			
			
			
		
//		if(Clock.getRoundNum() % 25 == 0) {
//			ArrayList<MapLocation> targets = new ArrayList<MapLocation>();
//			MapLocation pastrs[] = rc.sensePastrLocations(rc.getTeam().opponent());
//			if(pastrs.length > 0) {
//				for(int i = 0; i < pastrs.length && i < 4; i++) {
//					targets.add(pastrs[i]);
//				}
//				JobMessage jm = JobSystem.createHavocMessage(NavSystem.getWayPoints(rc.getLocation(), targets.get(0), 5), targets);
//				BroadcastSystem.sendJobMessage(jm);
//			}
//		}

//		if(rc.senseRobotCount() == 0 || (DataCache.numAlliedPastrs == 0 && DataCache.roundNumber % 250 == 0)) {
//			if(DataCache.ourPastrLocations.length < DataCache.idealPastrCount - 1) {
//				//int index = getClosestPastrMapLocationIndex(DataCache.buildPastrLocations);
//				JobMessage jm = JobSystem.createBuildMessage(RobotType.NOISETOWER, DataCache.ourHQLocation);
//				BroadcastSystem.sendJobMessage(jm);
//				jm = JobSystem.createBuildMessage(RobotType.PASTR, DataCache.ourHQLocation);
//				BroadcastSystem.sendJobMessage(jm);
//			}
//		}

		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}

}
